package com.phenix.admin.mapper;

import com.phenix.admin.entity.GatewayRateLimit;
import com.phenix.core.mybatis.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GatewayRateLimitMapper extends SuperMapper<GatewayRateLimit> {
}
