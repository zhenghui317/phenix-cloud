package com.phenix.admin.mapper;

import com.phenix.admin.entity.GatewayAccessLogs;
import com.phenix.core.mybatis.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhenghui
 */
@Mapper
public interface GatewayLogsMapper extends SuperMapper<GatewayAccessLogs> {
}
