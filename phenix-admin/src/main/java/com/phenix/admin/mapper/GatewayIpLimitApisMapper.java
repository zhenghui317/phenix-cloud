package com.phenix.admin.mapper;

import com.phenix.admin.entity.GatewayIpLimitApi;
import com.phenix.admin.pojo.dto.IpLimitApiDTO;
import com.phenix.core.mybatis.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhenghui
 */
@Mapper
public interface GatewayIpLimitApisMapper extends SuperMapper<GatewayIpLimitApi> {

    List<IpLimitApiDTO> selectIpLimitApi(@Param("policyType") int policyType);
}
