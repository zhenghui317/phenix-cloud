package com.phenix.admin.mapper;

import com.phenix.admin.entity.GatewayRateLimitApi;
import com.phenix.admin.pojo.dto.RateLimitApiDTO;
import com.phenix.core.mybatis.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author zhenghui
 */
@Mapper
public interface GatewayRateLimitApisMapper extends SuperMapper<GatewayRateLimitApi> {

    List<RateLimitApiDTO> selectRateLimitApi();

}
