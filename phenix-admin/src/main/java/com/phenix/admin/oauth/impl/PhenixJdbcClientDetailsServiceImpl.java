package com.phenix.admin.oauth.impl;

import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;

/**
 * @author zhenghui
 */
@Service
public class PhenixJdbcClientDetailsServiceImpl extends JdbcClientDetailsService {
    public PhenixJdbcClientDetailsServiceImpl(DataSource dataSource) {
        super(dataSource);
    }
}
