package com.phenix.admin.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.phenix.admin.entity.GatewayRateLimit;
import com.phenix.admin.entity.GatewayRateLimitApi;
import com.phenix.admin.mapper.GatewayRateLimitApisMapper;
import com.phenix.admin.mapper.GatewayRateLimitMapper;
import com.phenix.admin.pojo.dto.RateLimitApiDTO;
import com.phenix.admin.pojo.parameter.AddRateLimitApiParameter;
import com.phenix.admin.service.IGatewayRateLimitService;
import com.phenix.core.annotation.RefreshGateway;
import com.phenix.core.mybatis.base.service.impl.BaseServiceImpl;
import com.phenix.core.mybatis.model.PageParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author zhenghui
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class GatewayRateLimitServiceImpl extends BaseServiceImpl<GatewayRateLimitMapper, GatewayRateLimit> implements IGatewayRateLimitService {
    @Autowired
    private GatewayRateLimitMapper gatewayRateLimitMapper;

    @Autowired
    private GatewayRateLimitApisMapper gatewayRateLimitApisMapper;

    /**
     * 分页查询
     *
     * @param pageParams
     * @return
     */
    @Override
    public IPage<GatewayRateLimit> findListPage(PageParams pageParams) {
        GatewayRateLimit query = pageParams.mapToObject(GatewayRateLimit.class);
        QueryWrapper<GatewayRateLimit> queryWrapper = new QueryWrapper();
        queryWrapper.lambda()
                .likeRight(ObjectUtils.isNotEmpty(query.getPolicyName()),GatewayRateLimit::getPolicyName, query.getPolicyName())
                .eq(ObjectUtils.isNotEmpty(query.getPolicyType()),GatewayRateLimit::getPolicyType, query.getPolicyType());
        queryWrapper.orderByDesc("create_time");
        return gatewayRateLimitMapper.selectPage(pageParams,queryWrapper);
    }

    /**
     * 查询接口流量限制
     *
     * @return
     */
    @Override
    public List<RateLimitApiDTO> findRateLimitApiList() {
        List<RateLimitApiDTO> list = gatewayRateLimitApisMapper.selectRateLimitApi();
        return list;
    }

    /**
     * 查询策略已绑定API列表
     *
     * @param policyId
     * @return
     */
    @Override
    public List<GatewayRateLimitApi> findRateLimitApiList(Long policyId) {
        QueryWrapper<GatewayRateLimitApi> queryWrapper = new QueryWrapper();
        queryWrapper.lambda()
                .eq(GatewayRateLimitApi::getPolicyId, policyId);
        List<GatewayRateLimitApi> list = gatewayRateLimitApisMapper.selectList(queryWrapper);
        return list;
    }

    /**
     * 获取IP限制策略
     *
     * @param policyId
     * @return
     */
    @Override
    public GatewayRateLimit getRateLimitPolicy(Long policyId) {
        return gatewayRateLimitMapper.selectById(policyId);
    }

    /**
     * 添加IP限制策略
     *
     * @param policy
     */
    @RefreshGateway
    @Override
    public GatewayRateLimit addRateLimitPolicy(GatewayRateLimit policy) {
        gatewayRateLimitMapper.insert(policy);
        return policy;
    }

    /**
     * 更新IP限制策略
     *
     * @param policy
     */
    @RefreshGateway
    @Override
    public GatewayRateLimit updateRateLimitPolicy(GatewayRateLimit policy) {
        gatewayRateLimitMapper.updateById(policy);
        return policy;
    }

    /**
     * 删除IP限制策略
     *
     * @param policyId
     */
    @RefreshGateway
    @Override
    public void removeRateLimitPolicy(Long policyId) {
        clearRateLimitApisByPolicyId(policyId);
        gatewayRateLimitMapper.deleteById(policyId);
    }

    /**
     * 绑定API, 一个API只能绑定一个策略
     *
     * @param addRateLimitApiParameter
     */
    @RefreshGateway
    @Override
    public void addRateLimitApis(AddRateLimitApiParameter addRateLimitApiParameter) {
        // 先清空策略已有绑定
        clearRateLimitApisByPolicyId(addRateLimitApiParameter.getPolicyId());
        if (CollectionUtil.isNotEmpty(addRateLimitApiParameter.getApiIds())) {
            for (Long apiId : addRateLimitApiParameter.getApiIds()) {
                // 先api解除所有绑定, 一个API只能绑定一个策略
                clearRateLimitApisByApiId(apiId);
                GatewayRateLimitApi item = new GatewayRateLimitApi();
                item.setApiId(apiId);
                item.setPolicyId(addRateLimitApiParameter.getPolicyId());
                // 重新绑定策略
                gatewayRateLimitApisMapper.insert(item);
            }
        }
    }

    /**
     * 清空绑定的API
     *
     * @param policyId
     */
    @RefreshGateway
    @Override
    public void clearRateLimitApisByPolicyId(Long policyId) {
        QueryWrapper<GatewayRateLimitApi> queryWrapper = new QueryWrapper();
        queryWrapper.lambda()
                .eq(GatewayRateLimitApi::getPolicyId, policyId);
        gatewayRateLimitApisMapper.delete(queryWrapper);
    }

    /**
     * API解除所有策略
     *
     * @param apiId
     */
    @RefreshGateway
    @Override
    public void clearRateLimitApisByApiId(Long apiId) {
        QueryWrapper<GatewayRateLimitApi> queryWrapper = new QueryWrapper();
        queryWrapper.lambda()
                .eq(GatewayRateLimitApi::getApiId, apiId);
        gatewayRateLimitApisMapper.delete(queryWrapper);
    }
}
