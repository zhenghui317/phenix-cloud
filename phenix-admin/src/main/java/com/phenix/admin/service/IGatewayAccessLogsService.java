package com.phenix.admin.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.phenix.admin.entity.GatewayAccessLogs;
import com.phenix.core.mybatis.model.PageParams;

/**
 * 网关访问日志
 * @author zhenghui
 */
public interface IGatewayAccessLogsService {
    /**
     * 分页查询
     *
     * @param pageParams
     * @return
     */
    IPage<GatewayAccessLogs> findListPage(PageParams pageParams);
}
