package com.phenix.admin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.phenix.admin.entity.GatewayRateLimit;
import com.phenix.admin.entity.GatewayRateLimitApi;
import com.phenix.admin.pojo.dto.RateLimitApiDTO;
import com.phenix.admin.pojo.parameter.AddRateLimitApiParameter;
import com.phenix.core.mybatis.base.service.IBaseService;
import com.phenix.core.mybatis.model.PageParams;

import java.util.List;

/**
 * 访问日志
 * @author zhenghui
 */
public interface IGatewayRateLimitService extends IBaseService<GatewayRateLimit> {

    /**
     * 分页查询
     *
     * @param pageParams
     * @return
     */
    IPage<GatewayRateLimit> findListPage(PageParams pageParams);

    /**
     * 查询接口流量限制
     *
     * @return
     */
    List<RateLimitApiDTO> findRateLimitApiList();

    /**
     * 查询策略已绑定API列表
     *
     * @return
     */
    List<GatewayRateLimitApi> findRateLimitApiList(Long policyId);

    /**
     * 获取限流策略
     *
     * @param policyId
     * @return
     */
    GatewayRateLimit getRateLimitPolicy(Long policyId);

    /**
     * 添加限流策略
     *
     * @param policy
     * @return
     */
    GatewayRateLimit addRateLimitPolicy(GatewayRateLimit policy);

    /**
     * 更新限流策略
     *
     * @param policy
     */
    GatewayRateLimit updateRateLimitPolicy(GatewayRateLimit policy);

    /**
     * 删除限流策略
     *
     * @param policyId
     */
    void removeRateLimitPolicy(Long policyId);

    /**
     * 绑定API, 一个API只能绑定一个策略
     *
     * @param addRateLimitApiParameter
     */
    void addRateLimitApis(AddRateLimitApiParameter addRateLimitApiParameter);

    /**
     * 清空绑定的API
     *
     * @param policyId
     */
    void clearRateLimitApisByPolicyId(Long policyId);

    /**
     * API解除所有策略
     *
     * @param apiId
     */
    void clearRateLimitApisByApiId(Long apiId);
}
