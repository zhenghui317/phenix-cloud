package com.phenix.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.phenix.admin.entity.GatewayRoute;
import com.phenix.admin.mapper.GatewayRouteMapper;
import com.phenix.admin.service.IGatewayRouteService;
import com.phenix.core.annotation.RefreshGateway;
import com.phenix.core.exception.PhenixAlertException;
import com.phenix.core.mybatis.base.service.impl.BaseServiceImpl;
import com.phenix.core.mybatis.model.PageParams;
import com.phenix.defines.constants.BaseConstants;
import com.phenix.tools.string.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author zhenghui
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class GatewayRouteServiceImpl extends BaseServiceImpl<GatewayRouteMapper, GatewayRoute> implements IGatewayRouteService {



    /**
     * 分页查询
     *
     * @param pageParams
     * @return
     */
    @Override
    public IPage<GatewayRoute> findListPage(PageParams pageParams) {
        QueryWrapper<GatewayRoute> queryWrapper = new QueryWrapper();
        return page(pageParams, queryWrapper);
    }

    /**
     * 查询可用路由列表
     *
     * @return
     */
    @Override
    public List<GatewayRoute> findRouteList() {
        QueryWrapper<GatewayRoute> queryWrapper = new QueryWrapper();
        queryWrapper.lambda().eq(GatewayRoute::getStatus, BaseConstants.ENABLED);
        List<GatewayRoute> list = list(queryWrapper);
        return list;
    }

    /**
     * 获取路由信息
     *
     * @param routeId
     * @return
     */
    @Override
    public GatewayRoute getRoute(Long routeId) {
        return getById(routeId);
    }

    /**
     * 添加路由
     *
     * @param route
     */
    @RefreshGateway
    @Override
    public void addRoute(GatewayRoute route) {
        if (StringUtils.isBlank(route.getPath())) {
            throw new PhenixAlertException(String.format("path不能为空!"));
        }
        if (isExist(route.getRouteName())) {
            throw new PhenixAlertException(String.format("路由名称已存在!"));
        }
        route.setIsPersist(0);
        save(route);
    }

    /**
     * 更新路由
     *
     * @param route
     */
    @RefreshGateway
    @Override
    public void updateRoute(GatewayRoute route) {
        if (StringUtils.isBlank(route.getPath())) {
            throw new PhenixAlertException("path不能为空");
        }
        GatewayRoute saved = getRoute(route.getRouteId());
        if (saved == null) {
            throw new PhenixAlertException("路由信息不存在!");
        }
        if (saved != null && saved.getIsPersist().equals(BaseConstants.ENABLED)) {
            throw new PhenixAlertException("保留数据,不允许修改");
        }
        if (!saved.getRouteName().equals(route.getRouteName())) {
            // 和原来不一致重新检查唯一性
            if (isExist(route.getRouteName())) {
                throw new PhenixAlertException("路由名称已存在!");
            }
        }
        updateById(route);
    }

    /**
     * 删除路由
     *
     * @param routeId
     */
    @RefreshGateway
    @Override
    public void removeRoute(Long routeId) {
        GatewayRoute saved = getRoute(routeId);
        if (saved != null && saved.getIsPersist().equals(BaseConstants.ENABLED)) {
            throw new PhenixAlertException("保留数据,不允许删除");
        }
        removeById(routeId);
    }

    /**
     * 查询地址是否存在
     *
     * @param routeName
     */
    @Override
    public Boolean isExist(String routeName) {
        QueryWrapper<GatewayRoute> queryWrapper = new QueryWrapper();
        queryWrapper.lambda().eq(GatewayRoute::getRouteName, routeName);
        int count = count(queryWrapper);
        return count > 0;
    }
}
