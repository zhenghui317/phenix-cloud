package com.phenix.admin.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.phenix.admin.entity.GatewayRoute;
import com.phenix.core.mybatis.base.service.IBaseService;
import com.phenix.core.mybatis.model.PageParams;

import java.util.List;

/**
 * 路由管理
 *
 * @author zhenghui
 */
public interface IGatewayRouteService extends IBaseService<GatewayRoute> {
    /**
     * 分页查询
     *
     * @param pageParams
     * @return
     */
    IPage<GatewayRoute> findListPage(PageParams pageParams);

    /**
     * 查询可用路由列表
     *
     * @return
     */
    List<GatewayRoute> findRouteList();

    /**
     * 获取路由信息
     *
     * @param routeId
     * @return
     */
    GatewayRoute getRoute(Long routeId);

    /**
     * 添加路由
     *
     * @param route
     */
    void addRoute(GatewayRoute route);

    /**
     * 更新路由
     *
     * @param route
     */
    void updateRoute(GatewayRoute route);

    /**
     * 删除路由
     *
     * @param routeId
     */
    void removeRoute(Long routeId);

    /**
     * 是否存在
     *
     * @param routeName
     * @return
     */
    Boolean isExist(String routeName);
}
