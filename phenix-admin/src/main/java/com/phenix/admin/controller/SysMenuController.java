package com.phenix.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.phenix.admin.entity.SysAction;
import com.phenix.admin.entity.SysMenu;
import com.phenix.admin.pojo.parameter.MenuParameter;
import com.phenix.admin.service.ISysActionService;
import com.phenix.admin.service.ISysMenuService;
import com.phenix.admin.model.PageParams;
import com.phenix.defines.response.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author zhenghui
 */
@Slf4j
@SuppressWarnings("unchecked")
@Api(tags = "系统菜单资源管理")
@RestController
@RequestMapping("/menu")
public class SysMenuController {
    @Autowired
    private ISysMenuService sysMenuService;

    @Autowired
    private ISysActionService resourceOperationService;


    /**
     * 获取分页菜单资源列表
     *
     * @return
     */
    @ApiOperation(value = "获取分页菜单资源列表", notes = "获取分页菜单资源列表")
    @GetMapping("/page")
    public ResponseMessage<IPage<SysMenu>> getMenuListPage(@RequestParam(required = false) Map map) {
        return ResponseMessage.ok(sysMenuService.findListPage(new PageParams(map)));
    }

    /**
     * 菜单所有资源列表
     *
     * @return
     */
    @ApiOperation(value = "菜单所有资源列表", notes = "菜单所有资源列表")
    @GetMapping("/all")
    public ResponseMessage<List<SysMenu>> getMenuAllList() {
        return ResponseMessage.ok(sysMenuService.findAllList());
    }


    /**
     * 获取菜单下所有操作
     *
     * @param menuId
     * @return
     */
    @ApiOperation(value = "获取菜单下所有操作", notes = "获取菜单下所有操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "menuId", value = "menuId", paramType = "form"),
    })
    @GetMapping("/action")
    public ResponseMessage<List<SysAction>> getMenuAction(Long menuId) {
        return ResponseMessage.ok(resourceOperationService.findListByMenuId(menuId));
    }

    /**
     * 获取菜单资源详情
     *
     * @param menuId
     * @return 应用信息
     */
    @ApiOperation(value = "获取菜单资源详情", notes = "获取菜单资源详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "menuId", required = true, value = "menuId"),
    })
    @GetMapping("/{menuId}/info")
    public ResponseMessage<SysMenu> getMenu(@PathVariable("menuId") Long menuId) {
        return ResponseMessage.ok(sysMenuService.getMenu(menuId));
    }

    /**
     * 添加菜单资源
     *
     * @param parameter
     * @return
     */
    @ApiOperation(value = "添加菜单资源", notes = "添加菜单资源")
    @PostMapping("/add")
    public ResponseMessage<Long> addMenu(@RequestBody MenuParameter parameter) {
        SysMenu menu = new SysMenu();
        menu.setMenuCode(parameter.getMenuCode());
        menu.setMenuName(parameter.getMenuName());
        menu.setIcon(parameter.getIcon());
        menu.setPath(parameter.getPath());
        menu.setScheme(parameter.getScheme());
        menu.setTarget(parameter.getTarget());
        menu.setStatus(parameter.getStatus());
        menu.setParentId(parameter.getParentId());
        menu.setPriority(parameter.getPriority());
        menu.setMenuDesc(parameter.getMenuDesc());
        menu.setMenuId(IdWorker.getId());
        Boolean result = sysMenuService.addMenu(menu);
        return ResponseMessage.ok(menu.getMenuId());
    }

    /**
     * 编辑菜单资源
     *
     * @param parameter
     * @return
     */
    @ApiOperation(value = "编辑菜单资源", notes = "编辑菜单资源")
    @PutMapping("/update")
    public ResponseMessage updateMenu(@RequestBody MenuParameter parameter) {
        SysMenu menu = new SysMenu();
        menu.setMenuId(parameter.getMenuId());
        menu.setMenuCode(parameter.getMenuCode());
        menu.setMenuName(parameter.getMenuName());
        menu.setIcon(parameter.getIcon());
        menu.setPath(parameter.getPath());
        menu.setScheme(parameter.getScheme());
        menu.setTarget(parameter.getTarget());
        menu.setStatus(parameter.getStatus());
        menu.setParentId(parameter.getParentId());
        menu.setPriority(parameter.getPriority());
        menu.setMenuDesc(parameter.getMenuDesc());
        sysMenuService.updateMenu(menu);
        return ResponseMessage.ok();
    }

    /**
     * 移除菜单资源
     *
     * @param menuId
     * @return
     */
    @ApiOperation(value = "移除菜单资源", notes = "移除菜单资源")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "menuId", required = true, value = "menuId", paramType = "form"),
    })
    @DeleteMapping("/remove")
    public ResponseMessage<Boolean> removeMenu(
            @RequestParam("menuId") Long menuId
    ) {
        sysMenuService.removeMenu(menuId);
        return ResponseMessage.ok();
    }
}
