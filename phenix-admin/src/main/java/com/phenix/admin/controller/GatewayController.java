package com.phenix.admin.controller;

import com.phenix.admin.entity.GatewayRoute;
import com.phenix.admin.entity.IpLimitApi;
import com.phenix.admin.entity.RateLimitApi;
import com.phenix.admin.service.IGatewayIpLimitService;
import com.phenix.admin.service.IGatewayRateLimitService;
import com.phenix.admin.service.IGatewayRouteService;
import com.phenix.defines.response.ResponseMessage;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 网关接口
 *
 * @author: zhenghui
 * @date: 2019/3/12 15:12
 * @description:
 */
@Api(tags = "网关对外接口")
@RestController
public class GatewayController {

    @Autowired
    private IGatewayIpLimitService gatewayIpLimitService;
    @Autowired
    private IGatewayRateLimitService gatewayRateLimitService;
    @Autowired
    private IGatewayRouteService gatewayRouteService;

    @ApiOperation(value = "获取服务列表", notes = "获取服务列表")
    @GetMapping("/gateway/service/list")
    public ResponseMessage getServiceList() {
        List<Map> services = Lists.newArrayList();
        List<GatewayRoute> routes = gatewayRouteService.findRouteList();
        if (routes != null && routes.size() > 0) {
            routes.forEach(route -> {
                Map service = Maps.newHashMap();
                service.put("serviceId", route.getRouteName());
                service.put("serviceName", route.getRouteDesc());
                services.add(service);
            });
        }
        return ResponseMessage.ok(services);
    }

    /**
     * 获取接口黑名单列表
     *
     * @return
     */
    @ApiOperation(value = "获取接口黑名单列表", notes = "仅限内部调用")
    @GetMapping("/gateway/api/blackList")
    public ResponseMessage<List<IpLimitApi>> getApiBlackList() {
        return ResponseMessage.ok(gatewayIpLimitService.findBlackList());
    }

    /**
     * 获取接口白名单列表
     *
     * @return
     */
    @ApiOperation(value = "获取接口白名单列表", notes = "仅限内部调用")
    @GetMapping("/gateway/api/whiteList")
    public ResponseMessage<List<IpLimitApi>> getApiWhiteList() {
        return ResponseMessage.ok(gatewayIpLimitService.findBlackList());
    }

    /**
     * 获取限流列表
     *
     * @return
     */
    @ApiOperation(value = "获取限流列表", notes = "仅限内部调用")
    @GetMapping("/gateway/api/rateLimit")
    public ResponseMessage<List<RateLimitApi>> getApiRateLimitList() {
        return ResponseMessage.ok(gatewayRateLimitService.findRateLimitApiList());
    }

    /**
     * 获取路由列表
     *
     * @return
     */
    @ApiOperation(value = "获取路由列表", notes = "仅限内部调用")
    @GetMapping("/gateway/api/route")
    public ResponseMessage<List<GatewayRoute>> getApiRouteList() {
        return ResponseMessage.ok(gatewayRouteService.findRouteList());
    }
}
