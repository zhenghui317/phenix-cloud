package com.phenix.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.phenix.admin.entity.GatewayRateLimit;
import com.phenix.admin.pojo.parameter.AddRateLimitApiParameter;
import com.phenix.admin.service.IGatewayRateLimitService;
import com.phenix.core.mybatis.model.PageParams;
import com.phenix.defines.response.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 网关流量控制
 *
 * @author: zhenghui
 * @date: 2019/3/12 15:12
 * @description:
 */
@Api(tags = "网关流量控制")
@RestController
public class GatewayRateLimitController {

    @Autowired
    private IGatewayRateLimitService gatewayRateLimitService;

    /**
     * 获取分页接口列表
     *
     * @return
     */
    @ApiOperation(value = "获取分页接口列表", notes = "获取分页接口列表")
    @GetMapping(value = "/gateway/limit/rate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseMessage<IPage<GatewayRateLimit>> getRateLimitListPage(@RequestParam(required = false) Map map) {
        return ResponseMessage.ok(gatewayRateLimitService.findListPage(new PageParams(map)));
    }

    /**
     * 查询策略已绑定API列表
     *
     * @param policyId
     * @return
     */
    @ApiOperation(value = "查询策略已绑定API列表", notes = "获取分页接口列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "policyId", value = "策略ID", paramType = "form"),
    })
    @GetMapping(value = "/gateway/limit/rate/api/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseMessage<IPage<GatewayRateLimit>> getRateLimitApiList(
            @RequestParam("policyId") Long policyId
    ) {
        return ResponseMessage.ok(gatewayRateLimitService.findRateLimitApiList(policyId));
    }

    /**
     * 绑定API
     *
     * @param policyId 策略ID
     * @param apiIds   API接口ID.多个以,隔开.选填
     * @return
     */
    @ApiOperation(value = "绑定API", notes = "一个API只能绑定一个策略")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "policyId", value = "策略ID", defaultValue = "", required = true, paramType = "form"),
            @ApiImplicitParam(name = "apiIds", value = "API接口ID.多个以,隔开.选填", defaultValue = "", required = false, paramType = "form")
    })
    @PostMapping(value = "/gateway/limit/rate/api/add", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseMessage addRateLimitApis(@RequestBody AddRateLimitApiParameter addRateLimitApiParameter) {
        gatewayRateLimitService.addRateLimitApis(addRateLimitApiParameter);
        return ResponseMessage.ok();
    }

    /**
     * 获取流量控制
     *
     * @param policyId
     * @return
     */
    @ApiOperation(value = "获取流量控制", notes = "获取流量控制")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "policyId", required = true, value = "策略ID", paramType = "path"),
    })
    @GetMapping(value = "/gateway/limit/rate/info", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseMessage<GatewayRateLimit> getRateLimit(@RequestParam("policyId") Long policyId) {
        return ResponseMessage.ok(gatewayRateLimitService.getRateLimitPolicy(policyId));
    }

    /**
     * 添加流量控制
     *
     * @param rateLimit
     * @return
     */
    @ApiOperation(value = "添加流量控制", notes = "添加流量控制")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "policyName", required = true, value = "策略名称", paramType = "form"),
            @ApiImplicitParam(name = "policyType", required = true, value = "限流规则类型:url,origin,user", allowableValues = "url,origin,user", paramType = "form"),
            @ApiImplicitParam(name = "limitQuota", required = true, value = "限制数", paramType = "form"),
            @ApiImplicitParam(name = "intervalUnit", required = true, value = "单位时间:seconds-秒,minutes-分钟,hours-小时,days-天", allowableValues = "seconds,minutes,hours,days", paramType = "form"),
    })
    @PostMapping(value = "/gateway/limit/rate/add", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseMessage<Long> addRateLimit(@RequestBody GatewayRateLimit rateLimit ) {
        Long policyId = null;
        GatewayRateLimit result = gatewayRateLimitService.addRateLimitPolicy(rateLimit);
        if(result!=null){
            policyId = result.getPolicyId();
        }
        return ResponseMessage.ok(policyId);
    }

    /**
     * 编辑流量控制
     *
     * @param rateLimit
     * @return
     */
    @ApiOperation(value = "编辑流量控制", notes = "编辑流量控制")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "policyId", required = true, value = "接口Id", paramType = "form"),
            @ApiImplicitParam(name = "policyName", required = true, value = "策略名称", paramType = "form"),
            @ApiImplicitParam(name = "policyType", required = true, value = "限流规则类型:url,origin,user", allowableValues = "url,origin,user", paramType = "form"),
            @ApiImplicitParam(name = "limitQuota", required = true, value = "限制数", paramType = "form"),
            @ApiImplicitParam(name = "intervalUnit", required = true, value = "单位时间:seconds-秒,minutes-分钟,hours-小时,days-天", allowableValues = "seconds,minutes,hours,days", paramType = "form"),
    })
    @PostMapping(value = "/gateway/limit/rate/update", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseMessage updateRateLimit(@RequestBody GatewayRateLimit rateLimit) {
        gatewayRateLimitService.updateRateLimitPolicy(rateLimit);
        return ResponseMessage.ok();
    }


    /**
     * 移除流量控制
     *
     * @param policyId
     * @return
     */
    @ApiOperation(value = "移除流量控制", notes = "移除流量控制")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "policyId", required = true, value = "policyId", paramType = "form"),
    })
    @DeleteMapping(value = "/gateway/limit/rate/remove", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseMessage removeRateLimit(
            @RequestParam("policyId") Long policyId
    ) {
        gatewayRateLimitService.removeRateLimitPolicy(policyId);
        return ResponseMessage.ok();
    }
}
