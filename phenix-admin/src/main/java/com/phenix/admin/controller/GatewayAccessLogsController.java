package com.phenix.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.phenix.admin.entity.GatewayAccessLogs;
import com.phenix.admin.service.IGatewayAccessLogsService;
import com.phenix.core.mybatis.model.PageParams;
import com.phenix.defines.response.ResponseMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 网关智能路由
 *
 * @author: zhenghui
 * @date: 2019/3/12 15:12
 * @description:
 */
@Api(tags = "网关访问日志")
@RestController
public class GatewayAccessLogsController {

    @Autowired
    private IGatewayAccessLogsService IGatewayAccessLogsService;

    /**
     * 获取分页列表
     *
     * @return
     */
    @ApiOperation(value = "获取分页访问日志列表", notes = "获取分页访问日志列表")
    @GetMapping("/gateway/access/logs")
    public ResponseMessage<IPage<GatewayAccessLogs>> getAccessLogListPage(@RequestParam(required = false) Map map) {
        return ResponseMessage.ok(IGatewayAccessLogsService.findListPage(new PageParams(map)));
    }

}
