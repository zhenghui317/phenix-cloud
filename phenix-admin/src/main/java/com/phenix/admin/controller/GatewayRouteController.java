package com.phenix.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.phenix.admin.entity.GatewayRoute;
import com.phenix.admin.service.IGatewayRouteService;
import com.phenix.core.mybatis.model.PageParams;
import com.phenix.core.security.http.PhenixRestTemplate;
import com.phenix.defines.response.ResponseMessage;
import com.phenix.tools.string.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 网关智能路由
 *
 * @author: zhenghui
 * @date: 2019/3/12 15:12
 * @description:
 */
@Api(tags = "网关智能路由")
@RestController
public class GatewayRouteController {

    @Autowired
    private IGatewayRouteService gatewayRouteService;

    /**
     * 获取分页路由列表
     *
     * @return
     */
    @ApiOperation(value = "获取分页路由列表", notes = "获取分页路由列表")
    @GetMapping(value = "/gateway/route", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseMessage<IPage<GatewayRoute>> getRouteListPage(@RequestParam(required = false) Map map) {
        return ResponseMessage.ok(gatewayRouteService.findListPage(new PageParams(map)));
    }


    /**
     * 获取路由
     *
     * @param routeId
     * @return
     */
    @ApiOperation(value = "获取路由", notes = "获取路由")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "routeId", required = true, value = "路由ID", paramType = "path"),
    })
    @GetMapping(value = "/gateway/route/info", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseMessage<GatewayRoute> getRoute(@RequestParam("routeId") Long routeId) {
        return ResponseMessage.ok(gatewayRouteService.getRoute(routeId));
    }

    /**
     * 添加路由
     *
     * @param path        路径表达式
     * @param routeName   描述
     * @param serviceId   服务名方转发
     * @param url         地址转发
     * @param stripPrefix 忽略前缀
     * @param retryable   支持重试
     * @param status      是否启用
     * @return
     */
    @ApiOperation(value = "添加路由", notes = "添加路由")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "path", required = true, value = "路径表达式", paramType = "form"),
            @ApiImplicitParam(name = "routeName", required = true, value = "路由标识", paramType = "form"),
            @ApiImplicitParam(name = "routeDesc", required = true, value = "路由名称", paramType = "form"),
            @ApiImplicitParam(name = "serviceId", required = false, value = "服务名方转发", paramType = "form"),
            @ApiImplicitParam(name = "url", required = false, value = "地址转发", paramType = "form"),
            @ApiImplicitParam(name = "stripPrefix", required = false, allowableValues = "0,1", defaultValue = "1", value = "忽略前缀", paramType = "form"),
            @ApiImplicitParam(name = "retryable", required = false, allowableValues = "0,1", defaultValue = "0", value = "支持重试", paramType = "form"),
            @ApiImplicitParam(name = "status", required = false, allowableValues = "0,1", defaultValue = "1", value = "是否启用", paramType = "form")
    })
    @PostMapping(value = "/gateway/route/add", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseMessage<Long> addRoute(@RequestBody GatewayRoute route) {
        if (route.getUrl() != null && StringUtils.isNotEmpty(route.getUrl())) {
            route.setServiceId(null);
        }
        gatewayRouteService.addRoute(route);
        return ResponseMessage.ok();
    }

    /**
     * 编辑路由
     *
     * @param routeId     路由ID
     * @param path        路径表达式
     * @param serviceId   服务名方转发
     * @param url         地址转发
     * @param stripPrefix 忽略前缀
     * @param retryable   支持重试
     * @param status      是否启用
     * @param routeName   描述
     * @return
     */
    @ApiOperation(value = "编辑路由", notes = "编辑路由")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "routeId", required = true, value = "路由Id", paramType = "form"),
            @ApiImplicitParam(name = "routeName", required = true, value = "路由标识", paramType = "form"),
            @ApiImplicitParam(name = "routeDesc", required = true, value = "路由名称", paramType = "form"),
            @ApiImplicitParam(name = "path", required = true, value = "路径表达式", paramType = "form"),
            @ApiImplicitParam(name = "serviceId", required = false, value = "服务名方转发", paramType = "form"),
            @ApiImplicitParam(name = "url", required = false, value = "地址转发", paramType = "form"),
            @ApiImplicitParam(name = "stripPrefix", required = false, allowableValues = "0,1", defaultValue = "1", value = "忽略前缀", paramType = "form"),
            @ApiImplicitParam(name = "retryable", required = false, allowableValues = "0,1", defaultValue = "0", value = "支持重试", paramType = "form"),
            @ApiImplicitParam(name = "status", required = false, allowableValues = "0,1", defaultValue = "1", value = "是否启用", paramType = "form")
    })
    @PutMapping(value = "/gateway/route/update", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseMessage updateRoute(@RequestBody GatewayRoute route) {
        if (route.getUrl() != null && StringUtils.isNotEmpty(route.getUrl())) {
            route.setServiceId(null);
        }
        gatewayRouteService.updateRoute(route);
        return ResponseMessage.ok();
    }


    /**
     * 移除路由
     *
     * @param routeId
     * @return
     */
    @ApiOperation(value = "移除路由", notes = "移除路由")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "routeId", required = true, value = "routeId", paramType = "form"),
    })
    @DeleteMapping(value = "/gateway/route/remove", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseMessage removeRoute(
            @RequestParam("routeId") Long routeId
    ) {
        gatewayRouteService.removeRoute(routeId);
        return ResponseMessage.ok();
    }
}
