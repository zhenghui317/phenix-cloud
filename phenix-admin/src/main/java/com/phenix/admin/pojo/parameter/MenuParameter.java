package com.phenix.admin.pojo.parameter;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zhenghui
 * @date 2020/6/5
 */
@Getter
@Setter
public class MenuParameter {
    @ApiModelProperty(value = "资源ID")
    private Long menuId;
    @ApiModelProperty(value = "menuCode")
    String menuCode;
    @ApiModelProperty(value = "menuName")
    String menuName;
    @ApiModelProperty(value = "icon")
    String icon;
    @ApiModelProperty(value = "scheme")
    String scheme;
    @ApiModelProperty(value = "path")
    String path;
    @ApiModelProperty(value = "target")
    String target;
    @ApiModelProperty(value = "status")
    Integer status;
    @ApiModelProperty(value = "parentId")
    Long parentId;
    @ApiModelProperty(value = "priority")
    Integer priority;
    @ApiModelProperty(value = "menuDesc")
    String menuDesc;
}
