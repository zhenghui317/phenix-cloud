package com.phenix.admin.pojo.parameter;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zhenghui
 * @date 2020/6/4
 */
@Getter
@Setter
public class RoleParameter {

    private String roleCode;
    private String roleName;
    private String roleDesc;
    private Integer status;
}
