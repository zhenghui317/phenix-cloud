package com.phenix.defines.constants;

/**
 * @author zhenghui
 * @date 2020/1/13
 */
public class ResourceConstants {

    /**
     * 扫描资源键值
     */
    public final static String SCAN_API_RESOURCE_KEY_PREFIX = "scan_api_resource:";
}
