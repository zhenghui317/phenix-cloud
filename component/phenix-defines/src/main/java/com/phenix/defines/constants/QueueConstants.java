package com.phenix.defines.constants;

/**
 * @author: zhenghui
 * @date: 2020/1/09
 * @description:
 */
public class QueueConstants {
    public static final String QUEUE_SCAN_API_RESOURCE = "cloud.scan.api.resource";
    public static final String QUEUE_ACCESS_LOGS = "cloud.access.logs";
}
