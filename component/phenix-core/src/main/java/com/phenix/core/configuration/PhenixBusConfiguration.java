package com.phenix.core.configuration;

import com.phenix.core.event.RemoteRefreshRouteEvent;
import org.springframework.cloud.bus.jackson.RemoteApplicationEventScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhenghui
 * @date 2020/1/16
 */
@Configuration
@RemoteApplicationEventScan(basePackageClasses = {RemoteRefreshRouteEvent.class})
public class PhenixBusConfiguration {
}
