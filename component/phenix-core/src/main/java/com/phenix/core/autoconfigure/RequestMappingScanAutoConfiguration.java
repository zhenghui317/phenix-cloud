package com.phenix.core.autoconfigure;

import com.phenix.core.configuration.PhenixScanProperties;
import com.phenix.core.scan.RequestMappingScan;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author zhenghui
 * @date 2020/1/13
 */
@Slf4j
@Component
public class RequestMappingScanAutoConfiguration {
    /**
     * 自定义注解扫描
     *
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(RequestMappingScan.class)
    public RequestMappingScan reRequestMappingScanAutoConfigurationsourceAnnotationScan(AmqpTemplate amqpTemplate, PhenixScanProperties scanProperties) {
        RequestMappingScan scan = new RequestMappingScan(amqpTemplate, scanProperties);
        log.info("RequestMappingScan [{}]", scan);
        return scan;
    }

}
