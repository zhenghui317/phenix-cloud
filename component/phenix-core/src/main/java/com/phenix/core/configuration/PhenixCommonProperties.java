package com.phenix.core.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 通用属性
 * @author zhenghui
 * @date 2020-1-10
 */
@Data
@ConfigurationProperties(prefix = "phenix.oauth2.common")
public class PhenixCommonProperties {
    /**
     *
     */
    private String adminServerAddr;
    /**
     *
     */
    private String apiServerAddr;
    /**
     *
     */
    private String authServerAddr;
    /**
     *
     */
    private String userAuthorizationUri;
    /**
     *
     */
    private String accessTokenUri;
    /**
     *
     */
    private String tokenInfoUri;
    /**
     *
     */
    private String userInfoUri;
}
