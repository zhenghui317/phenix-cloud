package com.phenix.core.handler;

import com.phenix.core.annotation.RefreshGateway;
import com.phenix.core.publisher.PhenixEventPublisher;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author zhenghui
 * @date 2020/1/16
 */
@Aspect
@Slf4j
@Component
public class RefreshGatewayAspectHandler {

    @Autowired
    private PhenixEventPublisher phenixEventPublisher;

    @AfterReturning(value = "@annotation(refreshGateway)")
    public void afterReturning(JoinPoint joinPoint, RefreshGateway refreshGateway) throws Throwable {
        phenixEventPublisher.refreshGateway(joinPoint.getSignature().getDeclaringType().getSimpleName());
    }
}
