package com.phenix.core.scan.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author zhenghui
 * @date 2020/1/13
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PhenixServiceResource implements Serializable {
    private String application;
    private LocalDateTime pushTime;
    private List<PhenixApiResource> apis;
}
