package com.phenix.core.autoconfigure;//package com.phenix.core.autoconfigure;
//
//import com.phenix.core.configuration.PhenixScanProperties;
//import com.phenix.core.security.token.store.PhenixRedisTokenStore;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.oauth2.provider.token.TokenStore;
//
///**
// * @author zhenghui
// * @date 2020/1/13
// */
//@Slf4j
//@Configuration
//public class TokenStoreAutoConfiguration {
//
//    @Bean
//    @ConditionalOnMissingBean(PhenixRedisTokenStore.class)
//    public PhenixRedisTokenStore tokenStore() {
//        return new PhenixRedisTokenStore();
//    }
//}
