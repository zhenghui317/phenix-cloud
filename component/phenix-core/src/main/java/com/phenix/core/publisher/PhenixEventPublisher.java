package com.phenix.core.publisher;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.phenix.core.event.RemoteRefreshRouteEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;

import java.time.LocalDateTime;

/**
 * @author zhenghui
 * @date 2020/3/12
 */
@Slf4j
public class PhenixEventPublisher {

    @Value("${spring.application.name}")
    private String applicationName;

    @Autowired
    private ApplicationEventPublisher publisher;


    /**
     * 刷新网关
     * 注:不要频繁调用!
     * 1.资源权限发生变化时可以调用
     * 2.流量限制变化时可以调用
     * 3.IP访问发生变化时可以调用
     * 4.智能路由发生变化时可以调用
     */
    public void refreshGateway() {
        this.refreshGateway(this);
    }

    /**
     * 刷新网关
     * 注:不要频繁调用!
     * 1.资源权限发生变化时可以调用
     * 2.流量限制变化时可以调用
     * 3.IP访问发生变化时可以调用
     * 4.智能路由发生变化时可以调用
     */
    public void refreshGateway(Object source) {
        try {
            publisher.publishEvent(new RemoteRefreshRouteEvent(source, applicationName, null));
            log.info("refreshGateway:success");
        } catch (Exception e) {
            log.error("refreshGateway error:{}", e.getMessage());
        }
    }

}
