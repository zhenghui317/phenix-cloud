package com.phenix.core.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 资源扫描配置
 *
 * @author LYD
 * @date 2020/1/10
 */
@Data
@ConfigurationProperties(prefix = "phenix.scan")
public class PhenixScanProperties {

    /**
     * 请求资源注册到API列表
     */
    private Boolean registerRequestMapping = true;

}
