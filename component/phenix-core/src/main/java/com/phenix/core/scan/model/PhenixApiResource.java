package com.phenix.core.scan.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zhenghui
 * @date 2020/1/13
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PhenixApiResource implements Serializable {
    private String apiName;
    private String apiCode;
    private String apiDesc;
    private String path;
    private String className;
    private String methodName;
    private String md5;
    private String requestMethod;
    private String serviceId;
    private String contentType;
    private Boolean isAuth;
}
