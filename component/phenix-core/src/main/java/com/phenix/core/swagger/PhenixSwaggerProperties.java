package com.phenix.core.swagger;

import com.google.common.collect.Lists;
import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * 自定义swagger配置
 *
 * @author LYD
 * @date 2018/7/29
 */
@Data
@ToString
@ConfigurationProperties(prefix = "swagger2")
public class PhenixSwaggerProperties {
    /**
     * 是否启用swagger,生产环境建议关闭
     */
    private boolean enabled = true;
    /**
     * 文档标题
     */
    private String title;
    /**
     * 文档描述
     */
    private String description;

    private List<String> ignores = Lists.newArrayList();
}
