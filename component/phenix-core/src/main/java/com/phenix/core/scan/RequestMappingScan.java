package com.phenix.core.scan;

import com.phenix.core.configuration.PhenixScanProperties;
import com.phenix.core.scan.model.PhenixApiResource;
import com.phenix.core.scan.model.PhenixServiceResource;
import com.phenix.defines.constants.QueueConstants;
import com.phenix.defines.constants.ResourceConstants;
import com.phenix.tools.coder.MD5Utils;
import com.phenix.tools.reflect.ReflectUtils;
import com.phenix.tools.string.StringUtils;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.MethodParameter;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.condition.RequestMethodsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import springfox.documentation.annotations.ApiIgnore;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 自定义注解扫描
 *
 * @author zhenghui
 */
@Slf4j
public class RequestMappingScan implements ApplicationListener<ApplicationReadyEvent> {

    private AmqpTemplate amqpTemplate;
    private static final AntPathMatcher pathMatch = new AntPathMatcher();
    private PhenixScanProperties scanProperties;
    /**
     * 是否自行完成
     * 当前是基于应用准备完成事件，
     * 但是启动之后会多次触发事件，
     * 因此用此属性控制只执行一次
     */
    private Boolean isExecuted = false;

    public RequestMappingScan(AmqpTemplate amqpTemplate, PhenixScanProperties scanProperties) {
        this.amqpTemplate = amqpTemplate;
        this.scanProperties = scanProperties;
    }

    private ExecutorService executorService = new ThreadPoolExecutor(2, 10,
            60L, TimeUnit.SECONDS,
            new SynchronousQueue<Runnable>());

    /**
     * 初始化方法
     *
     * @param event
     */
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        if(isExecuted){
            return;
        }
        ConfigurableApplicationContext applicationContext = event.getApplicationContext();
        amqpTemplate = applicationContext.getBean(RabbitTemplate.class);
        if (scanProperties == null || !scanProperties.getRegisterRequestMapping()) {
            return;
        }
        Environment env = applicationContext.getEnvironment();
        // 服务名称
        String serviceId = env.getProperty("spring.application.name", "application");
        // 所有接口映射
        RequestMappingHandlerMapping mapping = applicationContext.getBean(RequestMappingHandlerMapping.class);
        // 获取url与类和方法的对应信息
        Map<RequestMappingInfo, HandlerMethod> map = mapping.getHandlerMethods();
        List<RequestMatcher> permitAll = Lists.newArrayList();
        try {
            // 获取所有安全配置适配器
            Map<String, HttpSecurity> securityConfigurerAdapterMap = applicationContext.getBeansOfType(HttpSecurity.class);
            Iterator<Map.Entry<String, HttpSecurity>> iterable = securityConfigurerAdapterMap.entrySet().iterator();
            while (iterable.hasNext()) {
                HttpSecurity httpSecurity = iterable.next().getValue();
                FilterSecurityInterceptor filterSecurityInterceptor = httpSecurity.getSharedObject(FilterSecurityInterceptor.class);
                FilterInvocationSecurityMetadataSource metadataSource = filterSecurityInterceptor.getSecurityMetadataSource();

                Map<RequestMatcher, Collection<ConfigAttribute>> requestMap = (Map) ReflectUtils.getFieldValue(metadataSource, "requestMap");
                Iterator<Map.Entry<RequestMatcher, Collection<ConfigAttribute>>> requestIterable = requestMap.entrySet().iterator();
                while (requestIterable.hasNext()) {
                    Map.Entry<RequestMatcher, Collection<ConfigAttribute>> match = requestIterable.next();
                    if (match.getValue().toString().contains("permitAll")) {
                        permitAll.add(match.getKey());
                    }
                }
            }
        } catch (Exception e) {
            log.error("error:{}", e);
        }
        List<PhenixApiResource> list = Lists.newArrayList();
        MD5Utils md5Utils = new MD5Utils();
        for (Map.Entry<RequestMappingInfo, HandlerMethod> m : map.entrySet()) {
            RequestMappingInfo info = m.getKey();
            HandlerMethod method = m.getValue();
            if (method.getMethodAnnotation(ApiIgnore.class) != null) {
                // 忽略的接口不扫描
                continue;
            }
            Set<MediaType> mediaTypeSet = info.getProducesCondition().getProducibleMediaTypes();
            for (MethodParameter params : method.getMethodParameters()) {
                if (params.hasParameterAnnotation(RequestBody.class)) {
                    mediaTypeSet.add(MediaType.APPLICATION_JSON_UTF8);
                    break;
                }
            }
            String mediaTypes = getMediaTypes(mediaTypeSet);
            // 请求类型
            RequestMethodsRequestCondition methodsCondition = info.getMethodsCondition();
            String methods = getMethods(methodsCondition.getMethods());
            // 请求路径
            PatternsRequestCondition p = info.getPatternsCondition();
            String urls = getUrls(p.getPatterns());
            // 类名
            String className = method.getMethod().getDeclaringClass().getName();
            // 方法名
            String methodName = method.getMethod().getName();
            String fullName = className + "." + methodName;
            // md5码
            String md5 = MD5Utils.getMD5(serviceId + urls);
            String name = "";
            String desc = "";
            // 是否需要安全认证 默认:1-是 0-否
            Boolean isAuth = true;
            // 匹配项目中.permitAll()配置
            for (String url : p.getPatterns()) {
                for (RequestMatcher requestMatcher : permitAll) {
                    if (requestMatcher instanceof AntPathRequestMatcher) {
                        AntPathRequestMatcher pathRequestMatcher = (AntPathRequestMatcher) requestMatcher;
                        if (pathMatch.match(pathRequestMatcher.getPattern(), url)) {
                            // 忽略验证
                            isAuth = false;
                        }
                    }
                }
            }

            ApiOperation apiOperation = method.getMethodAnnotation(ApiOperation.class);

            if (apiOperation != null) {
                name = apiOperation.value();
                desc = apiOperation.notes();
            }
            name = StringUtils.isBlank(name) ? methodName : name;
            PhenixApiResource.PhenixApiResourceBuilder builder = PhenixApiResource.builder();
            builder
                    .apiName(name)
                    .apiCode(md5)
                    .apiDesc(desc)
                    .path(urls)
                    .className(className)
                    .methodName(methodName)
                    .md5(md5)
                    .requestMethod(methods)
                    .serviceId(serviceId)
                    .contentType(mediaTypes)
                    .isAuth(isAuth);
            list.add(builder.build());
        }
        PhenixServiceResource serviceResource = new PhenixServiceResource(serviceId, LocalDateTime.now(), list);
        log.info("ApplicationReadyEvent:[{}]", serviceId);
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    String key = ResourceConstants.SCAN_API_RESOURCE_KEY_PREFIX + serviceId;
                    // 发送mq扫描消息
                    amqpTemplate.convertAndSend(QueueConstants.QUEUE_SCAN_API_RESOURCE, serviceResource);
                } catch (Exception e) {
                    log.error("发送失败:{}", e);
                }
            }
        });
        isExecuted = true;
    }


    private String getMediaTypes(Set<MediaType> mediaTypes) {
        StringBuilder sbf = new StringBuilder();
        for (MediaType mediaType : mediaTypes) {
            sbf.append(mediaType.toString()).append(",");
        }
        if (mediaTypes.size() > 0) {
            sbf.deleteCharAt(sbf.length() - 1);
        }
        return sbf.toString();
    }

    private String getMethods(Set<RequestMethod> requestMethods) {
        StringBuilder sbf = new StringBuilder();
        for (RequestMethod requestMethod : requestMethods) {
            sbf.append(requestMethod.toString()).append(",");
        }
        if (requestMethods.size() > 0) {
            sbf.deleteCharAt(sbf.length() - 1);
        }
        return sbf.toString();
    }

    private String getUrls(Set<String> urls) {
        StringBuilder sbf = new StringBuilder();
        for (String url : urls) {
            sbf.append(url).append(",");
        }
        if (urls.size() > 0) {
            sbf.deleteCharAt(sbf.length() - 1);
        }
        return sbf.toString();
    }


}
