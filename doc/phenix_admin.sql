/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50722
 Source Host           : localhost:3306
 Source Schema         : phenix_admin

 Target Server Type    : MySQL
 Target Server Version : 50722
 File Encoding         : 65001

 Date: 04/06/2020 20:19:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for oauth_access_token
-- ----------------------------
DROP TABLE IF EXISTS `oauth_access_token`;
CREATE TABLE `oauth_access_token`  (
  `token_id` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `token` blob NULL,
  `authentication_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `client_id` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `authentication` blob NULL,
  `refresh_token` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`authentication_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'oauth2访问令牌' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oauth_approvals
-- ----------------------------
DROP TABLE IF EXISTS `oauth_approvals`;
CREATE TABLE `oauth_approvals`  (
  `userId` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `clientId` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `scope` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `expiresAt` datetime(0) NULL DEFAULT NULL,
  `lastModifiedAt` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'oauth2已授权客户端' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details`  (
  `client_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `client_secret` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `resource_ids` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `scope` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `authorized_grant_types` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `web_server_redirect_uri` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `authorities` varchar(2048) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `access_token_validity` int(11) NULL DEFAULT NULL,
  `refresh_token_validity` int(11) NULL DEFAULT NULL,
  `additional_information` varchar(4096) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `autoapprove` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'oauth2客户端信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_client_details
-- ----------------------------
INSERT INTO `oauth_client_details` VALUES ('7gBZcbsC7kLIWCdELIl8nxcs', '{bcrypt}$2a$10$4di0sSQdr9yk4uTKWtZqzedsxI8sWXoR67x.G.Qmy4K2L7ZaFQt6W', '', 'userProfile', 'authorization_code,client_credentials,password', 'http://localhost:8888/login,http://localhost:8888/webjars/springfox-swagger-ui/o2c.html,http://www.baidu.com', '', 43200, 2592000, '{\"website\":\"http://www.baidu.com\",\"apiKey\":\"7gBZcbsC7kLIWCdELIl8nxcs\",\"secretKey\":\"0osTIhce7uPvDKHz6aa67bhCukaKoYl4\",\"appName\":\"平台用户认证服务器\",\"updateTime\":1562841065000,\"isPersist\":1,\"appOs\":\"\",\"appIcon\":\"\",\"developerId\":0,\"createTime\":1542016125000,\"appType\":\"server\",\"appDesc\":\"资源服务器\",\"appId\":\"1552274783265\",\"appNameEn\":\"open-cloud-uaa-admin-server\",\"status\":1}', '');
INSERT INTO `oauth_client_details` VALUES ('rOOM15Zbc3UFWgW2h71gRFvi', '{bcrypt}$2a$10$NSb94sKA5i9kS/F0mUBehuBs9Gtvlv8wdxQOYMg3WxMRt0nyP2Xn.', '', 'userProfile', 'authorization_code,client_credentials,password', 'http://localhost:2222/login', '', 43200, 2592000, '{\"website\":\"http://www.baidu.com\",\"apiKey\":\"rOOM15Zbc3UFWgW2h71gRFvi\",\"secretKey\":\"2Iw2B0UCMYd1cZjt8Fpr4KJUx75wQCwW\",\"appName\":\"SSO单点登录DEMO\",\"updateTime\":1568611165000,\"isPersist\":1,\"appOs\":\"\",\"appIcon\":\"\",\"developerId\":0,\"createTime\":1542016125000,\"appType\":\"pc\",\"appDesc\":\"sso\",\"appId\":\"1552294656514\",\"appNameEn\":\"sso-demo\",\"status\":1}', '');

-- ----------------------------
-- Table structure for oauth_client_token
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_token`;
CREATE TABLE `oauth_client_token`  (
  `token_id` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `token` blob NULL,
  `authentication_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `client_id` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`authentication_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'oauth2客户端令牌' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oauth_code
-- ----------------------------
DROP TABLE IF EXISTS `oauth_code`;
CREATE TABLE `oauth_code`  (
  `code` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `authentication` blob NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'oauth2授权码' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oauth_refresh_token
-- ----------------------------
DROP TABLE IF EXISTS `oauth_refresh_token`;
CREATE TABLE `oauth_refresh_token`  (
  `token_id` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `token` blob NULL,
  `authentication` blob NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'oauth2刷新令牌' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_account
-- ----------------------------
DROP TABLE IF EXISTS `sys_account`;
CREATE TABLE `sys_account`  (
  `account_id` bigint(20) NOT NULL,
  `tenant_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '租户id',
  `user_id` bigint(20) NOT NULL COMMENT '用户Id',
  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标识：手机号、邮箱、 用户名、或第三方应用的唯一标识',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码凭证：站内的保存密码、站外的不保存或保存token）',
  `account_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录类型:password-密码、mobile-手机号、email-邮箱、weixin-微信、weibo-微博、qq-等等',
  `domain` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账户域:@admin.com,@developer.com',
  `register_ip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注册IP',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态:0-禁用 1-启用 2-锁定',
  `expire_time` datetime(0) NULL DEFAULT '2100-12-12 00:00:00' COMMENT '帐号过期时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建用户id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
  `update_user_id` bigint(20) NULL DEFAULT NULL COMMENT '更新用户id',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否逻辑删除，0:未删除；1:已删除',
  PRIMARY KEY (`account_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '登录账号' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_account
-- ----------------------------
INSERT INTO `sys_account` VALUES (1, 0, 1, 'admin', '$2a$10$fP3giMF94xDTlyEaW0xl2u5BnF9/6nB81maFIa3.CltKzLbELAxny', 'username', '@admin.com', NULL, 1, '2100-12-12 00:00:00', NULL, NULL, '2019-07-03 17:11:59', NULL, '2020-06-04 18:04:14', b'0');
INSERT INTO `sys_account` VALUES (2, 0, 2, 'test', '$2a$10$fP3giMF94xDTlyEaW0xl2u5BnF9/6nB81maFIa3.CltKzLbELAxny', 'username', '@admin.com', NULL, 1, '2100-12-12 00:00:00', NULL, NULL, '2019-07-03 17:12:02', NULL, '2019-07-11 17:20:44', b'0');
INSERT INTO `sys_account` VALUES (1268503319009333249, 0, 1268503318577319937, 'test1', '$2a$10$uCMkxMvQhe0DEaJpc8tuMu2Bys03GMF6MBv9LfLKiOEho9QJPlcru', 'username', '@admin.com', NULL, 0, '2100-12-12 00:00:00', NULL, NULL, '2020-06-04 19:22:14', NULL, '2020-06-04 20:01:02', b'0');

-- ----------------------------
-- Table structure for sys_action
-- ----------------------------
DROP TABLE IF EXISTS `sys_action`;
CREATE TABLE `sys_action`  (
  `action_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '资源ID',
  `service_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '服务名称',
  `action_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '资源编码',
  `action_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '资源名称',
  `action_desc` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '资源描述',
  `menu_id` bigint(20) NULL DEFAULT NULL COMMENT '资源父节点',
  `priority` int(10) NOT NULL DEFAULT 0 COMMENT '优先级 越小越靠前',
  `status` tinyint(3) NOT NULL DEFAULT 1 COMMENT '状态:0-无效 1-有效',
  `is_persist` bit(1) NOT NULL DEFAULT b'0' COMMENT '保留数据0-否 1-是 不允许删除',
  `create_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '创建用户id',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NOT NULL DEFAULT 0,
  `update_time` datetime(0) NOT NULL,
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否逻辑删除，0:未删除；1:已删除',
  PRIMARY KEY (`action_id`) USING BTREE,
  UNIQUE INDEX `action_code`(`action_code`) USING BTREE,
  UNIQUE INDEX `action_id`(`action_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1164422211189084163 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '系统资源-功能操作' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_action
-- ----------------------------
INSERT INTO `sys_action` VALUES (1131849293404176385, 'open-cloud-base-server', 'systemMenuView', '查看', '', 3, 0, 1, b'1', 0, '2019-05-24 17:07:54', 0, '2019-08-22 14:25:48', b'0');
INSERT INTO `sys_action` VALUES (1131849510572654593, 'open-cloud-base-server', 'systemMenuEdit', '编辑', '', 3, 0, 1, b'1', 0, '2019-05-24 17:08:46', 0, '2019-05-24 17:08:46', b'0');
INSERT INTO `sys_action` VALUES (1131858946338992129, 'open-cloud-base-server', 'systemRoleView', '查看', '', 8, 0, 1, b'1', 0, '2019-05-24 17:46:16', 0, '2019-05-24 17:46:16', b'0');
INSERT INTO `sys_action` VALUES (1131863248310775809, 'open-cloud-base-server', 'systemRoleEdit', '编辑', '', 8, 0, 1, b'1', 0, '2019-05-24 18:03:22', 0, '2019-05-24 18:03:22', b'0');
INSERT INTO `sys_action` VALUES (1131863723722551297, 'open-cloud-base-server', 'systemAppView', '查看', '', 9, 0, 1, b'1', 0, '2019-05-24 18:05:15', 0, '2019-05-24 18:05:15', b'0');
INSERT INTO `sys_action` VALUES (1131863775899693057, 'open-cloud-base-server', 'systemAppEdit', '编辑', '', 9, 0, 1, b'1', 0, '2019-05-24 18:05:27', 0, '2019-05-24 18:05:27', b'0');
INSERT INTO `sys_action` VALUES (1131864400507056130, 'open-cloud-base-server', 'systemUserView', '查看', '', 10, 0, 1, b'1', 0, '2019-05-24 18:07:56', 0, '2019-05-24 18:07:56', b'0');
INSERT INTO `sys_action` VALUES (1131864444878598146, 'open-cloud-base-server', 'systemUserEdit', '编辑', '', 10, 0, 1, b'1', 0, '2019-05-24 18:08:07', 0, '2019-05-24 18:08:07', b'0');
INSERT INTO `sys_action` VALUES (1131864827252322305, 'open-cloud-base-server', 'gatewayIpLimitView', '查看', '', 2, 0, 1, b'1', 0, '2019-05-24 18:09:38', 0, '2019-05-24 18:09:38', b'0');
INSERT INTO `sys_action` VALUES (1131864864267055106, 'open-cloud-base-server', 'gatewayIpLimitEdit', '编辑', '', 2, 0, 1, b'1', 0, '2019-05-24 18:09:47', 0, '2019-05-24 18:09:47', b'0');
INSERT INTO `sys_action` VALUES (1131865040289411074, 'open-cloud-base-server', 'gatewayRouteView', '查看', '', 5, 0, 1, b'1', 0, '2019-05-24 18:10:29', 0, '2019-05-24 18:10:29', b'0');
INSERT INTO `sys_action` VALUES (1131865075609645057, 'open-cloud-base-server', 'gatewayRouteEdit', '编辑', '', 5, 0, 1, b'1', 0, '2019-05-24 18:10:37', 0, '2019-05-24 18:10:37', b'0');
INSERT INTO `sys_action` VALUES (1131865482314526722, 'open-cloud-base-server', 'systemApiView', '查看', '', 6, 0, 1, b'1', 0, '2019-05-24 18:12:14', 0, '2019-05-24 18:12:14', b'0');
INSERT INTO `sys_action` VALUES (1131865520738545666, 'open-cloud-base-server', 'systemApiEdit', '编辑', '', 6, 0, 1, b'1', 0, '2019-05-24 18:12:23', 0, '2019-05-24 18:12:23', b'0');
INSERT INTO `sys_action` VALUES (1131865772929462274, 'open-cloud-base-server', 'gatewayLogsView', '查看', '', 12, 0, 1, b'1', 0, '2019-05-24 18:13:23', 0, '2019-05-24 18:13:23', b'0');
INSERT INTO `sys_action` VALUES (1131865931146997761, 'open-cloud-base-server', 'gatewayRateLimitView', '查看', '', 14, 0, 1, b'1', 0, '2019-05-24 18:14:01', 0, '2019-05-24 18:14:01', b'0');
INSERT INTO `sys_action` VALUES (1131865974704844802, 'open-cloud-base-server', 'gatewayRateLimitEdit', '编辑', '', 14, 0, 1, b'1', 0, '2019-05-24 18:14:12', 0, '2019-05-24 18:14:12', b'0');
INSERT INTO `sys_action` VALUES (1131866278187905026, 'open-cloud-base-server', 'jobView', '查看', '', 16, 0, 1, b'1', 0, '2019-05-24 18:15:24', 0, '2019-05-25 03:23:15', b'0');
INSERT INTO `sys_action` VALUES (1131866310622457857, 'open-cloud-base-server', 'jobEdit', '编辑', '', 16, 0, 1, b'1', 0, '2019-05-24 18:15:32', 0, '2019-05-25 03:23:21', b'0');
INSERT INTO `sys_action` VALUES (1131866943459045377, 'open-cloud-base-server', 'schedulerLogsView', '查看', '', 19, 0, 1, b'1', 0, '2019-05-24 18:18:03', 0, '2019-05-24 18:18:03', b'0');
INSERT INTO `sys_action` VALUES (1131867094479155202, 'open-cloud-base-server', 'notifyHttpLogsView', '查看', '', 18, 0, 1, b'1', 0, '2019-05-24 18:18:39', 0, '2019-05-24 18:18:39', b'0');
INSERT INTO `sys_action` VALUES (1164422088547635202, 'open-cloud-base-server', 'developerView', '查看', '', 1149253733673287682, 0, 1, b'0', 0, '2019-08-22 14:20:34', 0, '2019-08-22 14:24:53', b'0');
INSERT INTO `sys_action` VALUES (1164422211189084162, 'open-cloud-base-server', 'developerEdit', '编辑', '', 1149253733673287682, 0, 1, b'0', 0, '2019-08-22 14:21:04', 0, '2019-08-22 14:21:04', b'0');

-- ----------------------------
-- Table structure for sys_api
-- ----------------------------
DROP TABLE IF EXISTS `sys_api`;
CREATE TABLE `sys_api`  (
  `api_id` bigint(20) NOT NULL COMMENT '接口ID',
  `api_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '接口编码',
  `api_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '接口名称',
  `api_category` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'default' COMMENT '接口分类:default-默认分类',
  `api_desc` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '资源描述',
  `request_method` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '请求方式',
  `content_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '响应类型',
  `service_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '服务ID',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '请求路径',
  `priority` int(11) NOT NULL DEFAULT 0 COMMENT '优先级',
  `status` tinyint(3) NOT NULL DEFAULT 1 COMMENT '状态:0-无效 1-有效',
  `is_persist` bit(1) NOT NULL DEFAULT b'0' COMMENT '保留数据0-否 1-是 不允许删除',
  `is_auth` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否需要认证: 0-无认证 1-身份认证 默认:1',
  `is_open` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否公开: 0-内部的 1-公开的',
  `class_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '类名',
  `method_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '方法名',
  `create_user_id` bigint(20) NULL DEFAULT 0 COMMENT '创建用户id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT 0 COMMENT '更新用户id',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `is_delete` bit(1) NULL DEFAULT b'0' COMMENT '是否逻辑删除，0:未删除；1:已删除',
  PRIMARY KEY (`api_id`) USING BTREE,
  UNIQUE INDEX `api_code`(`api_code`) USING BTREE,
  UNIQUE INDEX `api_id`(`api_id`) USING BTREE,
  INDEX `service_id`(`service_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '系统资源-API接口' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_api
-- ----------------------------
INSERT INTO `sys_api` VALUES (1, 'all', '全部', 'default', '所有请求', 'get,post', NULL, 'open-cloud-api-zuul-server', '/**', 0, 1, b'1', b'1', b'1', NULL, NULL, 0, '2019-03-07 21:52:17', 0, '2019-03-14 21:41:28', NULL);
INSERT INTO `sys_api` VALUES (2, 'actuator', '监控端点', 'default', '监控端点', 'post', NULL, 'open-cloud-api-zuul-server', '/actuator/**', 0, 1, b'1', b'1', b'1', NULL, NULL, 0, '2019-03-07 21:52:17', 0, '2019-03-14 21:41:28', NULL);
INSERT INTO `sys_api` VALUES (1149168010337116161, '9a1e16647d1e534f7b06ed9db83719fa', '获取功能按钮详情', 'default', '获取功能按钮详情', 'GET', '', 'open-cloud-base-server', '/action/{actionId}/info', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseActionController', 'getAction', 0, '2019-07-11 12:06:18', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1149168010500694017, '26128c7aafae30020bcac2c47631497a', '获取分页功能按钮列表', 'default', '获取分页功能按钮列表', 'GET', '', 'open-cloud-base-server', '/action', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseActionController', 'findActionListPage', 0, '2019-07-11 12:06:18', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1149168010571997185, '84d30b0aab28de096779d6f8bee960e7', '添加功能按钮', 'default', '添加功能按钮', 'POST', '', 'open-cloud-base-server', '/action/add', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseActionController', 'addAction', 0, '2019-07-11 12:06:18', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1149168010655883266, '756154d7c8c6a79f02da79d9148beb9f', '编辑功能按钮', 'default', '添加功能按钮', 'POST', '', 'open-cloud-base-server', '/action/update', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseActionController', 'updateAction', 0, '2019-07-11 12:06:18', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1149168010722992129, '92187c259636fdf5d4f108b688fc44ee', '移除功能按钮', 'default', '移除功能按钮', 'POST', '', 'open-cloud-base-server', '/action/remove', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseActionController', 'removeAction', 0, '2019-07-11 12:06:18', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1149168010794295298, '214e294809bb1f22abc70c38959e1343', '添加接口资源', 'default', '添加接口资源', 'POST', '', 'open-cloud-base-server', '/api/add', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseApiController', 'addApi', 0, '2019-07-11 12:06:18', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1149168010861404161, 'a0f00dd477c9232a51b94553ad7a7803', '移除接口资源', 'default', '移除接口资源', 'POST', '', 'open-cloud-base-server', '/api/remove', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseApiController', 'removeApi', 0, '2019-07-11 12:06:18', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1149168010920124418, '6311ab783c8818cd5918e5d903fb371e', '编辑接口资源', 'default', '编辑接口资源', 'POST', '', 'open-cloud-base-server', '/api/update', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseApiController', 'updateApi', 0, '2019-07-11 12:06:18', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1149168010991427586, 'aa37236289cedba711c8f648a94b1105', '获取分页接口列表', 'default', '获取分页接口列表', 'GET', '', 'open-cloud-base-server', '/api', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseApiController', 'getApiList', 0, '2019-07-11 12:06:18', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1149168011045953537, 'e18bae5b2fc727e77d36a3f4697abf0f', '获取所有接口列表', 'default', '获取所有接口列表', 'GET', '', 'open-cloud-base-server', '/api/all', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseApiController', 'getApiAllList', 0, '2019-07-11 12:06:18', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1149168011100479489, 'd4c4c1b5b3847cc63b52ed9007698325', '获取接口资源', 'default', '获取接口资源', 'GET', '', 'open-cloud-base-server', '/api/{apiId}/info', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseApiController', 'getApi', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1149168011163394050, '9e054a7dc4194e7635c2686e1109aa49', '删除应用信息', 'default', '删除应用信息', 'POST', '', 'open-cloud-base-server', '/app/remove', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseAppController', 'removeApp', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1149168011217920002, 'fa32726ed4076d44cae0023132f7014f', '获取应用详情', 'default', '获取应用详情', 'GET', '', 'open-cloud-base-server', '/app/{appId}/info', 0, 1, b'1', b'0', b'1', 'com.opencloud.base.server.controller.BaseAppController', 'getApp', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1149168011280834562, 'c5863e8ad8a5605354b9a122511ffa01', '添加应用信息', 'default', '添加应用信息', 'POST', '', 'open-cloud-base-server', '/app/add', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseAppController', 'addApp', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168011331166209, 'a23793da67076d4be853daa422943c53', '编辑应用信息', 'default', '编辑应用信息', 'POST', '', 'open-cloud-base-server', '/app/update', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseAppController', 'updateApp', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168011452801025, '93c006692e386f96555e7edece91c994', '重置应用秘钥', 'default', '重置应用秘钥', 'POST', '', 'open-cloud-base-server', '/app/reset', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseAppController', 'resetAppSecret', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1149168011524104194, '385d34ec827de818225a981d737f307a', '获取应用开发配置信息', 'default', '获取应用开发配置信息', 'GET', '', 'open-cloud-base-server', '/app/client/{clientId}/info', 0, 1, b'1', b'0', b'1', 'com.opencloud.base.server.controller.BaseAppController', 'getAppClientInfo', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1149168011578630146, 'ccb6139e4beb2a84e29f399215bfedc0', '获取分页应用列表', 'default', '获取分页应用列表', 'GET', '', 'open-cloud-base-server', '/app', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseAppController', 'getAppListPage', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1149168011628961794, 'f09f5543ff4c0ceab04b99ddc5a94bc2', '完善应用开发信息', 'default', '完善应用开发信息', 'POST', '', 'open-cloud-base-server', '/app/client/update', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseAppController', 'updateAppClientInfo', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168011687682050, '331bddbcd34cacdd74f80b48e3484017', '获取接口权限列表', 'default', '获取接口权限列表', 'GET', '', 'open-cloud-base-server', '/authority/api', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseAuthorityController', 'findAuthorityApi', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168011742208002, '785b2368bf67d1a942ea64d946c70144', '获取应用已分配接口权限', 'default', '获取应用已分配接口权限', 'GET', '', 'open-cloud-base-server', '/authority/app', 0, 1, b'1', b'0', b'1', 'com.opencloud.base.server.controller.BaseAuthorityController', 'findAuthorityApp', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168011805122561, '282c563cbfb922ac6328c058051d31a4', '分配角色权限', 'default', '分配角色权限', 'POST', '', 'open-cloud-base-server', '/authority/role/grant', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseAuthorityController', 'grantAuthorityRole', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168011851259906, 'f2be18d77a78f728df355f32e90cd780', '分配应用权限', 'default', '分配应用权限', 'POST', '', 'open-cloud-base-server', '/authority/app/grant', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseAuthorityController', 'grantAuthorityApp', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168011909980161, '4611bd638b538605286600dbc899cbb0', '功能按钮授权', 'default', '功能按钮授权', 'POST', '', 'open-cloud-base-server', '/authority/action/grant', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseAuthorityController', 'grantAuthorityAction', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168011960311809, '5262f2214a4ff43179c947dc458213d7', '获取功能权限列表', 'default', '获取功能权限列表', 'GET', '', 'open-cloud-base-server', '/authority/action', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseAuthorityController', 'findAuthorityAction', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012010643458, '8a6561a9a0efa7f5f9faadb6e39dc927', '获取用户已分配权限', 'default', '获取用户已分配权限', 'GET', '', 'open-cloud-base-server', '/authority/user', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseAuthorityController', 'findAuthorityUser', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012060975106, '1df7911bb85d15f51bcd78c43f18b24d', '获取菜单权限列表', 'default', '获取菜单权限列表', 'GET', '', 'open-cloud-base-server', '/authority/menu', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseAuthorityController', 'findAuthorityMenu', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012111306753, '80256cc196e1e6d2437289e038b13b8a', '获取所有访问权限列表', 'default', '获取所有访问权限列表', 'GET', '', 'open-cloud-base-server', '/authority/access', 0, 1, b'1', b'0', b'1', 'com.opencloud.base.server.controller.BaseAuthorityController', 'findAuthorityResource', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012161638401, '5586d5f337d4ebf99e551fdbf55d1116', '获取角色已分配权限', 'default', '获取角色已分配权限', 'GET', '', 'open-cloud-base-server', '/authority/role', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseAuthorityController', 'findAuthorityRole', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012216164353, 'b96a9514487f7e7679fe39c9254ab972', '分配用户权限', 'default', '分配用户权限', 'POST', '', 'open-cloud-base-server', '/authority/user/grant', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseAuthorityController', 'grantAuthorityUser', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012270690305, '3a1c4ef17565d863f35a3bccd7cc035e', '添加系统用户', 'default', '添加系统用户', 'POST', '', 'open-cloud-base-server', '/developer/add', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseDeveloperController', 'addUser', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012316827649, '734d1179e332309b9de3927894864396', '更新系统用户', 'default', '更新系统用户', 'POST', '', 'open-cloud-base-server', '/developer/update', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseDeveloperController', 'updateUser', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012421685249, '39ad69234435595ee67836e8a03d6ea6', '获取所有用户列表', 'default', '获取所有用户列表', 'GET', '', 'open-cloud-base-server', '/developer/all', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseDeveloperController', 'getUserAllList', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012467822593, '50520c56c7714aa9be9a808cd30fd9bb', '获取账号登录信息', 'default', '仅限系统内部调用', 'POST', '', 'open-cloud-base-server', '/developer/login', 0, 1, b'1', b'0', b'1', 'com.opencloud.base.server.controller.BaseDeveloperController', 'developerLogin', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012522348546, '9aca4415969b60158f383b2b49b8fe79', '修改用户密码', 'default', '修改用户密码', 'POST', '', 'open-cloud-base-server', '/developer/update/password', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseDeveloperController', 'updatePassword', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012576874498, 'c4c154b6ffa4b21835db1e09614c73e6', '系统分页用户列表', 'default', '系统分页用户列表', 'GET', '', 'open-cloud-base-server', '/developer', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseDeveloperController', 'getUserList', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012623011841, '4a13f24fa2d5cf74a6e408e82182e1b1', '注册第三方系统登录账号', 'default', '仅限系统内部调用', 'POST', '', 'open-cloud-base-server', '/developer/add/thirdParty', 0, 1, b'1', b'0', b'0', 'com.opencloud.base.server.controller.BaseDeveloperController', 'addDeveloperThirdParty', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012673343490, '2901b2b8040f1c44c0318650fbfd4460', '获取分页菜单资源列表', 'default', '获取分页菜单资源列表', 'GET', '', 'open-cloud-base-server', '/menu', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseMenuController', 'getMenuListPage', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012719480834, '87049d9db5261f50d524c45a32044415', '获取菜单资源详情', 'default', '获取菜单资源详情', 'GET', '', 'open-cloud-base-server', '/menu/{menuId}/info', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseMenuController', 'getMenu', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012769812481, '50bcf72df4a18a6a8c46a7076a7a014a', '移除菜单资源', 'default', '移除菜单资源', 'POST', '', 'open-cloud-base-server', '/menu/remove', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseMenuController', 'removeMenu', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012820144129, '638be151d314740a2f2aaebaaba3f479', '菜单所有资源列表', 'default', '菜单所有资源列表', 'GET', '', 'open-cloud-base-server', '/menu/all', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseMenuController', 'getMenuAllList', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012870475777, 'e3c1b0b860591e11b50d6dccc9e2fa6a', '获取菜单下所有操作', 'default', '获取菜单下所有操作', 'GET', '', 'open-cloud-base-server', '/menu/action', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseMenuController', 'getMenuAction', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012920807425, 'bbc5546a6cfa1161859b69ef5754eace', '编辑菜单资源', 'default', '编辑菜单资源', 'POST', '', 'open-cloud-base-server', '/menu/update', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseMenuController', 'updateMenu', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168012979527682, '720f3b2571fe2850108f090408514cae', '添加菜单资源', 'default', '添加菜单资源', 'POST', '', 'open-cloud-base-server', '/menu/add', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseMenuController', 'addMenu', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168013029859330, '8f2b70d4b55f13eaf82c4cf687903ced', '获取角色详情', 'default', '获取角色详情', 'GET', '', 'open-cloud-base-server', '/role/{roleId}/info', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseRoleController', 'getRole', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168013071802369, 'd9fc987468fd4c73350653d5c4e53453', '角色添加成员', 'default', '角色添加成员', 'POST', '', 'open-cloud-base-server', '/role/users/add', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseRoleController', 'addUserRoles', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013122134017, 'f4e83562a36e943883c5eb6b7376e365', '查询角色成员', 'default', '查询角色成员', 'GET', '', 'open-cloud-base-server', '/role/users', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseRoleController', 'getRoleUsers', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013176659970, '9b57678bc96af393c0c60e135acc90a9', '获取所有角色列表', 'default', '获取所有角色列表', 'GET', '', 'open-cloud-base-server', '/role/all', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseRoleController', 'getRoleAllList', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013226991617, '4693b9a455022754623dd25f9905c19b', '获取分页角色列表', 'default', '获取分页角色列表', 'GET', '', 'open-cloud-base-server', '/role', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseRoleController', 'getRoleListPage', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:19', NULL);
INSERT INTO `sys_api` VALUES (1149168013277323266, '51ea3146eaebdd33fd065b029ee147fb', '添加角色', 'default', '添加角色', 'POST', '', 'open-cloud-base-server', '/role/add', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseRoleController', 'addRole', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013327654913, 'b4cdd238a7e34e1794988d00c4d45779', '编辑角色', 'default', '编辑角色', 'POST', '', 'open-cloud-base-server', '/role/update', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseRoleController', 'updateRole', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013373792257, '610b499835de759e37429ece2de1ee33', '删除角色', 'default', '删除角色', 'POST', '', 'open-cloud-base-server', '/role/remove', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseRoleController', 'removeRole', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013419929601, '334d10291e4061c07a180546dd9cedea', '添加系统用户', 'default', '添加系统用户', 'POST', '', 'open-cloud-base-server', '/user/add', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseUserController', 'addUser', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013466066945, 'd722dc79f9c8d3ece3b9324a03caf902', '更新系统用户', 'default', '更新系统用户', 'POST', '', 'open-cloud-base-server', '/user/update', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseUserController', 'updateUser', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013512204290, 'c8293b83e96ca425be2e24f0cfa8c7bf', '获取所有用户列表', 'default', '获取所有用户列表', 'GET', '', 'open-cloud-base-server', '/user/all', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseUserController', 'getUserAllList', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013562535938, '3fdb78cf597834182b4c6b558a800774', '修改用户密码', 'default', '修改用户密码', 'POST', '', 'open-cloud-base-server', '/user/update/password', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseUserController', 'updatePassword', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013608673282, 'dc01f78458d3fe0cff028da861bdb65b', '系统分页用户列表', 'default', '系统分页用户列表', 'GET', '', 'open-cloud-base-server', '/user', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseUserController', 'getUserList', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013659004929, '25c851a0698d161f2b00bdcf74668648', '用户分配角色', 'default', '用户分配角色', 'POST', '', 'open-cloud-base-server', '/user/roles/add', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseUserController', 'addUserRoles', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013700947969, '86197fb92e8fd3359842cf511c68be8c', '获取账号登录信息', 'default', '仅限系统内部调用', 'POST', '', 'open-cloud-base-server', '/user/login', 0, 1, b'1', b'0', b'1', 'com.opencloud.base.server.controller.BaseUserController', 'userLogin', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013747085313, 'db9fbb85ee040fd2f8981e47a6873614', '获取用户已分配角色', 'default', '获取用户已分配角色', 'GET', '', 'open-cloud-base-server', '/user/roles', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseUserController', 'getUserRoles', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013797416961, 'bbd37ea32bf91697909dd261befa8a5f', '注册第三方系统登录账号', 'default', '仅限系统内部调用', 'POST', '', 'open-cloud-base-server', '/user/add/thirdParty', 0, 1, b'1', b'0', b'1', 'com.opencloud.base.server.controller.BaseUserController', 'addUserThirdParty', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013843554306, 'b0d37206d8ebf60a7359e1b15d79b361', '修改当前登录用户密码', 'default', '修改当前登录用户密码', 'GET', '', 'open-cloud-base-server', '/current/user/rest/password', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.CurrentUserController', 'restPassword', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013889691650, '88fa6cc7c4fa7f0fb2b8d3df951fdfa9', '修改当前登录用户基本信息', 'default', '修改当前登录用户基本信息', 'POST', '', 'open-cloud-base-server', '/current/user/update', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.CurrentUserController', 'updateUserInfo', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013940023298, '902ca6aa4d96c21a48cabe5363a4b3dd', '获取当前登录用户已分配菜单权限', 'default', '获取当前登录用户已分配菜单权限', 'GET', '', 'open-cloud-base-server', '/current/user/menu', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.CurrentUserController', 'findAuthorityMenu', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168013994549249, '0f86b27eb3b0fcd3e714e22d43b1172b', '获取分页访问日志列表', 'default', '获取分页访问日志列表', 'GET', '', 'open-cloud-base-server', '/gateway/access/logs', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayAccessLogsController', 'getAccessLogListPage', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168014036492290, '83ad6d5e0903bf19cfa28c7a83595de2', '获取接口白名单列表', 'default', '仅限内部调用', 'GET', '', 'open-cloud-base-server', '/gateway/api/whiteList', 0, 1, b'1', b'0', b'0', 'com.opencloud.base.server.controller.GatewayController', 'getApiWhiteList', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168014082629634, '6eb627d08d2f0aee74bfd7c9b53b1124', '获取接口黑名单列表', 'default', '仅限内部调用', 'GET', '', 'open-cloud-base-server', '/gateway/api/blackList', 0, 1, b'1', b'0', b'0', 'com.opencloud.base.server.controller.GatewayController', 'getApiBlackList', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168014128766978, '3fde01046c8643e9b536ae6f61d4431f', '获取路由列表', 'default', '仅限内部调用', 'GET', '', 'open-cloud-base-server', '/gateway/api/route', 0, 1, b'1', b'0', b'0', 'com.opencloud.base.server.controller.GatewayController', 'getApiRouteList', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168014170710018, '2cfc3bbc760608bda59d68b2c0b4e2fd', '获取限流列表', 'default', '仅限内部调用', 'GET', '', 'open-cloud-base-server', '/gateway/api/rateLimit', 0, 1, b'1', b'0', b'0', 'com.opencloud.base.server.controller.GatewayController', 'getApiRateLimitList', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168014212653057, 'f5abed6e35a7a1715fac80841e4e592f', '添加IP限制', 'default', '添加IP限制', 'POST', '', 'open-cloud-base-server', '/gateway/limit/ip/add', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayIpLimitController', 'addIpLimit', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168014258790401, 'c584e31594bc5bedfc959c729a2e90e7', '移除IP限制', 'default', '移除IP限制', 'POST', '', 'open-cloud-base-server', '/gateway/limit/ip/remove', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayIpLimitController', 'removeIpLimit', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168014405591042, '24b5559e0204223af9b73f3c68ee68f3', '获取IP限制', 'default', '获取IP限制', 'GET', '', 'open-cloud-base-server', '/gateway/limit/ip/{policyId}/info', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayIpLimitController', 'getIpLimit', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168014451728385, '160a9fb0ce4243b989bbaaac031a345e', '编辑IP限制', 'default', '编辑IP限制', 'POST', '', 'open-cloud-base-server', '/gateway/limit/ip/update', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayIpLimitController', 'updateIpLimit', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168014497865729, '99a30e8643b0820a04ecbdcf7c9628af', '绑定API', 'default', '一个API只能绑定一个策略', 'POST', '', 'open-cloud-base-server', '/gateway/limit/ip/api/add', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayIpLimitController', 'addIpLimitApis', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168014544003073, '0dfca465c864c74d04a4d6135fee1880', '获取分页接口列表', 'default', '获取分页接口列表', 'GET', '', 'open-cloud-base-server', '/gateway/limit/ip', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayIpLimitController', 'getIpLimitListPage', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168014585946113, '2aecc273984a1d3981dc0f62cafbe894', '查询策略已绑定API列表', 'default', '获取分页接口列表', 'GET', '', 'open-cloud-base-server', '/gateway/limit/ip/api/list', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayIpLimitController', 'getIpLimitApiList', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168014636277762, 'b993f44f1ad4dd08bfd4f36730af9ea1', '获取流量控制', 'default', '获取流量控制', 'GET', '', 'open-cloud-base-server', '/gateway/limit/rate/{policyId}/info', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayRateLimitController', 'getRateLimit', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:21', NULL);
INSERT INTO `sys_api` VALUES (1149168014686609409, 'ab71840616ff1f1a619170538b1f1de9', '添加流量控制', 'default', '添加流量控制', 'POST', '', 'open-cloud-base-server', '/gateway/limit/rate/add', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayRateLimitController', 'addRateLimit', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:21', NULL);
INSERT INTO `sys_api` VALUES (1149168014732746753, '5f512e5065e80c8d0ffd6be4f27c386e', '编辑流量控制', 'default', '编辑流量控制', 'POST', '', 'open-cloud-base-server', '/gateway/limit/rate/update', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayRateLimitController', 'updateRateLimit', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168014774689793, 'f83ecfdd9325be59c60627f0164ec8a8', '绑定API', 'default', '一个API只能绑定一个策略', 'POST', '', 'open-cloud-base-server', '/gateway/limit/rate/api/add', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayRateLimitController', 'addRateLimitApis', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:21', NULL);
INSERT INTO `sys_api` VALUES (1149168014816632833, '9c565ce4ded10ccd227001f6d7eab11c', '移除流量控制', 'default', '移除流量控制', 'POST', '', 'open-cloud-base-server', '/gateway/limit/rate/remove', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayRateLimitController', 'removeRateLimit', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168014862770178, '0830e3c6c3d42f11d86f5e2ed0d99985', '查询策略已绑定API列表', 'default', '获取分页接口列表', 'GET', '', 'open-cloud-base-server', '/gateway/limit/rate/api/list', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayRateLimitController', 'getRateLimitApiList', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:21', NULL);
INSERT INTO `sys_api` VALUES (1149168014908907521, '8dff62591ccefba3537b84aa9d72d7b7', '获取分页接口列表', 'default', '获取分页接口列表', 'GET', '', 'open-cloud-base-server', '/gateway/limit/rate', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayRateLimitController', 'getRateLimitListPage', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:20', NULL);
INSERT INTO `sys_api` VALUES (1149168014955044866, '382e5fe59fdc31f240b5e42b2ae1a522', '获取分页路由列表', 'default', '获取分页路由列表', 'GET', '', 'open-cloud-base-server', '/gateway/route', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayRouteController', 'getRouteListPage', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:21', NULL);
INSERT INTO `sys_api` VALUES (1149168015001182210, '177b640de1b90d39536f462f24e3051f', '获取路由', 'default', '获取路由', 'GET', '', 'open-cloud-base-server', '/gateway/route/{routeId}/info', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayRouteController', 'getRoute', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:21', NULL);
INSERT INTO `sys_api` VALUES (1149168015055708162, '1307447addfddf7e075eea74100c150d', '添加路由', 'default', '添加路由', 'POST', '', 'open-cloud-base-server', '/gateway/route/add', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayRouteController', 'addRoute', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:21', NULL);
INSERT INTO `sys_api` VALUES (1149168015118622721, '40a481fc86814de744e91e42c15b6f3e', '移除路由', 'default', '移除路由', 'POST', '', 'open-cloud-base-server', '/gateway/route/remove', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayRouteController', 'removeRoute', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:21', NULL);
INSERT INTO `sys_api` VALUES (1149168015164760065, '61e8c492eb9e56441d1c428ab3e09f66', '编辑路由', 'default', '编辑路由', 'POST', '', 'open-cloud-base-server', '/gateway/route/update', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayRouteController', 'updateRoute', 0, '2019-07-11 12:06:19', 0, '2019-08-19 18:59:21', NULL);
INSERT INTO `sys_api` VALUES (1149168208614449153, '3c28ffa71681fb53f1f458548456256c', '登录获取用户访问令牌', 'default', '基于oauth2密码模式登录,无需签名,返回access_token', 'POST', '', 'open-cloud-uaa-admin-server', '/login/token', 0, 1, b'1', b'0', b'1', 'com.opencloud.uaa.admin.server.controller.LoginController', 'getLoginToken', 0, '2019-07-11 12:07:06', 0, '2019-08-19 18:55:51', NULL);
INSERT INTO `sys_api` VALUES (1149168208677363714, '29c8264c689bce3567708f977779c3b9', '退出并移除令牌', 'default', '退出并移除令牌,令牌将失效', 'POST', '', 'open-cloud-uaa-admin-server', '/logout/token', 0, 1, b'1', b'1', b'1', 'com.opencloud.uaa.admin.server.controller.LoginController', 'removeToken', 0, '2019-07-11 12:07:06', 0, '2019-08-19 18:55:51', NULL);
INSERT INTO `sys_api` VALUES (1149168208740278273, 'f98a1543766b726e4b5a6d7a7f32fd93', '获取当前登录用户信息', 'default', '获取当前登录用户信息', 'GET', '', 'open-cloud-uaa-admin-server', '/current/user', 0, 1, b'1', b'1', b'1', 'com.opencloud.uaa.admin.server.controller.LoginController', 'getUserProfile', 0, '2019-07-11 12:07:06', 0, '2019-08-19 18:55:50', NULL);
INSERT INTO `sys_api` VALUES (1149265710541914114, 'be5eb57a4df27bf8dece0bda751a5a62', '发送邮件', 'default', '发送邮件', 'POST', '', 'open-cloud-msg-server', '/email', 0, 1, b'1', b'0', b'1', 'com.opencloud.msg.server.controller.EmailController', 'send', 0, '2019-07-11 18:34:32', 0, '2019-07-17 18:33:08', NULL);
INSERT INTO `sys_api` VALUES (1149265710646771714, '0273e0d10bea64046178b7c5ff5055cc', '发送短信', 'default', '发送短信', 'POST', 'application/json;charset=UTF-8', 'open-cloud-msg-server', '/sms', 0, 1, b'1', b'0', b'0', 'com.opencloud.msg.server.controller.SmsController', 'send', 0, '2019-07-11 18:34:32', 0, '2019-07-17 18:33:08', NULL);
INSERT INTO `sys_api` VALUES (1149265710743240705, '3b5928629e5398dcc534c548d5c01085', 'Webhook异步通知', 'default', '即时推送，重试通知时间间隔为 5s、10s、2min、5min、10min、30min、1h、2h、6h、15h，直到你正确回复状态 200 并且返回 success 或者超过最大重发次数', 'POST', 'application/json;charset=UTF-8', 'open-cloud-msg-server', '/webhook', 0, 1, b'1', b'0', b'0', 'com.opencloud.msg.server.controller.WebHookController', 'send', 0, '2019-07-11 18:34:32', 0, '2019-07-17 18:33:08', NULL);
INSERT INTO `sys_api` VALUES (1149265710839709697, 'df78a9afa62bb2d1165aeea14aca9e41', '获取分页异步通知列表', 'default', '获取分页异步通知列表', 'GET', '', 'open-cloud-msg-server', '/webhook/logs', 0, 1, b'1', b'0', b'0', 'com.opencloud.msg.server.controller.WebHookController', 'getLogsListPage', 0, '2019-07-11 18:34:32', 0, '2019-07-17 18:33:08', NULL);
INSERT INTO `sys_api` VALUES (1151324453580460033, '4fdfe1d02c78379a3a8eb2a84b0e8b82', '搜索', 'default', '搜索', 'POST', '', 'refuse_classify_server', '/items/search', 0, 1, b'1', b'0', b'0', 'com.classify.server.controller.ItemsController', 'search', 0, '2019-07-17 10:55:14', 0, '2019-07-17 10:55:14', NULL);
INSERT INTO `sys_api` VALUES (1151324453773398018, '92eabcb8d4d4d587343cc2b6e11ccecc', '同步数据', 'default', '同步数据', 'POST', '', 'refuse_classify_server', '/items/sync', 0, 1, b'1', b'0', b'0', 'com.classify.server.controller.ItemsController', 'sync', 0, '2019-07-17 10:55:15', 0, '2019-07-17 10:55:15', NULL);
INSERT INTO `sys_api` VALUES (1151324453869867009, 'd094367240c4c6847eeda0fe2317069c', '语音识别搜索', 'default', '语音识别搜索', 'POST', '', 'refuse_classify_server', '/items/search/speech', 0, 1, b'1', b'0', b'0', 'com.classify.server.controller.ItemsController', 'searchSpeech', 0, '2019-07-17 10:55:15', 0, '2019-07-17 10:55:15', NULL);
INSERT INTO `sys_api` VALUES (1151324453974724609, 'd26205d1d1e42d925ea972a6d034ba15', '热门搜索词', 'default', '热门搜索词', 'POST', '', 'refuse_classify_server', '/items/hot', 0, 1, b'1', b'0', b'0', 'com.classify.server.controller.ItemsController', 'hot', 0, '2019-07-17 10:55:15', 0, '2019-07-17 10:55:15', NULL);
INSERT INTO `sys_api` VALUES (1151324454205411329, '5b2390e91198ae02e58b4704e23f7a45', '图像识别搜索', 'default', '图像识别搜索', 'POST', '', 'refuse_classify_server', '/items/search/image', 0, 1, b'1', b'0', b'0', 'com.classify.server.controller.ItemsController', 'searchImage', 0, '2019-07-17 10:55:15', 0, '2019-07-17 10:55:15', NULL);
INSERT INTO `sys_api` VALUES (1151324462224920577, 'f17677d7f0f33851333de6bc48d5ac1a', '获取服务列表', 'default', '获取服务列表', 'GET', '', 'open-cloud-base-server', '/gateway/service/list', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.GatewayController', 'getServiceList', 0, '2019-07-17 10:55:17', 0, '2019-08-20 14:27:16', NULL);
INSERT INTO `sys_api` VALUES (1151395583808917505, '042dd26520a30c0fecd13ee015a79be5', '发送模板邮件', 'default', '发送模板邮件', 'POST', '', 'open-cloud-msg-server', '/email/tpl', 0, 1, b'1', b'0', b'1', 'com.opencloud.msg.server.controller.EmailController', 'sendByTpl', 0, '2019-07-17 15:37:53', 0, '2019-07-17 18:33:08', NULL);
INSERT INTO `sys_api` VALUES (1151426336538128385, '93ff44fd4adab6c4097327eb5ffd405a', '获取任务列表', 'default', '获取任务列表', 'GET', '', 'open-cloud-task-server', '/job', 0, 1, b'1', b'1', b'1', 'com.opencloud.task.server.controller.SchedulerController', 'getJobList', 0, '2019-07-17 17:40:05', 0, '2019-07-17 18:25:46', NULL);
INSERT INTO `sys_api` VALUES (1151426336915615746, '030aae5c245d4d780ca248fdd5e4bb67', '恢复任务', 'default', '恢复任务', 'POST', '', 'open-cloud-task-server', '/job/resume', 0, 1, b'1', b'1', b'1', 'com.opencloud.task.server.controller.SchedulerController', 'resumeJob', 0, '2019-07-17 17:40:05', 0, '2019-07-17 18:25:46', NULL);
INSERT INTO `sys_api` VALUES (1151426338278764545, '7f542a952be1a9d8b089330f5f0aba3b', '删除任务', 'default', '删除任务', 'POST', '', 'open-cloud-task-server', '/job/delete', 0, 1, b'1', b'1', b'1', 'com.opencloud.task.server.controller.SchedulerController', 'deleteJob', 0, '2019-07-17 17:40:06', 0, '2019-07-17 18:25:46', NULL);
INSERT INTO `sys_api` VALUES (1151426338278764547, 'b9f2f4ed495f3f71da31c14e8ad2193d', '暂停任务', 'default', '暂停任务', 'POST', '', 'open-cloud-task-server', '/job/pause', 0, 1, b'1', b'1', b'1', 'com.opencloud.task.server.controller.SchedulerController', 'pauseJob', 0, '2019-07-17 17:40:06', 0, '2019-07-17 18:25:46', NULL);
INSERT INTO `sys_api` VALUES (1151426338278764549, '14467518667ccfa46541736433d0efa2', '获取任务执行日志列表', 'default', '获取任务执行日志列表', 'GET', '', 'open-cloud-task-server', '/job/logs', 0, 1, b'1', b'1', b'1', 'com.opencloud.task.server.controller.SchedulerController', 'getJobLogList', 0, '2019-07-17 17:40:06', 0, '2019-07-17 18:25:46', NULL);
INSERT INTO `sys_api` VALUES (1151426338756915201, '903acc6b8c02184170bd3f316cd0f0c3', '添加远程调度任务', 'default', '添加远程调度任务', 'POST', '', 'open-cloud-task-server', '/job/add/http', 0, 1, b'1', b'1', b'1', 'com.opencloud.task.server.controller.SchedulerController', 'addHttpJob', 0, '2019-07-17 17:40:06', 0, '2019-07-17 18:25:47', NULL);
INSERT INTO `sys_api` VALUES (1151426338903715841, 'cb8aded7fdffd0e8e86940f540b32472', '修改远程调度任务', 'default', '修改远程调度任务', 'POST', '', 'open-cloud-task-server', '/job/update/http', 0, 1, b'1', b'1', b'1', 'com.opencloud.task.server.controller.SchedulerController', 'updateHttpJob', 0, '2019-07-17 17:40:06', 0, '2019-07-17 18:25:47', NULL);
INSERT INTO `sys_api` VALUES (1153543688817876993, '8aedf1bcd8ebc00a41361067602e4866', '获取所有表信息', 'default', '获取所有表信息', 'GET', '', 'open-cloud-generator-server', '/tables', 0, 1, b'1', b'1', b'0', 'com.opencloud.generator.server.controller.GenerateController', 'tables', 0, '2019-07-23 13:53:41', 0, '2019-07-23 13:53:41', NULL);
INSERT INTO `sys_api` VALUES (1153543700121526273, '9ae39e2a7bb1f2502dcd5f4d280b739d', 'swaggerResources', 'default', '', '', '', 'open-cloud-base-server', '/swagger-resources', 0, 1, b'1', b'1', b'0', 'springfox.documentation.swagger.web.ApiResourceController', 'swaggerResources', 0, '2019-07-23 13:53:44', 0, '2019-08-19 18:59:21', NULL);
INSERT INTO `sys_api` VALUES (1153543700201218049, '8e25fdc700610c63d638c55545441119', 'uiConfiguration', 'default', '', '', '', 'open-cloud-base-server', '/swagger-resources/configuration/ui', 0, 1, b'1', b'1', b'0', 'springfox.documentation.swagger.web.ApiResourceController', 'uiConfiguration', 0, '2019-07-23 13:53:44', 0, '2019-08-19 18:59:21', NULL);
INSERT INTO `sys_api` VALUES (1153543700285104130, '0cea701426acbc8348e64ecb598a8bf7', 'securityConfiguration', 'default', '', '', '', 'open-cloud-base-server', '/swagger-resources/configuration/security', 0, 1, b'1', b'1', b'0', 'springfox.documentation.swagger.web.ApiResourceController', 'securityConfiguration', 0, '2019-07-23 13:53:44', 0, '2019-08-19 18:59:21', NULL);
INSERT INTO `sys_api` VALUES (1153543700398350338, '604809625765c7df1e1e8ee8b7e7fe81', 'error', 'default', '', '', '', 'open-cloud-base-server', '/error', 0, 1, b'1', b'1', b'0', 'org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController', 'error', 0, '2019-07-23 13:53:44', 0, '2019-08-19 18:59:21', NULL);
INSERT INTO `sys_api` VALUES (1153544494501732354, '09e8e7b093a0fb008071c09bf440fda1', 'handleError', 'default', '', '', '', 'open-cloud-uaa-admin-server', '/oauth/error', 0, 1, b'1', b'0', b'0', 'com.opencloud.uaa.admin.server.controller.IndexController', 'handleError', 0, '2019-07-23 13:56:53', 0, '2019-08-19 18:55:50', NULL);
INSERT INTO `sys_api` VALUES (1153544494598201345, '1f7307e3c2809aff5437f3594594c8ca', 'welcome', 'default', '', 'GET', '', 'open-cloud-uaa-admin-server', '/', 0, 1, b'1', b'1', b'0', 'com.opencloud.uaa.admin.server.controller.IndexController', 'welcome', 0, '2019-07-23 13:56:53', 0, '2019-08-19 18:55:50', NULL);
INSERT INTO `sys_api` VALUES (1153544494694670337, '4d27a9ae885d7dbfc82e7ae2c86ec9d6', 'login', 'default', '', 'GET', '', 'open-cloud-uaa-admin-server', '/login', 0, 1, b'1', b'0', b'0', 'com.opencloud.uaa.admin.server.controller.IndexController', 'login', 0, '2019-07-23 13:56:54', 0, '2019-08-19 18:55:50', NULL);
INSERT INTO `sys_api` VALUES (1153544494765973506, 'c235abccc85e47cda9710697c59a4aa7', 'confirm_access', 'default', '', '', '', 'open-cloud-uaa-admin-server', '/oauth/confirm_access', 0, 1, b'1', b'0', b'0', 'com.opencloud.uaa.admin.server.controller.IndexController', 'confirm_access', 0, '2019-07-23 13:56:54', 0, '2019-08-19 18:55:50', NULL);
INSERT INTO `sys_api` VALUES (1153544495072157698, '404ea9fec51f41d5ebdefcf30039d173', 'securityConfiguration', 'default', '', '', '', 'open-cloud-uaa-admin-server', '/swagger-resources/configuration/security', 0, 1, b'1', b'1', b'0', 'springfox.documentation.swagger.web.ApiResourceController', 'securityConfiguration', 0, '2019-07-23 13:56:54', 0, '2019-08-19 18:55:51', NULL);
INSERT INTO `sys_api` VALUES (1153544495139266561, '154a10d28aa54f3ab5eec62c5b3cc396', 'swaggerResources', 'default', '', '', '', 'open-cloud-uaa-admin-server', '/swagger-resources', 0, 1, b'1', b'1', b'0', 'springfox.documentation.swagger.web.ApiResourceController', 'swaggerResources', 0, '2019-07-23 13:56:54', 0, '2019-08-19 18:55:51', NULL);
INSERT INTO `sys_api` VALUES (1153544495210569729, 'd0cb7641b01ea63b4b680aa41372f3dd', 'uiConfiguration', 'default', '', '', '', 'open-cloud-uaa-admin-server', '/swagger-resources/configuration/ui', 0, 1, b'1', b'1', b'0', 'springfox.documentation.swagger.web.ApiResourceController', 'uiConfiguration', 0, '2019-07-23 13:56:54', 0, '2019-08-19 18:55:51', NULL);
INSERT INTO `sys_api` VALUES (1153544495277678593, 'e57afa79be638071473395318684b732', 'error', 'default', '', '', '', 'open-cloud-uaa-admin-server', '/error', 0, 1, b'1', b'1', b'0', 'org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController', 'error', 0, '2019-07-23 13:56:54', 0, '2019-08-19 18:55:51', NULL);
INSERT INTO `sys_api` VALUES (1156037702112673793, '8a2aa97d530b74b9e440505870e36d89', 'index', 'default', '', 'GET', '', 'open-cloud-api-zuul-server', '/', 0, 1, b'1', b'0', b'0', 'com.opencloud.gateway.zuul.server.controller.IndexController', 'index', 0, '2019-07-30 11:04:00', 0, '2019-07-30 13:02:42', NULL);
INSERT INTO `sys_api` VALUES (1156037702343360514, 'f03bda1e0dcaeb9b777caf1ef6b23af5', 'swaggerResources', 'default', '', '', '', 'open-cloud-api-zuul-server', '/swagger-resources', 0, 1, b'1', b'1', b'0', 'springfox.documentation.swagger.web.ApiResourceController', 'swaggerResources', 0, '2019-07-30 11:04:01', 0, '2019-07-30 13:02:42', NULL);
INSERT INTO `sys_api` VALUES (1156037702464995329, 'cf6a6c1897dc3bc2ae3fe59abba8c2f6', 'uiConfiguration', 'default', '', '', '', 'open-cloud-api-zuul-server', '/swagger-resources/configuration/ui', 0, 1, b'1', b'1', b'0', 'springfox.documentation.swagger.web.ApiResourceController', 'uiConfiguration', 0, '2019-07-30 11:04:01', 0, '2019-07-30 13:02:42', NULL);
INSERT INTO `sys_api` VALUES (1156037702569852929, '7b980f1a7227e563fee6047756cf5ac3', 'securityConfiguration', 'default', '', '', '', 'open-cloud-api-zuul-server', '/swagger-resources/configuration/security', 0, 1, b'1', b'1', b'0', 'springfox.documentation.swagger.web.ApiResourceController', 'securityConfiguration', 0, '2019-07-30 11:04:01', 0, '2019-07-30 13:02:42', NULL);
INSERT INTO `sys_api` VALUES (1156037702674710529, '89c3da13314b86a70f299e6ca1455150', 'errorHtml', 'default', '', '', 'text/html', 'open-cloud-api-zuul-server', '/error', 0, 1, b'1', b'1', b'0', 'org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController', 'errorHtml', 0, '2019-07-30 11:04:01', 0, '2019-07-30 13:02:42', NULL);
INSERT INTO `sys_api` VALUES (1156037705061269506, '9275410ca1d4198af7a7e66b2661f422', '批量删除数据', 'default', '批量删除数据', 'POST', '', 'open-cloud-base-server', '/api/batch/remove', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseApiController', 'batchRemove', 0, '2019-07-30 11:04:01', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1156037705921101825, 'de9889498a75766df0dd9644b21b07bf', '批量修改公开状态', 'default', '批量修改公开状态', 'POST', '', 'open-cloud-base-server', '/api/batch/update/open', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseApiController', 'batchUpdateOpen', 0, '2019-07-30 11:04:01', 0, '2019-08-21 11:44:34', NULL);
INSERT INTO `sys_api` VALUES (1156037706126622721, 'f7244837c4701df4f47e540682405e5e', '批量修改身份认证', 'default', '批量修改身份认证', 'POST', '', 'open-cloud-base-server', '/api/batch/update/auth', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseApiController', 'batchUpdateAuth', 0, '2019-07-30 11:04:01', 0, '2019-08-21 11:44:26', NULL);
INSERT INTO `sys_api` VALUES (1156037706206314497, '1c335308b42f13c97e2f41ae42418f56', '批量修改状态', 'default', '批量修改状态', 'POST', '', 'open-cloud-base-server', '/api/batch/update/status', 0, 1, b'1', b'1', b'1', 'com.opencloud.base.server.controller.BaseApiController', 'batchUpdateStatus', 0, '2019-07-30 11:04:01', 0, '2019-08-19 18:59:18', NULL);
INSERT INTO `sys_api` VALUES (1156038470987317250, 'b8c71f653aa802bbedf16c4ec9b20e3e', '获取当前登录用户信息-SSO单点登录', 'default', '获取当前登录用户信息-SSO单点登录', 'GET', '', 'open-cloud-uaa-admin-server', '/current/user/sso', 0, 1, b'1', b'1', b'0', 'com.opencloud.uaa.admin.server.controller.LoginController', 'principal', 0, '2019-07-30 11:07:04', 0, '2019-08-22 14:36:44', NULL);

-- ----------------------------
-- Table structure for sys_app
-- ----------------------------
DROP TABLE IF EXISTS `sys_app`;
CREATE TABLE `sys_app`  (
  `app_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户端ID',
  `api_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'API访问key',
  `secret_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'API访问密钥',
  `app_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'app名称',
  `app_name_en` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'app英文名称',
  `app_icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用图标',
  `app_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'app类型:server-服务应用 app-手机应用 pc-PC网页应用 wap-手机网页应用',
  `app_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'app描述',
  `app_os` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移动应用操作系统:ios-苹果 android-安卓',
  `website` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '官网地址',
  `status` tinyint(3) NOT NULL DEFAULT 1 COMMENT '状态:0-无效 1-有效',
  `is_persist` bit(1) NOT NULL DEFAULT b'0' COMMENT '保留数据0-否 1-是 不允许删除',
  `create_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '创建用户id',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '更新用户id',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否逻辑删除，0:未删除；1:已删除',
  PRIMARY KEY (`app_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统应用-基础信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_app
-- ----------------------------
INSERT INTO `sys_app` VALUES ('1552274783265', '7gBZcbsC7kLIWCdELIl8nxcs', '0osTIhce7uPvDKHz6aa67bhCukaKoYl4', '平台用户认证服务器', 'open-cloud-uaa-admin-server', '', 'server', '资源服务器', '', 'http://www.baidu.com', 1, b'1', 0, '2018-11-12 17:48:45', 0, '2019-07-11 18:31:05', b'0');
INSERT INTO `sys_app` VALUES ('1552294656514', 'rOOM15Zbc3UFWgW2h71gRFvi', '2Iw2B0UCMYd1cZjt8Fpr4KJUx75wQCwW', 'SSO单点登录DEMO', 'sso-demo', '', 'server', 'sso', '', 'http://www.baidu.com', 1, b'1', 0, '2018-11-12 17:48:45', 0, '2019-07-11 18:31:05', b'0');

-- ----------------------------
-- Table structure for sys_authority
-- ----------------------------
DROP TABLE IF EXISTS `sys_authority`;
CREATE TABLE `sys_authority`  (
  `authority_id` bigint(20) NOT NULL,
  `authority_type` tinyint(3) NOT NULL DEFAULT 1 COMMENT '权限类型，1：菜单权限，2：api权限',
  `authority` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限标识',
  `menu_id` bigint(20) NULL DEFAULT NULL COMMENT '菜单资源ID',
  `api_id` bigint(20) NULL DEFAULT NULL COMMENT 'API资源ID',
  `action_id` bigint(20) NULL DEFAULT NULL COMMENT '动作id',
  `status` tinyint(3) NOT NULL DEFAULT 1 COMMENT '状态',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建用户id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT NULL COMMENT '更新用户id',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_delete` bit(1) NULL DEFAULT b'0' COMMENT '是否逻辑删除，0:未删除；1:已删除',
  PRIMARY KEY (`authority_id`) USING BTREE,
  INDEX `menu_id`(`menu_id`) USING BTREE,
  INDEX `api_id`(`api_id`) USING BTREE,
  INDEX `action_id`(`action_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统权限-菜单权限、操作权限、API权限' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_authority
-- ----------------------------
INSERT INTO `sys_authority` VALUES (1, 1, 'MENU_system', 1, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (2, 1, 'MENU_gatewayIpLimit', 2, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (3, 1, 'MENU_systemMenu', 3, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (5, 1, 'MENU_gatewayRoute', 5, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (6, 1, 'MENU_systemApi', 6, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (7, 1, 'MENU_gatewayTrace', 7, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (8, 1, 'MENU_systemRole', 8, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (9, 1, 'MENU_systemApp', 9, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (10, 1, 'MENU_systemUser', 10, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (11, 1, 'MENU_apiDebug', 11, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (12, 1, 'MENU_gatewayLogs', 12, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (13, 1, 'MENU_gateway', 13, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (14, 1, 'MENU_gatewayRateLimit', 14, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (15, 1, 'MENU_task', 15, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (16, 1, 'MENU_job', 16, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (17, 1, 'MENU_message', 17, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (18, 1, 'MENU_webhook', 18, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (19, 1, 'MENU_taskLogs', 19, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (99, 2, 'API_all', NULL, 1, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (100, 2, 'API_actuator', NULL, 2, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131849293509033986, 1, 'ACTION_systemMenuView', NULL, NULL, 1131849293404176385, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131849510677512193, 1, 'ACTION_systemMenuEdit', NULL, NULL, 1131849510572654593, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131858946414489602, 1, 'ACTION_systemRoleView', NULL, NULL, 1131858946338992129, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131863248373690369, 1, 'ACTION_systemRoleEdit', NULL, NULL, 1131863248310775809, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131863723806437377, 1, 'ACTION_systemAppView', NULL, NULL, 1131863723722551297, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131863775966801921, 1, 'ACTION_systemAppEdit', NULL, NULL, 1131863775899693057, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131864400590942210, 1, 'ACTION_systemUserView', NULL, NULL, 1131864400507056130, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131864444954095617, 1, 'ACTION_systemUserEdit', NULL, NULL, 1131864444878598146, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131864827327819778, 1, 'ACTION_gatewayIpLimitView', NULL, NULL, 1131864827252322305, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131864864325775361, 1, 'ACTION_gatewayIpLimitEdit', NULL, NULL, 1131864864267055106, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131865040381685761, 1, 'ACTION_gatewayRouteView', NULL, NULL, 1131865040289411074, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131865075697725442, 1, 'ACTION_gatewayRouteEdit', NULL, NULL, 1131865075609645057, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131865482390024193, 1, 'ACTION_systemApiView', NULL, NULL, 1131865482314526722, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131865520809848834, 1, 'ACTION_systemApiEdit', NULL, NULL, 1131865520738545666, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131865773000765441, 1, 'ACTION_gatewayLogsView', NULL, NULL, 1131865772929462274, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131865931214106626, 1, 'ACTION_gatewayRateLimitView', NULL, NULL, 1131865931146997761, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131865974771953666, 1, 'ACTION_gatewayRateLimitEdit', NULL, NULL, 1131865974704844802, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131866278280179714, 1, 'ACTION_jobView', NULL, NULL, 1131866278187905026, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131866310676983810, 1, 'ACTION_jobEdit', NULL, NULL, 1131866310622457857, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131866943534542850, 1, 'ACTION_schedulerLogsView', NULL, NULL, 1131866943459045377, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1131867094550458369, 1, 'ACTION_notifyHttpLogsView', NULL, NULL, 1131867094479155202, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168010446168066, 2, 'API_9a1e16647d1e534f7b06ed9db83719fa', NULL, 1149168010337116161, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168010530054146, 2, 'API_26128c7aafae30020bcac2c47631497a', NULL, 1149168010500694017, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168010601357314, 2, 'API_84d30b0aab28de096779d6f8bee960e7', NULL, 1149168010571997185, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168010681049090, 2, 'API_756154d7c8c6a79f02da79d9148beb9f', NULL, 1149168010655883266, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168010743963649, 2, 'API_92187c259636fdf5d4f108b688fc44ee', NULL, 1149168010722992129, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168010823655425, 2, 'API_214e294809bb1f22abc70c38959e1343', NULL, 1149168010794295298, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168010886569985, 2, 'API_a0f00dd477c9232a51b94553ad7a7803', NULL, 1149168010861404161, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168010953678850, 2, 'API_6311ab783c8818cd5918e5d903fb371e', NULL, 1149168010920124418, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168011012399105, 2, 'API_aa37236289cedba711c8f648a94b1105', NULL, 1149168010991427586, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168011071119362, 2, 'API_e18bae5b2fc727e77d36a3f4697abf0f', NULL, 1149168011045953537, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168011125645314, 2, 'API_d4c4c1b5b3847cc63b52ed9007698325', NULL, 1149168011100479489, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168011188559874, 2, 'API_9e054a7dc4194e7635c2686e1109aa49', NULL, 1149168011163394050, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168011243085826, 2, 'API_fa32726ed4076d44cae0023132f7014f', NULL, 1149168011217920002, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168011301806082, 2, 'API_c5863e8ad8a5605354b9a122511ffa01', NULL, 1149168011280834562, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168011356332033, 2, 'API_a23793da67076d4be853daa422943c53', NULL, 1149168011331166209, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168011490549761, 2, 'API_93c006692e386f96555e7edece91c994', NULL, 1149168011452801025, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168011545075713, 2, 'API_385d34ec827de818225a981d737f307a', NULL, 1149168011524104194, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168011599601666, 2, 'API_ccb6139e4beb2a84e29f399215bfedc0', NULL, 1149168011578630146, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168011649933313, 2, 'API_f09f5543ff4c0ceab04b99ddc5a94bc2', NULL, 1149168011628961794, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168011708653570, 2, 'API_331bddbcd34cacdd74f80b48e3484017', NULL, 1149168011687682050, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168011767373826, 2, 'API_785b2368bf67d1a942ea64d946c70144', NULL, 1149168011742208002, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168011826094081, 2, 'API_282c563cbfb922ac6328c058051d31a4', NULL, 1149168011805122561, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168011876425729, 2, 'API_f2be18d77a78f728df355f32e90cd780', NULL, 1149168011851259906, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168011926757377, 2, 'API_4611bd638b538605286600dbc899cbb0', NULL, 1149168011909980161, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168011981283329, 2, 'API_5262f2214a4ff43179c947dc458213d7', NULL, 1149168011960311809, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012031614977, 2, 'API_8a6561a9a0efa7f5f9faadb6e39dc927', NULL, 1149168012010643458, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012086140929, 2, 'API_1df7911bb85d15f51bcd78c43f18b24d', NULL, 1149168012060975106, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012132278273, 2, 'API_80256cc196e1e6d2437289e038b13b8a', NULL, 1149168012111306753, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012182609922, 2, 'API_5586d5f337d4ebf99e551fdbf55d1116', NULL, 1149168012161638401, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012237135873, 2, 'API_b96a9514487f7e7679fe39c9254ab972', NULL, 1149168012216164353, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012287467521, 2, 'API_3a1c4ef17565d863f35a3bccd7cc035e', NULL, 1149168012270690305, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012341993473, 2, 'API_734d1179e332309b9de3927894864396', NULL, 1149168012316827649, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012442656770, 2, 'API_39ad69234435595ee67836e8a03d6ea6', NULL, 1149168012421685249, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012492988418, 2, 'API_50520c56c7714aa9be9a808cd30fd9bb', NULL, 1149168012467822593, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012539125762, 2, 'API_9aca4415969b60158f383b2b49b8fe79', NULL, 1149168012522348546, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012593651713, 2, 'API_c4c154b6ffa4b21835db1e09614c73e6', NULL, 1149168012576874498, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012639789058, 2, 'API_4a13f24fa2d5cf74a6e408e82182e1b1', NULL, 1149168012623011841, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012690120705, 2, 'API_2901b2b8040f1c44c0318650fbfd4460', NULL, 1149168012673343490, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012736258049, 2, 'API_87049d9db5261f50d524c45a32044415', NULL, 1149168012719480834, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012790784002, 2, 'API_50bcf72df4a18a6a8c46a7076a7a014a', NULL, 1149168012769812481, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012836921346, 2, 'API_638be151d314740a2f2aaebaaba3f479', NULL, 1149168012820144129, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012891447297, 2, 'API_e3c1b0b860591e11b50d6dccc9e2fa6a', NULL, 1149168012870475777, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012941778945, 2, 'API_bbc5546a6cfa1161859b69ef5754eace', NULL, 1149168012920807425, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168012996304898, 2, 'API_720f3b2571fe2850108f090408514cae', NULL, 1149168012979527682, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013046636546, 2, 'API_8f2b70d4b55f13eaf82c4cf687903ced', NULL, 1149168013029859330, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013092773889, 2, 'API_d9fc987468fd4c73350653d5c4e53453', NULL, 1149168013071802369, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013143105537, 2, 'API_f4e83562a36e943883c5eb6b7376e365', NULL, 1149168013122134017, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013197631489, 2, 'API_9b57678bc96af393c0c60e135acc90a9', NULL, 1149168013176659970, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013243768834, 2, 'API_4693b9a455022754623dd25f9905c19b', NULL, 1149168013226991617, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013302489090, 2, 'API_51ea3146eaebdd33fd065b029ee147fb', NULL, 1149168013277323266, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013348626434, 2, 'API_b4cdd238a7e34e1794988d00c4d45779', NULL, 1149168013327654913, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013390569474, 2, 'API_610b499835de759e37429ece2de1ee33', NULL, 1149168013373792257, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013436706818, 2, 'API_334d10291e4061c07a180546dd9cedea', NULL, 1149168013419929601, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013482844161, 2, 'API_d722dc79f9c8d3ece3b9324a03caf902', NULL, 1149168013466066945, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013533175810, 2, 'API_c8293b83e96ca425be2e24f0cfa8c7bf', NULL, 1149168013512204290, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013583507458, 2, 'API_3fdb78cf597834182b4c6b558a800774', NULL, 1149168013562535938, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013625450498, 2, 'API_dc01f78458d3fe0cff028da861bdb65b', NULL, 1149168013608673282, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013675782145, 2, 'API_25c851a0698d161f2b00bdcf74668648', NULL, 1149168013659004929, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013726113794, 2, 'API_86197fb92e8fd3359842cf511c68be8c', NULL, 1149168013700947969, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013768056834, 2, 'API_db9fbb85ee040fd2f8981e47a6873614', NULL, 1149168013747085313, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013814194178, 2, 'API_bbd37ea32bf91697909dd261befa8a5f', NULL, 1149168013797416961, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013864525825, 2, 'API_b0d37206d8ebf60a7359e1b15d79b361', NULL, 1149168013843554306, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013910663170, 2, 'API_88fa6cc7c4fa7f0fb2b8d3df951fdfa9', NULL, 1149168013889691650, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168013960994818, 2, 'API_902ca6aa4d96c21a48cabe5363a4b3dd', NULL, 1149168013940023298, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014011326465, 2, 'API_0f86b27eb3b0fcd3e714e22d43b1172b', NULL, 1149168013994549249, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014057463810, 2, 'API_83ad6d5e0903bf19cfa28c7a83595de2', NULL, 1149168014036492290, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014099406849, 2, 'API_6eb627d08d2f0aee74bfd7c9b53b1124', NULL, 1149168014082629634, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014141349889, 2, 'API_3fde01046c8643e9b536ae6f61d4431f', NULL, 1149168014128766978, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014187487234, 2, 'API_2cfc3bbc760608bda59d68b2c0b4e2fd', NULL, 1149168014170710018, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014233624578, 2, 'API_f5abed6e35a7a1715fac80841e4e592f', NULL, 1149168014212653057, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014376230913, 2, 'API_c584e31594bc5bedfc959c729a2e90e7', NULL, 1149168014258790401, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014426562561, 2, 'API_24b5559e0204223af9b73f3c68ee68f3', NULL, 1149168014405591042, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014472699906, 2, 'API_160a9fb0ce4243b989bbaaac031a345e', NULL, 1149168014451728385, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014518837249, 2, 'API_99a30e8643b0820a04ecbdcf7c9628af', NULL, 1149168014497865729, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014560780290, 2, 'API_0dfca465c864c74d04a4d6135fee1880', NULL, 1149168014544003073, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014606917634, 2, 'API_2aecc273984a1d3981dc0f62cafbe894', NULL, 1149168014585946113, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014657249282, 2, 'API_b993f44f1ad4dd08bfd4f36730af9ea1', NULL, 1149168014636277762, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014703386625, 2, 'API_ab71840616ff1f1a619170538b1f1de9', NULL, 1149168014686609409, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014749523970, 2, 'API_5f512e5065e80c8d0ffd6be4f27c386e', NULL, 1149168014732746753, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014795661313, 2, 'API_f83ecfdd9325be59c60627f0164ec8a8', NULL, 1149168014774689793, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014833410049, 2, 'API_9c565ce4ded10ccd227001f6d7eab11c', NULL, 1149168014816632833, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014879547393, 2, 'API_0830e3c6c3d42f11d86f5e2ed0d99985', NULL, 1149168014862770178, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014925684737, 2, 'API_8dff62591ccefba3537b84aa9d72d7b7', NULL, 1149168014908907521, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168014976016385, 2, 'API_382e5fe59fdc31f240b5e42b2ae1a522', NULL, 1149168014955044866, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168015017959426, 2, 'API_177b640de1b90d39536f462f24e3051f', NULL, 1149168015001182210, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168015076679682, 2, 'API_1307447addfddf7e075eea74100c150d', NULL, 1149168015055708162, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168015135399937, 2, 'API_40a481fc86814de744e91e42c15b6f3e', NULL, 1149168015118622721, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168015185731586, 2, 'API_61e8c492eb9e56441d1c428ab3e09f66', NULL, 1149168015164760065, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168208639614978, 2, 'API_3c28ffa71681fb53f1f458548456256c', NULL, 1149168208614449153, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168208706723841, 2, 'API_29c8264c689bce3567708f977779c3b9', NULL, 1149168208677363714, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149168208765444098, 2, 'API_f98a1543766b726e4b5a6d7a7f32fd93', NULL, 1149168208740278273, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149253733748785154, 1, 'MENU_developer', 1149253733673287682, NULL, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149265710541914115, 2, 'API_be5eb57a4df27bf8dece0bda751a5a62', NULL, 1149265710541914114, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149265710692909058, 2, 'API_0273e0d10bea64046178b7c5ff5055cc', NULL, 1149265710646771714, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149265710789378049, 2, 'API_3b5928629e5398dcc534c548d5c01085', NULL, 1149265710743240705, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1149265710877458434, 2, 'API_df78a9afa62bb2d1165aeea14aca9e41', NULL, 1149265710839709697, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1151324453706289153, 2, 'API_4fdfe1d02c78379a3a8eb2a84b0e8b82', NULL, 1151324453580460033, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1151324453811146754, 2, 'API_92eabcb8d4d4d587343cc2b6e11ccecc', NULL, 1151324453773398018, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1151324453920198658, 2, 'API_d094367240c4c6847eeda0fe2317069c', NULL, 1151324453869867009, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1151324454025056258, 2, 'API_d26205d1d1e42d925ea972a6d034ba15', NULL, 1151324453974724609, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1151324454247354369, 2, 'API_5b2390e91198ae02e58b4704e23f7a45', NULL, 1151324454205411329, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1151324462254280706, 2, 'API_f17677d7f0f33851333de6bc48d5ac1a', NULL, 1151324462224920577, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1151395583808917506, 2, 'API_042dd26520a30c0fecd13ee015a79be5', NULL, 1151395583808917505, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1151426336538128386, 2, 'API_93ff44fd4adab6c4097327eb5ffd405a', NULL, 1151426336538128385, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1151426336915615747, 2, 'API_030aae5c245d4d780ca248fdd5e4bb67', NULL, 1151426336915615746, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1151426338278764546, 2, 'API_7f542a952be1a9d8b089330f5f0aba3b', NULL, 1151426338278764545, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1151426338278764548, 2, 'API_b9f2f4ed495f3f71da31c14e8ad2193d', NULL, 1151426338278764547, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1151426338681417729, 2, 'API_14467518667ccfa46541736433d0efa2', NULL, 1151426338278764549, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1151426338811441154, 2, 'API_903acc6b8c02184170bd3f316cd0f0c3', NULL, 1151426338756915201, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1151426339037933569, 2, 'API_cb8aded7fdffd0e8e86940f540b32472', NULL, 1151426338903715841, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1153543688968871938, 2, 'API_8aedf1bcd8ebc00a41361067602e4866', NULL, 1153543688817876993, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1153543700159275010, 2, 'API_9ae39e2a7bb1f2502dcd5f4d280b739d', NULL, 1153543700121526273, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1153543700238966785, 2, 'API_8e25fdc700610c63d638c55545441119', NULL, 1153543700201218049, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1153543700343824386, 2, 'API_0cea701426acbc8348e64ecb598a8bf7', NULL, 1153543700285104130, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1153543700427710466, 2, 'API_604809625765c7df1e1e8ee8b7e7fe81', NULL, 1153543700398350338, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1153544494539481090, 2, 'API_09e8e7b093a0fb008071c09bf440fda1', NULL, 1153544494501732354, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1153544494644338690, 2, 'API_1f7307e3c2809aff5437f3594594c8ca', NULL, 1153544494598201345, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1153544494724030465, 2, 'API_4d27a9ae885d7dbfc82e7ae2c86ec9d6', NULL, 1153544494694670337, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1153544494807916545, 2, 'API_c235abccc85e47cda9710697c59a4aa7', NULL, 1153544494765973506, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1153544495109906433, 2, 'API_404ea9fec51f41d5ebdefcf30039d173', NULL, 1153544495072157698, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1153544495168626690, 2, 'API_154a10d28aa54f3ab5eec62c5b3cc396', NULL, 1153544495139266561, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1153544495244124162, 2, 'API_d0cb7641b01ea63b4b680aa41372f3dd', NULL, 1153544495210569729, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1153544495307038721, 2, 'API_e57afa79be638071473395318684b732', NULL, 1153544495277678593, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1156037702284640257, 2, 'API_8a2aa97d530b74b9e440505870e36d89', NULL, 1156037702112673793, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1156037702410469377, 2, 'API_f03bda1e0dcaeb9b777caf1ef6b23af5', NULL, 1156037702343360514, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1156037702523715586, 2, 'API_cf6a6c1897dc3bc2ae3fe59abba8c2f6', NULL, 1156037702464995329, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1156037702620184577, 2, 'API_7b980f1a7227e563fee6047756cf5ac3', NULL, 1156037702569852929, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1156037702708264962, 2, 'API_89c3da13314b86a70f299e6ca1455150', NULL, 1156037702674710529, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1156037705195487233, 2, 'API_9275410ca1d4198af7a7e66b2661f422', NULL, 1156037705061269506, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1156037705963044866, 2, 'API_de9889498a75766df0dd9644b21b07bf', NULL, 1156037705921101825, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1156037706155982850, 2, 'API_f7244837c4701df4f47e540682405e5e', NULL, 1156037706126622721, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1156037706244063234, 2, 'API_1c335308b42f13c97e2f41ae42418f56', NULL, 1156037706206314497, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1156038471020871682, 2, 'API_b8c71f653aa802bbedf16c4ec9b20e3e', NULL, 1156038470987317250, NULL, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1164422088618938370, 1, 'ACTION_developerView', NULL, NULL, 1164422088547635202, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');
INSERT INTO `sys_authority` VALUES (1164422211226832898, 1, 'ACTION_developerEdit', NULL, NULL, 1164422211189084162, 1, NULL, '2019-07-30 15:43:15', NULL, '2019-07-30 15:43:15', b'0');

-- ----------------------------
-- Table structure for sys_authority_action
-- ----------------------------
DROP TABLE IF EXISTS `sys_authority_action`;
CREATE TABLE `sys_authority_action`  (
  `relate_id` bigint(20) NOT NULL,
  `action_id` bigint(20) NOT NULL COMMENT '操作ID',
  `authority_id` bigint(20) NOT NULL COMMENT 'API',
  `create_user_id` bigint(20) NOT NULL DEFAULT 0,
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NOT NULL DEFAULT 0,
  `update_time` datetime(0) NOT NULL COMMENT '修改时间',
  `is_delete` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`relate_id`) USING BTREE,
  INDEX `action_id`(`action_id`) USING BTREE,
  INDEX `authority_id`(`authority_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统权限-功能操作关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_authority_action
-- ----------------------------
INSERT INTO `sys_authority_action` VALUES (1, 1131864400507056130, 1149168013625450498, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (2, 1131864400507056130, 1131813663584489473, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (3, 1131864400507056130, 1131753771397967873, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (4, 1131864400507056130, 1131753771712540674, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (5, 1131864400507056130, 1131753772526235650, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (6, 1131866943459045377, 1151426338681417729, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (7, 1131866943459045377, 1131814634553282561, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (8, 1131864444878598146, 1149168013675782145, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (9, 1131864444878598146, 1149168013436706818, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (10, 1131864444878598146, 1149168013583507458, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (11, 1131864444878598146, 1149168013482844161, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (12, 1131864444878598146, 1149168012086140929, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (13, 1131864444878598146, 1149168012031614977, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (14, 1131864444878598146, 1149168013768056834, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (15, 1131864444878598146, 1149168013197631489, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (16, 1131864444878598146, 1131813663827759105, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (17, 1131864444878598146, 1131753772400406529, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (18, 1131864444878598146, 1131753772249411585, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (19, 1131864444878598146, 1131753771507019778, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (20, 1131864444878598146, 1131753771607683074, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (21, 1164422211189084162, 1149168012287467521, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (22, 1164422211189084162, 1149168012539125762, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (23, 1164422211189084162, 1149168012341993473, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (24, 1164422211189084162, 1149168012442656770, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (25, 1131849293404176385, 1149168012891447297, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (26, 1131849293404176385, 1149168012736258049, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (27, 1131849293404176385, 1149168012836921346, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (28, 1131849293404176385, 1131813661852241922, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (29, 1131849293404176385, 1131813663894867969, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (30, 1131849293404176385, 1131813661235679233, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (31, 1131849293404176385, 1131753768596172802, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (32, 1131849293404176385, 1131753768814276610, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (33, 1131849293404176385, 1131844868619030529, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (34, 1164422088547635202, 1149168012593651713, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (35, 1131858946338992129, 1149168013046636546, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (36, 1131858946338992129, 1149168013243768834, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (37, 1131858946338992129, 1131813663743873026, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (38, 1131858946338992129, 1131813663672569857, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (39, 1131858946338992129, 1131753770320031746, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (40, 1131858946338992129, 1131753770445860866, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (41, 1131858946338992129, 1131753770185814018, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (42, 1131858946338992129, 1131753770596855809, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (43, 1131863248310775809, 1149168013533175810, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (44, 1131863248310775809, 1149168013143105537, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (45, 1131863248310775809, 1149168013348626434, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (46, 1131863248310775809, 1149168013390569474, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (47, 1131863248310775809, 1149168013046636546, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (48, 1131863248310775809, 1149168013092773889, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (49, 1131863248310775809, 1149168012182609922, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (50, 1131863248310775809, 1149168011826094081, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (51, 1131863248310775809, 1149168013302489090, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (52, 1131863248310775809, 1131753770705907714, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (53, 1131863248310775809, 1131753770823348225, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (54, 1131863248310775809, 1131753771100172289, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (55, 1131863248310775809, 1131753771226001410, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (56, 1131863248310775809, 1131813664213635074, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (57, 1131865482314526722, 1149168011125645314, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (58, 1131865482314526722, 1149168011012399105, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (59, 1131865482314526722, 1151324462254280706, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (60, 1131865482314526722, 1131995510689820674, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (61, 1131865482314526722, 1131753765458833410, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (62, 1131865482314526722, 1131753765324615681, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (63, 1131865482314526722, 1131753765182009346, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (64, 1131863775899693057, 1149168011243085826, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (65, 1131863775899693057, 1149168011545075713, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (66, 1131863775899693057, 1149168011649933313, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (67, 1131863775899693057, 1149168011767373826, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (68, 1131863775899693057, 1149168011356332033, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (69, 1131863775899693057, 1149168011188559874, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (70, 1131863775899693057, 1149168011490549761, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (71, 1131863775899693057, 1149168011301806082, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (72, 1131863775899693057, 1149168011876425729, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (73, 1131863775899693057, 1149168011708653570, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (74, 1131863775899693057, 1149168012442656770, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (75, 1131863775899693057, 1131813663953588225, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (76, 1131863723722551297, 1149168011243085826, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (77, 1131863723722551297, 1149168011545075713, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (78, 1131863723722551297, 1149168011599601666, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (79, 1131863723722551297, 1131813663404134402, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (80, 1131863723722551297, 1132203893955006465, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (81, 1131863723722551297, 1132203893384581121, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (82, 1131863723722551297, 1132203893577519106, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (83, 1131849510572654593, 1149168012736258049, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (84, 1131849510572654593, 1149168011926757377, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (85, 1131849510572654593, 1149168012891447297, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (86, 1131849510572654593, 1149168011981283329, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (87, 1131849510572654593, 1149168010530054146, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (88, 1131849510572654593, 1149168010601357314, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (89, 1131849510572654593, 1149168010681049090, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (90, 1131849510572654593, 1149168010446168066, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (91, 1131849510572654593, 1149168010743963649, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (92, 1131849510572654593, 1149168012790784002, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (93, 1131849510572654593, 1149168012996304898, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (94, 1131849510572654593, 1149168012941778945, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (95, 1131849510572654593, 1131813661348925441, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (96, 1131849510572654593, 1131813661483143169, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (97, 1131849510572654593, 1131813661613166594, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (98, 1131849510572654593, 1131813663500603394, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (99, 1131849510572654593, 1131813663894867969, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (100, 1131849510572654593, 1131813664033280001, 0, '2019-08-22 14:54:41', 0, '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_action` VALUES (101, 1131849510572654593, 1131753768357097474, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (102, 1131849510572654593, 1131753768474537986, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (103, 1131849510572654593, 1131753768705224706, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (104, 1131865520738545666, 1156037705963044866, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (105, 1131865520738545666, 1156037706155982850, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (106, 1131865520738545666, 1156037706244063234, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (107, 1131865520738545666, 1156037705195487233, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (108, 1131865520738545666, 1149168011125645314, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (109, 1131865520738545666, 1149168010953678850, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (110, 1131865520738545666, 1149168010823655425, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (111, 1131865520738545666, 1149168010886569985, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (112, 1131865520738545666, 1131753764884213761, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (113, 1131865520738545666, 1131753765026820098, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (114, 1131865520738545666, 1131753765614022657, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (115, 1131865772929462274, 1149168014011326465, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (116, 1131865772929462274, 1131753774162014210, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (117, 1131864827252322305, 1149168014426562561, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (118, 1131864827252322305, 1149168014560780290, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (119, 1131864827252322305, 1131753775453859842, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (120, 1131864827252322305, 1131753774891823105, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (121, 1131864827252322305, 1131753775000875010, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (122, 1131865931146997761, 1149168014925684737, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (123, 1131865931146997761, 1149168014657249282, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (124, 1131865931146997761, 1131753774187180034, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (125, 1131865931146997761, 1131753776095588354, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (126, 1131865931146997761, 1131753776204640257, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (127, 1131865931146997761, 1131753776292720641, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (128, 1131864864267055106, 1149168011071119362, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (129, 1131864864267055106, 1149168014472699906, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (130, 1131864864267055106, 1149168014233624578, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (131, 1131864864267055106, 1149168014518837249, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (132, 1131864864267055106, 1149168014376230913, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (133, 1131864864267055106, 1149168014426562561, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (134, 1131864864267055106, 1149168014606917634, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (135, 1131864864267055106, 1131753775768432642, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (136, 1131864864267055106, 1131753775642603522, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (137, 1131864864267055106, 1131753775181230082, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (138, 1131864864267055106, 1131753775000875012, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (139, 1131865974704844802, 1149168014795661313, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (140, 1131865974704844802, 1149168014657249282, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (141, 1131865974704844802, 1149168014833410049, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (142, 1131865974704844802, 1149168014703386625, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (143, 1131865974704844802, 1149168014879547393, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (144, 1131865974704844802, 1149168014749523970, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (145, 1131865974704844802, 1149168011071119362, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (146, 1131865974704844802, 1131753776527601665, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (147, 1131865974704844802, 1131753776657625090, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (148, 1131865974704844802, 1131753776808620034, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (149, 1131865974704844802, 1131753776917671938, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (150, 1131865040289411074, 1149168014976016385, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (151, 1131865040289411074, 1149168015017959426, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (152, 1131865040289411074, 1131753777139970050, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (153, 1131865040289411074, 1131753777026723841, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (154, 1131865040289411074, 1131753774552084481, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (155, 1131865075609645057, 1149168015135399937, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (156, 1131865075609645057, 1149168015185731586, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (157, 1131865075609645057, 1149168015017959426, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (158, 1131865075609645057, 1149168014141349889, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (159, 1131865075609645057, 1149168015076679682, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (160, 1131865075609645057, 1131753777211273218, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (161, 1131865075609645057, 1131753777416794113, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (162, 1131865075609645057, 1131753777517457410, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (163, 1131866278187905026, 1151426338681417729, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (164, 1131866278187905026, 1151426336538128386, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (165, 1131866278187905026, 1131814634553282561, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (166, 1131866278187905026, 1131814634746220545, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (167, 1131866310622457857, 1151426338278764546, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (168, 1131866310622457857, 1151426339037933569, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (169, 1131866310622457857, 1151426338278764548, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (170, 1131866310622457857, 1151426338811441154, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (171, 1131866310622457857, 1151426336915615747, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (172, 1131866310622457857, 1131814634217738242, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (173, 1131866310622457857, 1131814634293235714, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (174, 1131866310622457857, 1131814634381316097, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (175, 1131866310622457857, 1131814634469396481, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (176, 1131866310622457857, 1131814634645557249, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (177, 1131867094479155202, 1149265710877458434, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');
INSERT INTO `sys_authority_action` VALUES (178, 1131867094479155202, 1131814508397006849, 0, '2019-08-22 15:00:00', 0, '2019-08-22 15:00:00', b'0');

-- ----------------------------
-- Table structure for sys_authority_app
-- ----------------------------
DROP TABLE IF EXISTS `sys_authority_app`;
CREATE TABLE `sys_authority_app`  (
  `relate_id` bigint(20) NOT NULL,
  `authority_id` bigint(50) NOT NULL COMMENT '权限ID',
  `app_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用ID',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间:null表示长期',
  `create_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '创建用户id',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '更新用户id',
  `update_time` datetime(0) NOT NULL COMMENT '修改时间',
  `is_delete` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`relate_id`) USING BTREE,
  INDEX `authority_id`(`authority_id`) USING BTREE,
  INDEX `app_id`(`app_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统权限-应用关联' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_authority_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_authority_role`;
CREATE TABLE `sys_authority_role`  (
  `relate_id` bigint(20) NOT NULL,
  `authority_id` bigint(20) NOT NULL COMMENT '权限ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间:null表示长期',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_delete` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`relate_id`) USING BTREE,
  INDEX `authority_id`(`authority_id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统权限-角色关联' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_authority_role
-- ----------------------------
INSERT INTO `sys_authority_role` VALUES (1, 1, 2, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (2, 3, 2, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (3, 8, 2, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (4, 9, 2, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (5, 10, 2, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (6, 1131858946414489602, 2, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (7, 1131863723806437377, 2, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (8, 1131864400590942210, 2, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (9, 1131849293509033986, 2, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (10, 1131858946414489602, 2, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (11, 1131863723806437377, 2, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (12, 1131864400590942210, 2, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (13, 1131849293509033986, 2, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (14, 1131858946414489602, 2, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (15, 1131863723806437377, 2, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (16, 1131864400590942210, 2, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (17, 1131849293509033986, 2, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (18, 9, 3, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (19, 1, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (20, 9, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (21, 10, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (22, 1149253733748785154, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (23, 3, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (24, 8, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (25, 13, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (26, 6, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (27, 11, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (28, 12, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (29, 2, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (30, 14, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (31, 5, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (32, 15, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (33, 16, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (34, 19, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (35, 17, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (36, 18, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (37, 1131849293509033986, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (38, 1131849510677512193, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (39, 1131858946414489602, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (40, 1131863248373690369, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (41, 1131863775966801921, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (42, 1131864400590942210, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (43, 1131864444954095617, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (44, 1131864827327819778, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (45, 1131864864325775361, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (46, 1131865040381685761, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (47, 1131865075697725442, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (48, 1131865482390024193, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (49, 1131865773000765441, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (50, 1131865931214106626, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (51, 1131865974771953666, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (52, 1131866278280179714, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (53, 1131866310676983810, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (54, 1131866943534542850, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (55, 1131867094550458369, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (56, 1131863723806437377, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (57, 1164422088618938370, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (58, 1164422211226832898, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');
INSERT INTO `sys_authority_role` VALUES (59, 1131865520809848834, 1, NULL, '2019-08-22 14:54:41', '2019-08-22 14:54:41', b'0');

-- ----------------------------
-- Table structure for sys_authority_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_authority_user`;
CREATE TABLE `sys_authority_user`  (
  `relate_id` bigint(20) NOT NULL,
  `authority_id` bigint(20) NOT NULL COMMENT '权限ID',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  `create_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '创建用户id',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '更新用户id',
  `update_time` datetime(0) NOT NULL COMMENT '修改时间',
  `is_delete` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`relate_id`) USING BTREE,
  INDEX `authority_id`(`authority_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统权限-用户关联' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_authority_user
-- ----------------------------
INSERT INTO `sys_authority_user` VALUES (1268512864330895362, 1, 1268503318577319937, NULL, 0, '2020-06-04 20:00:09', 0, '2020-06-04 20:00:09', b'0');
INSERT INTO `sys_authority_user` VALUES (1268512864754520066, 10, 1268503318577319937, NULL, 0, '2020-06-04 20:00:09', 0, '2020-06-04 20:00:09', b'0');
INSERT INTO `sys_authority_user` VALUES (1268512865169756161, 8, 1268503318577319937, NULL, 0, '2020-06-04 20:00:10', 0, '2020-06-04 20:00:10', b'0');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_user_id` bigint(20) NULL DEFAULT 0 COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT 0 COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `is_delete` bit(1) NULL DEFAULT b'0' COMMENT '是否逻辑删除，0:未删除；1:已删除',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow', b'0');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', '初始化密码 123456', b'0');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue', b'0');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` tinyint(3) NULL DEFAULT 1 COMMENT '状态:0-无效 1-有效',
  `create_user_id` bigint(20) NULL DEFAULT 0 COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT 0 COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `is_delete` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '是否逻辑删除，0:未删除；1:已删除',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', 0, 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', '0');

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` bit(1) NULL DEFAULT b'0' COMMENT '是否默认（1是 0否）',
  `status` tinyint(3) NULL DEFAULT 1 COMMENT '状态（0停用 1正常 ）',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_user_id` bigint(20) NULL DEFAULT 0 COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT 0 COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `is_delete` bit(1) NULL DEFAULT b'0' COMMENT '是否逻辑删除，0:未删除；1:已删除',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', b'1', 0, '性别男', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', b'0', 0, '性别女', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', b'0', 0, '性别未知', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', b'1', 0, '显示菜单', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', b'0', 0, '隐藏菜单', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', b'1', 0, '正常状态', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', b'0', 0, '停用状态', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', b'1', 0, '正常状态', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', b'0', 0, '停用状态', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', b'1', 0, '默认分组', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', b'0', 0, '系统分组', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', b'1', 0, '系统默认是', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', b'0', 0, '系统默认否', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', b'1', 0, '通知', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', b'0', 0, '公告', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', b'1', 0, '正常状态', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', b'0', 0, '关闭状态', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', b'0', 0, '新增操作', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', b'0', 0, '修改操作', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', b'0', 0, '删除操作', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', b'0', 0, '授权操作', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', b'0', 0, '导出操作', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', b'0', 0, '导入操作', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', b'0', 0, '强退操作', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', b'0', 0, '生成操作', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', b'0', 0, '清空操作', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', b'0', 0, '正常状态', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', b'0', 0, '停用状态', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_user_id` bigint(20) NULL DEFAULT 0 COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT 0 COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `is_delete` bit(1) NULL DEFAULT b'0' COMMENT '是否逻辑删除，0:未删除；1:已删除',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', '用户性别列表', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', b'0');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', '菜单状态列表', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', b'0');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', '系统开关列表', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', b'0');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', '任务状态列表', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', b'0');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', '任务分组列表', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', b'0');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', '系统是否列表', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', b'0');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', '通知类型列表', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', b'0');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', '通知状态列表', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', b'0');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', '操作类型列表', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', b'0');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', '登录状态列表', 1, '2018-03-16 11:33:00', 1, '2018-03-16 11:33:00', b'0');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL COMMENT '菜单Id',
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父级菜单',
  `service_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '服务名',
  `menu_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '菜单编码',
  `menu_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '菜单名称',
  `menu_desc` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '描述',
  `scheme` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '路径前缀',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '请求路径',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '菜单标题',
  `target` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '_self' COMMENT '打开方式:_self窗口内,_blank新窗口',
  `priority` int(11) NOT NULL DEFAULT 0 COMMENT '优先级 越小越靠前',
  `status` tinyint(3) NOT NULL DEFAULT 1 COMMENT '状态:0-无效 1-有效',
  `is_persist` bit(1) NOT NULL DEFAULT b'0' COMMENT '保留数据0-否 1-是 不允许删除',
  `create_user_id` bigint(20) NOT NULL COMMENT '创建用户id',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NOT NULL COMMENT '更新用户id',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否逻辑删除，0:未删除；1:已删除',
  PRIMARY KEY (`menu_id`) USING BTREE,
  UNIQUE INDEX `menu_code`(`menu_code`) USING BTREE,
  INDEX `service_id`(`service_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '系统资源-菜单信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, 'open-cloud-base-server', 'system', '系统管理', '系统管理', '/', '', 'md-folder', '_self', 0, 1, b'1', 0, '2018-07-29 21:20:10', 0, '2019-05-25 01:49:23', b'0');
INSERT INTO `sys_menu` VALUES (2, 13, 'open-cloud-base-server', 'gatewayIpLimit', '访问控制', '来源IP/域名访问控制,白名单、黑名单', '/', 'gateway/ip-limit/index', 'md-document', '_self', 1, 1, b'1', 0, '2018-07-29 21:20:13', 0, '2019-07-11 18:05:32', b'1');
INSERT INTO `sys_menu` VALUES (3, 1, 'open-cloud-base-server', 'systemMenu', '功能菜单', '功能菜单资源', '/', 'system/menu/index', 'md-list', '_self', 3, 1, b'1', 0, '2018-07-29 21:20:13', 0, '2019-07-11 18:03:23', b'1');
INSERT INTO `sys_menu` VALUES (5, 13, 'open-cloud-base-server', 'gatewayRoute', '网关路由', '网关路由', '/', 'gateway/route/index', 'md-document', '_self', 5, 1, b'1', 0, '2018-07-29 21:20:13', 0, '2019-07-11 18:04:59', b'1');
INSERT INTO `sys_menu` VALUES (6, 13, 'open-cloud-base-server', 'systemApi', 'API列表', 'API接口资源', '/', 'system/api/index', 'md-document', '_self', 0, 1, b'1', 0, '2018-07-29 21:20:13', 0, '2019-03-13 21:48:12', b'0');
INSERT INTO `sys_menu` VALUES (8, 1, 'open-cloud-base-server', 'systemRole', '角色管理', '角色信息管理', '/', 'system/role/index', 'md-people', '_self', 8, 1, b'1', 0, '2018-12-27 15:26:54', 0, '2019-07-11 18:03:10', b'0');
INSERT INTO `sys_menu` VALUES (9, 1, 'open-cloud-base-server', 'systemApp', '应用管理', '应用信息管理', '/', 'system/app/index', 'md-apps', '_self', 0, 1, b'1', 0, '2018-12-27 15:41:52', 0, '2019-07-11 18:03:45', b'1');
INSERT INTO `sys_menu` VALUES (10, 1, 'open-cloud-base-server', 'systemUser', '用户管理', '系统用户', '/', 'system/user/index', 'md-person', '_self', 0, 1, b'1', 0, '2018-12-27 15:46:29', 0, '2019-07-11 18:03:55', b'0');
INSERT INTO `sys_menu` VALUES (11, 13, 'open-cloud-base-server', 'apiDebug', '接口调试', 'swagger接口调试', 'http://', 'localhost:8888', 'md-document', '_blank', 0, 1, b'1', 0, '2019-01-10 20:47:19', 0, '2019-05-25 03:26:47', b'1');
INSERT INTO `sys_menu` VALUES (12, 13, 'open-cloud-base-server', 'gatewayLogs', '访问日志', '', '/', 'gateway/logs/index', 'md-document', '_self', 0, 1, b'1', 0, '2019-01-28 02:37:42', 0, '2019-02-25 00:16:40', b'1');
INSERT INTO `sys_menu` VALUES (13, 0, 'open-cloud-base-server', 'gateway', 'API网关', 'API网关', '/', '', 'md-folder', '_self', 0, 1, b'1', 0, '2019-02-25 00:15:09', 0, '2019-03-18 04:44:20', b'1');
INSERT INTO `sys_menu` VALUES (14, 13, 'open-cloud-base-server', 'gatewayRateLimit', '流量控制', 'API限流', '/', 'gateway/rate-limit/index', 'md-document', '_self', 2, 1, b'1', 0, '2019-03-13 21:47:20', 0, '2019-07-11 18:05:18', b'1');
INSERT INTO `sys_menu` VALUES (15, 0, 'open-cloud-base-server', 'task', '任务调度', '任务调度', '/', '', 'md-document', '_self', 0, 1, b'1', 0, '2019-04-01 16:30:27', 0, '2019-07-11 18:07:50', b'1');
INSERT INTO `sys_menu` VALUES (16, 15, 'open-cloud-base-server', 'job', '定时任务', '定时任务列表', '/', 'task/job/index', 'md-document', '_self', 0, 1, b'1', 0, '2019-04-01 16:31:15', 0, '2019-07-11 18:08:12', b'1');
INSERT INTO `sys_menu` VALUES (17, 0, 'open-cloud-base-server', 'message', '消息管理', '消息管理', '/', '', 'md-document', '_self', 0, 1, b'1', 0, '2019-04-04 16:37:23', 0, '2019-04-04 16:37:23', b'1');
INSERT INTO `sys_menu` VALUES (18, 17, 'open-cloud-base-server', 'webhook', '异步通知日志', '异步通知日志', '/', 'msg/webhook/index', 'md-document', '_self', 0, 1, b'1', 0, '2019-04-04 16:38:21', 0, '2019-07-11 18:06:23', b'1');
INSERT INTO `sys_menu` VALUES (19, 15, 'open-cloud-base-server', 'taskLogs', '调度日志', '调度日志', '/', 'task/logs/index', 'md-document', '_self', 0, 1, b'1', 0, '2019-05-24 18:17:49', 0, '2019-07-11 18:08:26', b'1');
INSERT INTO `sys_menu` VALUES (1141579952217567234, 0, 'open-cloud-base-server', 'monitor', '系统监控', '系统监控', '/', '', 'md-document', '_self', 0, 1, b'0', 0, '2019-06-20 13:34:04', 0, '2019-06-20 13:34:04', b'1');
INSERT INTO `sys_menu` VALUES (1141580147030405121, 1141579952217567234, 'open-cloud-base-server', 'SpringBootAdmin', 'SpringBootAdmin', 'SpringBootAdmin', 'http://', 'localhost:8849', 'md-document', '_blank', 0, 1, b'0', 0, '2019-06-20 13:34:51', 0, '2019-07-11 18:11:58', b'1');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `tenant_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '租户id',
  `role_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色编码',
  `role_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '角色名称',
  `role_desc` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '角色描述',
  `status` tinyint(3) NOT NULL DEFAULT 1 COMMENT '状态:0-无效 1-有效',
  `is_persist` bit(1) NOT NULL DEFAULT b'0' COMMENT '保留数据0-否 1-是 不允许删除',
  `create_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '创建用户id',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '更新用户id',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否逻辑删除，0:未删除；1:已删除',
  PRIMARY KEY (`role_id`) USING BTREE,
  UNIQUE INDEX `role_code`(`role_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '系统角色-基础信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 0, 'admin', '系统管理员', '系统管理员', 1, b'1', 0, '2018-07-29 21:14:54', 0, '2019-05-25 03:06:57', b'0');
INSERT INTO `sys_role` VALUES (2, 0, 'operator', '运营人员', '运营人员', 1, b'1', 0, '2018-07-29 21:14:54', 0, '2019-05-25 15:14:56', b'0');
INSERT INTO `sys_role` VALUES (3, 0, 'support', '客服', '客服', 0, b'1', 0, '2018-07-29 21:14:54', 0, '2020-06-04 17:58:27', b'0');
INSERT INTO `sys_role` VALUES (1268505420728918017, 0, 'test', '测试', '', 1, b'0', 0, '2020-06-04 19:30:35', 0, '2020-06-04 19:30:35', b'0');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `relate_id` bigint(20) NOT NULL COMMENT '角色ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`relate_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (1, 1, 100);
INSERT INTO `sys_role_dept` VALUES (2, 1, 101);
INSERT INTO `sys_role_dept` VALUES (3, 1, 105);

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user`  (
  `relate_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `create_user_id` bigint(20) NULL DEFAULT 0 COMMENT '创建用户id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT 0,
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_delete` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`relate_id`) USING BTREE,
  INDEX `fk_user`(`user_id`) USING BTREE,
  INDEX `fk_role`(`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '系统角色-用户关联' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
INSERT INTO `sys_role_user` VALUES (1, 1, 1, 0, '2019-07-30 15:51:08', 0, '2019-07-30 15:51:08', b'0');
INSERT INTO `sys_role_user` VALUES (2, 2, 2, 0, '2019-07-30 15:51:08', 0, '2019-07-30 15:51:08', b'0');
INSERT INTO `sys_role_user` VALUES (1268490071270813698, 557063237640650752, 1, 0, '2020-06-04 18:29:35', 0, '2020-06-04 18:29:35', b'0');
INSERT INTO `sys_role_user` VALUES (1268495020318842881, 1, 3, 0, '2020-06-04 18:49:15', 0, '2020-06-04 18:49:15', b'0');
INSERT INTO `sys_role_user` VALUES (1268495020356591618, 557063237640650752, 3, 0, '2020-06-04 18:49:15', 0, '2020-06-04 18:49:15', b'0');
INSERT INTO `sys_role_user` VALUES (1268505552425869314, 1268503318577319937, 1268505420728918017, 0, '2020-06-04 19:31:06', 0, '2020-06-04 19:31:06', b'0');

-- ----------------------------
-- Table structure for sys_tenant
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant`;
CREATE TABLE `sys_tenant`  (
  `tenant_id` bigint(20) NOT NULL COMMENT '租户ID',
  `tenant_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '租户名称',
  `tenant_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '租户描述',
  `create_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '创建用户id',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '更新用户id',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否逻辑删除，0:未删除；1:已删除',
  PRIMARY KEY (`tenant_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '租户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_tenant_modules
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant_modules`;
CREATE TABLE `sys_tenant_modules`  (
  `module_id` bigint(20) NOT NULL COMMENT '模块ID',
  `tenant_id` bigint(20) NOT NULL COMMENT '租户ID',
  `service_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '服务名称',
  `module_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模块描述',
  `is_persist` tinyint(3) NOT NULL DEFAULT 0 COMMENT '保留数据0-否 1-是 不允许删除',
  `create_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '创建用户id',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '更新用户id',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否逻辑删除，0:未删除；1:已删除',
  PRIMARY KEY (`module_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '租户模块' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `company_id` bigint(20) NULL DEFAULT 0 COMMENT '企业ID',
  `tenant_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '租户id',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登陆账号',
  `nick_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `user_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'normal' COMMENT '用户类型:super-超级管理员 normal-普通管理员',
  `user_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '描述',
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '状态:0-禁用 1-正常 2-锁定',
  `is_persist` bit(1) NOT NULL DEFAULT b'0' COMMENT '保留数据0-否 1-是 不允许删除',
  `create_user_id` bigint(20) NULL DEFAULT 0 COMMENT '创建用户id',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT 0 COMMENT '更新用户id',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否逻辑删除，0:未删除；1:已删除',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `user_name`(`user_name`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户-管理员信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, NULL, 0, 'admin', '超级管理员', '', '515608851@qq.com', '', 'super', '123', 1, b'1', 0, '2018-12-10 13:20:45', 0, '2020-06-04 18:55:59', b'0');
INSERT INTO `sys_user` VALUES (2, NULL, 0, 'test', '测试用户', '', '', '', 'normal', '', 1, b'0', 0, '2019-03-18 04:50:25', 0, NULL, b'0');
INSERT INTO `sys_user` VALUES (1268503318577319937, 0, 0, 'test1', 'test1', '', '', '', 'normal', '', 0, b'0', 0, '2020-06-04 19:22:13', 0, '2020-06-04 20:01:02', b'0');

-- ----------------------------
-- Table structure for t_account_logs
-- ----------------------------
DROP TABLE IF EXISTS `t_account_logs`;
CREATE TABLE `t_account_logs`  (
  `id` bigint(20) NOT NULL,
  `login_time` datetime(0) NOT NULL COMMENT '登录时间',
  `login_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录Ip',
  `login_agent` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录设备',
  `login_nums` int(11) NOT NULL COMMENT '登录次数',
  `user_id` bigint(20) NOT NULL COMMENT '登录用户id',
  `account` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录帐号',
  `account_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录帐号类型',
  `account_id` bigint(20) NOT NULL COMMENT '账号ID',
  `domain` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号域',
  `create_user_id` bigint(20) NOT NULL COMMENT '创建用户id',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user_id` bigint(20) NOT NULL COMMENT '更新用户id',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否逻辑删除，0:未删除；1:已删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `account_id`(`account_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '登录日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_account_logs
-- ----------------------------
INSERT INTO `t_account_logs` VALUES (1207480929383403522, '2019-12-19 10:00:59', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 1, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:01:02', 0, '2019-12-19 10:01:02', b'0');
INSERT INTO `t_account_logs` VALUES (1207481041576841217, '2019-12-19 10:01:28', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 2, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:01:28', 0, '2019-12-19 10:01:28', b'0');
INSERT INTO `t_account_logs` VALUES (1207481547539927042, '2019-12-19 10:03:29', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 3, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:03:29', 0, '2019-12-19 10:03:29', b'0');
INSERT INTO `t_account_logs` VALUES (1207481603647131650, '2019-12-19 10:03:42', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 4, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:03:42', 0, '2019-12-19 10:03:42', b'0');
INSERT INTO `t_account_logs` VALUES (1207481849248796673, '2019-12-19 10:04:41', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 5, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:04:41', 0, '2019-12-19 10:04:41', b'0');
INSERT INTO `t_account_logs` VALUES (1207482102391820289, '2019-12-19 10:05:41', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 6, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:05:41', 0, '2019-12-19 10:05:41', b'0');
INSERT INTO `t_account_logs` VALUES (1207482705146859521, '2019-12-19 10:08:05', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 7, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:08:05', 0, '2019-12-19 10:08:05', b'0');
INSERT INTO `t_account_logs` VALUES (1207482757370138625, '2019-12-19 10:08:17', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 8, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:08:17', 0, '2019-12-19 10:08:17', b'0');
INSERT INTO `t_account_logs` VALUES (1207482881739640833, '2019-12-19 10:08:47', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 9, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:08:47', 0, '2019-12-19 10:08:47', b'0');
INSERT INTO `t_account_logs` VALUES (1207482983522816001, '2019-12-19 10:09:11', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 10, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:09:11', 0, '2019-12-19 10:09:11', b'0');
INSERT INTO `t_account_logs` VALUES (1207483329997492225, '2019-12-19 10:10:34', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 11, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:10:34', 0, '2019-12-19 10:10:34', b'0');
INSERT INTO `t_account_logs` VALUES (1207483866356699138, '2019-12-19 10:12:42', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 12, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:12:42', 0, '2019-12-19 10:12:42', b'0');
INSERT INTO `t_account_logs` VALUES (1207489563207192578, '2019-12-19 10:35:20', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 13, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:35:20', 0, '2019-12-19 10:35:20', b'0');
INSERT INTO `t_account_logs` VALUES (1207489670593957890, '2019-12-19 10:35:46', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 14, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:35:46', 0, '2019-12-19 10:35:46', b'0');
INSERT INTO `t_account_logs` VALUES (1207489674784067585, '2019-12-19 10:35:47', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 15, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:35:47', 0, '2019-12-19 10:35:47', b'0');
INSERT INTO `t_account_logs` VALUES (1207489781323583489, '2019-12-19 10:36:12', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 16, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:36:12', 0, '2019-12-19 10:36:12', b'0');
INSERT INTO `t_account_logs` VALUES (1207489802894888962, '2019-12-19 10:36:17', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 17, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:36:17', 0, '2019-12-19 10:36:17', b'0');
INSERT INTO `t_account_logs` VALUES (1207489967420657666, '2019-12-19 10:36:56', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 18, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:36:56', 0, '2019-12-19 10:36:56', b'0');
INSERT INTO `t_account_logs` VALUES (1207492315660472322, '2019-12-19 10:46:16', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 19, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:46:16', 0, '2019-12-19 10:46:16', b'0');
INSERT INTO `t_account_logs` VALUES (1207492454655512577, '2019-12-19 10:46:49', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 20, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:46:49', 0, '2019-12-19 10:46:49', b'0');
INSERT INTO `t_account_logs` VALUES (1207492539468533761, '2019-12-19 10:47:10', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 21, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:47:10', 0, '2019-12-19 10:47:10', b'0');
INSERT INTO `t_account_logs` VALUES (1207492699439288322, '2019-12-19 10:47:48', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 22, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 10:47:48', 0, '2019-12-19 10:47:48', b'0');
INSERT INTO `t_account_logs` VALUES (1207496481824886785, '2019-12-19 11:02:50', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 23, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 11:02:50', 0, '2019-12-19 11:02:50', b'0');
INSERT INTO `t_account_logs` VALUES (1207505074284449793, '2019-12-19 11:36:58', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 24, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 11:36:58', 0, '2019-12-19 11:36:58', b'0');
INSERT INTO `t_account_logs` VALUES (1207508823367274497, '2019-12-19 11:51:52', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 25, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 11:51:52', 0, '2019-12-19 11:51:52', b'0');
INSERT INTO `t_account_logs` VALUES (1207508899959459842, '2019-12-19 11:52:10', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 26, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 11:52:10', 0, '2019-12-19 11:52:10', b'0');
INSERT INTO `t_account_logs` VALUES (1207508964899868673, '2019-12-19 11:52:26', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 27, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 11:52:26', 0, '2019-12-19 11:52:26', b'0');
INSERT INTO `t_account_logs` VALUES (1207509747976372225, '2019-12-19 11:55:33', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 28, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 11:55:33', 0, '2019-12-19 11:55:33', b'0');
INSERT INTO `t_account_logs` VALUES (1207509956823351297, '2019-12-19 11:56:22', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 29, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 11:56:22', 0, '2019-12-19 11:56:22', b'0');
INSERT INTO `t_account_logs` VALUES (1207510012779560961, '2019-12-19 11:56:36', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 30, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 11:56:36', 0, '2019-12-19 11:56:36', b'0');
INSERT INTO `t_account_logs` VALUES (1207510061152468994, '2019-12-19 11:56:47', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 31, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 11:56:47', 0, '2019-12-19 11:56:47', b'0');
INSERT INTO `t_account_logs` VALUES (1207512590481358849, '2019-12-19 12:06:50', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 32, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 12:06:50', 0, '2019-12-19 12:06:50', b'0');
INSERT INTO `t_account_logs` VALUES (1207512625340219394, '2019-12-19 12:06:59', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 33, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 12:06:59', 0, '2019-12-19 12:06:59', b'0');
INSERT INTO `t_account_logs` VALUES (1207512996032806913, '2019-12-19 12:08:27', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 34, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 12:08:27', 0, '2019-12-19 12:08:27', b'0');
INSERT INTO `t_account_logs` VALUES (1207514405813878785, '2019-12-19 12:14:03', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 35, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 12:14:03', 0, '2019-12-19 12:14:03', b'0');
INSERT INTO `t_account_logs` VALUES (1207514484742291457, '2019-12-19 12:14:22', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 36, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 12:14:22', 0, '2019-12-19 12:14:22', b'0');
INSERT INTO `t_account_logs` VALUES (1207514530040774657, '2019-12-19 12:14:33', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 37, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 12:14:33', 0, '2019-12-19 12:14:33', b'0');
INSERT INTO `t_account_logs` VALUES (1207516274141458433, '2019-12-19 12:21:28', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 38, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 12:21:28', 0, '2019-12-19 12:21:28', b'0');
INSERT INTO `t_account_logs` VALUES (1207516345784365057, '2019-12-19 12:21:46', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 39, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 12:21:46', 0, '2019-12-19 12:21:46', b'0');
INSERT INTO `t_account_logs` VALUES (1207516401132400642, '2019-12-19 12:21:59', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 40, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 12:21:59', 0, '2019-12-19 12:21:59', b'0');
INSERT INTO `t_account_logs` VALUES (1207516484569690114, '2019-12-19 12:22:19', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 41, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 12:22:19', 0, '2019-12-19 12:22:19', b'0');
INSERT INTO `t_account_logs` VALUES (1207619456754589697, '2019-12-19 19:11:29', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 42, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-19 19:11:29', 0, '2019-12-19 19:11:29', b'0');
INSERT INTO `t_account_logs` VALUES (1207876042018283521, '2019-12-20 12:11:04', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 43, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-20 12:11:04', 0, '2019-12-20 12:11:04', b'0');
INSERT INTO `t_account_logs` VALUES (1207876121798139905, '2019-12-20 12:11:23', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 44, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-20 12:11:23', 0, '2019-12-20 12:11:23', b'0');
INSERT INTO `t_account_logs` VALUES (1207880484490911745, '2019-12-20 12:28:43', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 45, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-20 12:28:43', 0, '2019-12-20 12:28:43', b'0');
INSERT INTO `t_account_logs` VALUES (1207893360911917057, '2019-12-20 13:19:53', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 46, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-20 13:19:53', 0, '2019-12-20 13:19:53', b'0');
INSERT INTO `t_account_logs` VALUES (1207899721972391937, '2019-12-20 13:45:10', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 47, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-20 13:45:10', 0, '2019-12-20 13:45:10', b'0');
INSERT INTO `t_account_logs` VALUES (1207899735809400834, '2019-12-20 13:45:13', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 48, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-20 13:45:13', 0, '2019-12-20 13:45:13', b'0');
INSERT INTO `t_account_logs` VALUES (1207899801571893249, '2019-12-20 13:45:29', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 49, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-20 13:45:29', 0, '2019-12-20 13:45:29', b'0');
INSERT INTO `t_account_logs` VALUES (1207900105289834498, '2019-12-20 13:46:41', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 50, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-20 13:46:41', 0, '2019-12-20 13:46:41', b'0');
INSERT INTO `t_account_logs` VALUES (1207902550137106434, '2019-12-20 13:56:24', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 51, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-20 13:56:24', 0, '2019-12-20 13:56:24', b'0');
INSERT INTO `t_account_logs` VALUES (1207902673206374402, '2019-12-20 13:56:53', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 52, 521677655146233856, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-20 13:56:53', 0, '2019-12-20 13:56:53', b'0');
INSERT INTO `t_account_logs` VALUES (1207902882896408578, '2019-12-20 13:57:43', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 1, 9223372036854775807, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-20 13:57:43', 0, '2019-12-20 13:57:43', b'0');
INSERT INTO `t_account_logs` VALUES (1207902945458647042, '2019-12-20 13:57:58', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 2, 9223372036854775807, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-20 13:57:58', 0, '2019-12-20 13:57:58', b'0');
INSERT INTO `t_account_logs` VALUES (1207903043164958722, '2019-12-20 13:58:21', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 3, 9223372036854775807, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-20 13:58:21', 0, '2019-12-20 13:58:21', b'0');
INSERT INTO `t_account_logs` VALUES (1207905561429274626, '2019-12-20 14:08:22', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 4, 9223372036854775807, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-20 14:08:22', 0, '2019-12-20 14:08:22', b'0');
INSERT INTO `t_account_logs` VALUES (1207915894634049537, '2019-12-20 14:49:25', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 5, 9223372036854775807, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-20 14:49:25', 0, '2019-12-20 14:49:25', b'0');
INSERT INTO `t_account_logs` VALUES (1207915924610740225, '2019-12-20 14:49:33', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 6, 9223372036854775807, 'admin', 'username', 521677655368531968, '@admin.com', 0, '2019-12-20 14:49:33', 0, '2019-12-20 14:49:33', b'0');
INSERT INTO `t_account_logs` VALUES (1207961978429313026, '2019-12-20 17:52:33', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 1, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 17:52:33', 0, '2019-12-20 17:52:33', b'0');
INSERT INTO `t_account_logs` VALUES (1207963327720144897, '2019-12-20 17:57:55', '192.168.0.118', 'PostmanRuntime/7.21.0', 2, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 17:57:55', 0, '2019-12-20 17:57:55', b'0');
INSERT INTO `t_account_logs` VALUES (1207966094887022594, '2019-12-20 18:08:55', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 3, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 18:08:55', 0, '2019-12-20 18:08:55', b'0');
INSERT INTO `t_account_logs` VALUES (1207966435875536897, '2019-12-20 18:10:16', '192.168.0.118', 'PostmanRuntime/7.21.0', 4, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 18:10:16', 0, '2019-12-20 18:10:16', b'0');
INSERT INTO `t_account_logs` VALUES (1207968175618306050, '2019-12-20 18:17:11', '192.168.0.118', 'PostmanRuntime/7.21.0', 5, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 18:17:11', 0, '2019-12-20 18:17:11', b'0');
INSERT INTO `t_account_logs` VALUES (1207969311343538178, '2019-12-20 18:21:41', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 6, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 18:21:41', 0, '2019-12-20 18:21:41', b'0');
INSERT INTO `t_account_logs` VALUES (1207969818975956994, '2019-12-20 18:23:42', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 7, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 18:23:42', 0, '2019-12-20 18:23:42', b'0');
INSERT INTO `t_account_logs` VALUES (1207970003609219074, '2019-12-20 18:24:27', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 8, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 18:24:27', 0, '2019-12-20 18:24:27', b'0');
INSERT INTO `t_account_logs` VALUES (1207972653822160898, '2019-12-20 18:34:58', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 9, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 18:34:58', 0, '2019-12-20 18:34:58', b'0');
INSERT INTO `t_account_logs` VALUES (1207972995699875841, '2019-12-20 18:36:20', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 10, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 18:36:20', 0, '2019-12-20 18:36:20', b'0');
INSERT INTO `t_account_logs` VALUES (1207973209898749954, '2019-12-20 18:37:11', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 11, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 18:37:11', 0, '2019-12-20 18:37:11', b'0');
INSERT INTO `t_account_logs` VALUES (1207973954341572610, '2019-12-20 18:40:08', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 12, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 18:40:08', 0, '2019-12-20 18:40:08', b'0');
INSERT INTO `t_account_logs` VALUES (1207977346677927938, '2019-12-20 18:53:37', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 13, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 18:53:37', 0, '2019-12-20 18:53:37', b'0');
INSERT INTO `t_account_logs` VALUES (1207977405540790273, '2019-12-20 18:53:51', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 14, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 18:53:51', 0, '2019-12-20 18:53:51', b'0');
INSERT INTO `t_account_logs` VALUES (1207979833694662658, '2019-12-20 19:03:30', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 15, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 19:03:30', 0, '2019-12-20 19:03:30', b'0');
INSERT INTO `t_account_logs` VALUES (1207980193347842050, '2019-12-20 19:04:56', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 16, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 19:04:56', 0, '2019-12-20 19:04:56', b'0');
INSERT INTO `t_account_logs` VALUES (1208002869261029377, '2019-12-20 20:35:02', '127.0.0.1', 'PostmanRuntime/7.21.0', 17, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 20:35:02', 0, '2019-12-20 20:35:02', b'0');
INSERT INTO `t_account_logs` VALUES (1208004508596416513, '2019-12-20 20:41:33', '127.0.0.1', 'PostmanRuntime/7.21.0', 18, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 20:41:33', 0, '2019-12-20 20:41:33', b'0');
INSERT INTO `t_account_logs` VALUES (1208004738343612418, '2019-12-20 20:42:28', '127.0.0.1', 'PostmanRuntime/7.21.0', 19, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 20:42:28', 0, '2019-12-20 20:42:28', b'0');
INSERT INTO `t_account_logs` VALUES (1208004759852003329, '2019-12-20 20:42:33', '127.0.0.1', 'PostmanRuntime/7.21.0', 20, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 20:42:33', 0, '2019-12-20 20:42:33', b'0');
INSERT INTO `t_account_logs` VALUES (1208007221031813122, '2019-12-20 20:52:20', '127.0.0.1', 'PostmanRuntime/7.21.0', 21, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 20:52:20', 0, '2019-12-20 20:52:20', b'0');
INSERT INTO `t_account_logs` VALUES (1208007610468786177, '2019-12-20 20:53:53', '127.0.0.1', 'PostmanRuntime/7.21.0', 22, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 20:53:53', 0, '2019-12-20 20:53:53', b'0');
INSERT INTO `t_account_logs` VALUES (1208007635626221570, '2019-12-20 20:53:59', '127.0.0.1', 'PostmanRuntime/7.21.0', 23, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 20:53:59', 0, '2019-12-20 20:53:59', b'0');
INSERT INTO `t_account_logs` VALUES (1208008988956057601, '2019-12-20 20:59:21', '127.0.0.1', 'PostmanRuntime/7.21.0', 24, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 20:59:21', 0, '2019-12-20 20:59:21', b'0');
INSERT INTO `t_account_logs` VALUES (1208009336202485761, '2019-12-20 21:00:44', '127.0.0.1', 'PostmanRuntime/7.21.0', 25, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 21:00:44', 0, '2019-12-20 21:00:44', b'0');
INSERT INTO `t_account_logs` VALUES (1208009380758577154, '2019-12-20 21:00:55', '127.0.0.1', 'PostmanRuntime/7.21.0', 26, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 21:00:55', 0, '2019-12-20 21:00:55', b'0');
INSERT INTO `t_account_logs` VALUES (1208010167899443202, '2019-12-20 21:04:02', '127.0.0.1', 'PostmanRuntime/7.21.0', 27, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 21:04:02', 0, '2019-12-20 21:04:02', b'0');
INSERT INTO `t_account_logs` VALUES (1208010455418982402, '2019-12-20 21:05:11', '127.0.0.1', 'PostmanRuntime/7.21.0', 28, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 21:05:11', 0, '2019-12-20 21:05:11', b'0');
INSERT INTO `t_account_logs` VALUES (1208010532518678529, '2019-12-20 21:05:29', '127.0.0.1', 'PostmanRuntime/7.21.0', 29, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 21:05:29', 0, '2019-12-20 21:05:29', b'0');
INSERT INTO `t_account_logs` VALUES (1208011731967987714, '2019-12-20 21:10:15', '127.0.0.1', 'PostmanRuntime/7.21.0', 30, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 21:10:15', 0, '2019-12-20 21:10:15', b'0');
INSERT INTO `t_account_logs` VALUES (1208011794505060353, '2019-12-20 21:10:30', '127.0.0.1', 'PostmanRuntime/7.21.0', 31, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 21:10:30', 0, '2019-12-20 21:10:30', b'0');
INSERT INTO `t_account_logs` VALUES (1208011876021358593, '2019-12-20 21:10:50', '127.0.0.1', 'PostmanRuntime/7.21.0', 32, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 21:10:50', 0, '2019-12-20 21:10:50', b'0');
INSERT INTO `t_account_logs` VALUES (1208013103887077378, '2019-12-20 21:15:42', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 33, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 21:15:42', 0, '2019-12-20 21:15:42', b'0');
INSERT INTO `t_account_logs` VALUES (1208013884480606209, '2019-12-20 21:18:49', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 34, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 21:18:49', 0, '2019-12-20 21:18:49', b'0');
INSERT INTO `t_account_logs` VALUES (1208014082825048066, '2019-12-20 21:19:36', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 35, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 21:19:36', 0, '2019-12-20 21:19:36', b'0');
INSERT INTO `t_account_logs` VALUES (1208014796997578754, '2019-12-20 21:22:26', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 36, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 21:22:26', 0, '2019-12-20 21:22:26', b'0');
INSERT INTO `t_account_logs` VALUES (1208016083382226945, '2019-12-20 21:27:33', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 37, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 21:27:33', 0, '2019-12-20 21:27:33', b'0');
INSERT INTO `t_account_logs` VALUES (1208017263369965569, '2019-12-20 21:32:14', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 38, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 21:32:14', 0, '2019-12-20 21:32:14', b'0');
INSERT INTO `t_account_logs` VALUES (1208019546170281986, '2019-12-20 21:41:18', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 39, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 21:41:18', 0, '2019-12-20 21:41:18', b'0');
INSERT INTO `t_account_logs` VALUES (1208019900928708610, '2019-12-20 21:42:43', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 40, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 21:42:43', 0, '2019-12-20 21:42:43', b'0');
INSERT INTO `t_account_logs` VALUES (1208021686305509378, '2019-12-20 21:49:49', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 41, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-20 21:49:49', 0, '2019-12-20 21:49:49', b'0');
INSERT INTO `t_account_logs` VALUES (1208566613699559425, '2019-12-22 09:55:09', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 42, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 09:55:09', 0, '2019-12-22 09:55:09', b'0');
INSERT INTO `t_account_logs` VALUES (1208566656825393154, '2019-12-22 09:55:20', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 43, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 09:55:20', 0, '2019-12-22 09:55:20', b'0');
INSERT INTO `t_account_logs` VALUES (1208566807350575105, '2019-12-22 09:55:56', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 44, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 09:55:56', 0, '2019-12-22 09:55:56', b'0');
INSERT INTO `t_account_logs` VALUES (1208566845359357954, '2019-12-22 09:56:05', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 45, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 09:56:05', 0, '2019-12-22 09:56:05', b'0');
INSERT INTO `t_account_logs` VALUES (1208566957947060226, '2019-12-22 09:56:32', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 46, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 09:56:32', 0, '2019-12-22 09:56:32', b'0');
INSERT INTO `t_account_logs` VALUES (1208566987596595201, '2019-12-22 09:56:39', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 47, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 09:56:39', 0, '2019-12-22 09:56:39', b'0');
INSERT INTO `t_account_logs` VALUES (1208568528516775937, '2019-12-22 10:02:46', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 48, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:02:46', 0, '2019-12-22 10:02:46', b'0');
INSERT INTO `t_account_logs` VALUES (1208569491419922434, '2019-12-22 10:06:36', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 49, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:06:36', 0, '2019-12-22 10:06:36', b'0');
INSERT INTO `t_account_logs` VALUES (1208569616099803137, '2019-12-22 10:07:05', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 50, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:07:05', 0, '2019-12-22 10:07:05', b'0');
INSERT INTO `t_account_logs` VALUES (1208570507284545537, '2019-12-22 10:10:38', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 51, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:10:38', 0, '2019-12-22 10:10:38', b'0');
INSERT INTO `t_account_logs` VALUES (1208572525906591746, '2019-12-22 10:18:39', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 52, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:18:39', 0, '2019-12-22 10:18:39', b'0');
INSERT INTO `t_account_logs` VALUES (1208572573486776321, '2019-12-22 10:18:50', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 53, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:18:50', 0, '2019-12-22 10:18:50', b'0');
INSERT INTO `t_account_logs` VALUES (1208572680399585282, '2019-12-22 10:19:16', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 54, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:19:16', 0, '2019-12-22 10:19:16', b'0');
INSERT INTO `t_account_logs` VALUES (1208573071048671233, '2019-12-22 10:20:49', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 55, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:20:49', 0, '2019-12-22 10:20:49', b'0');
INSERT INTO `t_account_logs` VALUES (1208573800249393153, '2019-12-22 10:23:43', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 56, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:23:43', 0, '2019-12-22 10:23:43', b'0');
INSERT INTO `t_account_logs` VALUES (1208573911692050433, '2019-12-22 10:24:09', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 57, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:24:09', 0, '2019-12-22 10:24:09', b'0');
INSERT INTO `t_account_logs` VALUES (1208576496041820162, '2019-12-22 10:34:26', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 58, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:34:26', 0, '2019-12-22 10:34:26', b'0');
INSERT INTO `t_account_logs` VALUES (1208576692003897345, '2019-12-22 10:35:12', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 59, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:35:12', 0, '2019-12-22 10:35:12', b'0');
INSERT INTO `t_account_logs` VALUES (1208576764787654657, '2019-12-22 10:35:30', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 60, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:35:30', 0, '2019-12-22 10:35:30', b'0');
INSERT INTO `t_account_logs` VALUES (1208577071898787842, '2019-12-22 10:36:43', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 61, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:36:43', 0, '2019-12-22 10:36:43', b'0');
INSERT INTO `t_account_logs` VALUES (1208577701715476482, '2019-12-22 10:39:13', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 62, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:39:13', 0, '2019-12-22 10:39:13', b'0');
INSERT INTO `t_account_logs` VALUES (1208577828836442114, '2019-12-22 10:39:43', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 63, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:39:43', 0, '2019-12-22 10:39:43', b'0');
INSERT INTO `t_account_logs` VALUES (1208578140364177409, '2019-12-22 10:40:58', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 64, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:40:58', 0, '2019-12-22 10:40:58', b'0');
INSERT INTO `t_account_logs` VALUES (1208578166729572353, '2019-12-22 10:41:04', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 65, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:41:04', 0, '2019-12-22 10:41:04', b'0');
INSERT INTO `t_account_logs` VALUES (1208578302222368770, '2019-12-22 10:41:36', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 66, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:41:36', 0, '2019-12-22 10:41:36', b'0');
INSERT INTO `t_account_logs` VALUES (1208578373210963969, '2019-12-22 10:41:53', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 67, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 10:41:53', 0, '2019-12-22 10:41:53', b'0');
INSERT INTO `t_account_logs` VALUES (1208583977459564545, '2019-12-22 11:04:09', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 68, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 11:04:09', 0, '2019-12-22 11:04:09', b'0');
INSERT INTO `t_account_logs` VALUES (1208584359376109569, '2019-12-22 11:05:40', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 69, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 11:05:40', 0, '2019-12-22 11:05:40', b'0');
INSERT INTO `t_account_logs` VALUES (1208584802483359745, '2019-12-22 11:07:26', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 70, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 11:07:26', 0, '2019-12-22 11:07:26', b'0');
INSERT INTO `t_account_logs` VALUES (1208585010789273601, '2019-12-22 11:08:16', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 71, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 11:08:16', 0, '2019-12-22 11:08:16', b'0');
INSERT INTO `t_account_logs` VALUES (1208586219331825666, '2019-12-22 11:13:04', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 72, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 11:13:04', 0, '2019-12-22 11:13:04', b'0');
INSERT INTO `t_account_logs` VALUES (1208586261467803649, '2019-12-22 11:13:14', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 73, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 11:13:14', 0, '2019-12-22 11:13:14', b'0');
INSERT INTO `t_account_logs` VALUES (1208622970721136641, '2019-12-22 13:39:06', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 74, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 13:39:06', 0, '2019-12-22 13:39:06', b'0');
INSERT INTO `t_account_logs` VALUES (1208622998906859521, '2019-12-22 13:39:13', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 75, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 13:39:13', 0, '2019-12-22 13:39:13', b'0');
INSERT INTO `t_account_logs` VALUES (1208623084965588994, '2019-12-22 13:39:33', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 76, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 13:39:33', 0, '2019-12-22 13:39:33', b'0');
INSERT INTO `t_account_logs` VALUES (1208623582904971266, '2019-12-22 13:41:32', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 77, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 13:41:32', 0, '2019-12-22 13:41:32', b'0');
INSERT INTO `t_account_logs` VALUES (1208624856924811266, '2019-12-22 13:46:36', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 78, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 13:46:36', 0, '2019-12-22 13:46:36', b'0');
INSERT INTO `t_account_logs` VALUES (1208624936989880322, '2019-12-22 13:46:55', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 79, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 13:46:55', 0, '2019-12-22 13:46:55', b'0');
INSERT INTO `t_account_logs` VALUES (1208625833916624898, '2019-12-22 13:50:29', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 80, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 13:50:29', 0, '2019-12-22 13:50:29', b'0');
INSERT INTO `t_account_logs` VALUES (1208698679237808130, '2019-12-22 18:39:56', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 81, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 18:39:56', 0, '2019-12-22 18:39:56', b'0');
INSERT INTO `t_account_logs` VALUES (1208717945831981057, '2019-12-22 19:56:30', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 82, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 19:56:30', 0, '2019-12-22 19:56:30', b'0');
INSERT INTO `t_account_logs` VALUES (1208719208715993089, '2019-12-22 20:01:31', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 83, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 20:01:31', 0, '2019-12-22 20:01:31', b'0');
INSERT INTO `t_account_logs` VALUES (1208721466031685634, '2019-12-22 20:10:29', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 84, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 20:10:29', 0, '2019-12-22 20:10:29', b'0');
INSERT INTO `t_account_logs` VALUES (1208721798895845378, '2019-12-22 20:11:48', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 85, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 20:11:48', 0, '2019-12-22 20:11:48', b'0');
INSERT INTO `t_account_logs` VALUES (1208723223788994561, '2019-12-22 20:17:28', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 86, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 20:17:28', 0, '2019-12-22 20:17:28', b'0');
INSERT INTO `t_account_logs` VALUES (1208723341598605313, '2019-12-22 20:17:56', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 87, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 20:17:56', 0, '2019-12-22 20:17:56', b'0');
INSERT INTO `t_account_logs` VALUES (1208731099328757761, '2019-12-22 20:48:46', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 88, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-22 20:48:46', 0, '2019-12-22 20:48:46', b'0');
INSERT INTO `t_account_logs` VALUES (1208963699091755009, '2019-12-23 12:13:02', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 89, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-23 12:13:02', 0, '2019-12-23 12:13:02', b'0');
INSERT INTO `t_account_logs` VALUES (1208963801504075777, '2019-12-23 12:13:26', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 90, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-23 12:13:26', 0, '2019-12-23 12:13:26', b'0');
INSERT INTO `t_account_logs` VALUES (1208964000913870849, '2019-12-23 12:14:14', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 91, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-23 12:14:14', 0, '2019-12-23 12:14:14', b'0');
INSERT INTO `t_account_logs` VALUES (1208975819745316865, '2019-12-23 13:01:12', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 92, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-23 13:01:12', 0, '2019-12-23 13:01:12', b'0');
INSERT INTO `t_account_logs` VALUES (1209112533587488769, '2019-12-23 22:04:27', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 93, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-23 22:04:27', 0, '2019-12-23 22:04:27', b'0');
INSERT INTO `t_account_logs` VALUES (1209114310449852418, '2019-12-23 22:11:31', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 94, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-23 22:11:31', 0, '2019-12-23 22:11:31', b'0');
INSERT INTO `t_account_logs` VALUES (1209140499583365121, '2019-12-23 23:55:34', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 95, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-23 23:55:35', 0, '2019-12-23 23:55:35', b'0');
INSERT INTO `t_account_logs` VALUES (1209267530317811713, '2019-12-24 08:20:21', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 96, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 08:20:21', 0, '2019-12-24 08:20:21', b'0');
INSERT INTO `t_account_logs` VALUES (1209303223097372673, '2019-12-24 10:42:11', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 97, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 10:42:11', 0, '2019-12-24 10:42:11', b'0');
INSERT INTO `t_account_logs` VALUES (1209303358346899458, '2019-12-24 10:42:43', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 98, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 10:42:43', 0, '2019-12-24 10:42:43', b'0');
INSERT INTO `t_account_logs` VALUES (1209303573078487042, '2019-12-24 10:43:34', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 99, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 10:43:34', 0, '2019-12-24 10:43:34', b'0');
INSERT INTO `t_account_logs` VALUES (1209303635212906497, '2019-12-24 10:43:49', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 100, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 10:43:49', 0, '2019-12-24 10:43:49', b'0');
INSERT INTO `t_account_logs` VALUES (1209303673456570369, '2019-12-24 10:43:58', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 101, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 10:43:58', 0, '2019-12-24 10:43:58', b'0');
INSERT INTO `t_account_logs` VALUES (1209304238806806530, '2019-12-24 10:46:13', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 102, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 10:46:13', 0, '2019-12-24 10:46:13', b'0');
INSERT INTO `t_account_logs` VALUES (1209304327411478530, '2019-12-24 10:46:34', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 103, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 10:46:34', 0, '2019-12-24 10:46:34', b'0');
INSERT INTO `t_account_logs` VALUES (1209305173897523201, '2019-12-24 10:49:56', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 104, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 10:49:56', 0, '2019-12-24 10:49:56', b'0');
INSERT INTO `t_account_logs` VALUES (1209305428705685506, '2019-12-24 10:50:57', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 105, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 10:50:57', 0, '2019-12-24 10:50:57', b'0');
INSERT INTO `t_account_logs` VALUES (1209305992755687425, '2019-12-24 10:53:11', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 106, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 10:53:11', 0, '2019-12-24 10:53:11', b'0');
INSERT INTO `t_account_logs` VALUES (1209306047520714753, '2019-12-24 10:53:24', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 107, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 10:53:24', 0, '2019-12-24 10:53:24', b'0');
INSERT INTO `t_account_logs` VALUES (1209306096401133570, '2019-12-24 10:53:36', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 108, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 10:53:36', 0, '2019-12-24 10:53:36', b'0');
INSERT INTO `t_account_logs` VALUES (1209306213145391105, '2019-12-24 10:54:04', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 109, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 10:54:04', 0, '2019-12-24 10:54:04', b'0');
INSERT INTO `t_account_logs` VALUES (1209306242258055169, '2019-12-24 10:54:11', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 110, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 10:54:11', 0, '2019-12-24 10:54:11', b'0');
INSERT INTO `t_account_logs` VALUES (1209306278098382850, '2019-12-24 10:54:19', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 111, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 10:54:19', 0, '2019-12-24 10:54:19', b'0');
INSERT INTO `t_account_logs` VALUES (1209306466028367873, '2019-12-24 10:55:04', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 112, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 10:55:04', 0, '2019-12-24 10:55:04', b'0');
INSERT INTO `t_account_logs` VALUES (1209307107371003905, '2019-12-24 10:57:37', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 113, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 10:57:37', 0, '2019-12-24 10:57:37', b'0');
INSERT INTO `t_account_logs` VALUES (1209317204822339585, '2019-12-24 11:37:44', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 114, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 11:37:44', 0, '2019-12-24 11:37:44', b'0');
INSERT INTO `t_account_logs` VALUES (1209401279650213890, '2019-12-24 17:11:49', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 115, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:11:49', 0, '2019-12-24 17:11:49', b'0');
INSERT INTO `t_account_logs` VALUES (1209401328891342850, '2019-12-24 17:12:01', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 116, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:12:01', 0, '2019-12-24 17:12:01', b'0');
INSERT INTO `t_account_logs` VALUES (1209402069785784322, '2019-12-24 17:14:58', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 117, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:14:58', 0, '2019-12-24 17:14:58', b'0');
INSERT INTO `t_account_logs` VALUES (1209402169824129025, '2019-12-24 17:15:22', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 118, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:15:22', 0, '2019-12-24 17:15:22', b'0');
INSERT INTO `t_account_logs` VALUES (1209402685979373570, '2019-12-24 17:17:25', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 119, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:17:25', 0, '2019-12-24 17:17:25', b'0');
INSERT INTO `t_account_logs` VALUES (1209403291720757249, '2019-12-24 17:19:49', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 120, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:19:49', 0, '2019-12-24 17:19:49', b'0');
INSERT INTO `t_account_logs` VALUES (1209403443332263938, '2019-12-24 17:20:25', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 121, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:20:25', 0, '2019-12-24 17:20:25', b'0');
INSERT INTO `t_account_logs` VALUES (1209403538152894465, '2019-12-24 17:20:48', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 122, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:20:48', 0, '2019-12-24 17:20:48', b'0');
INSERT INTO `t_account_logs` VALUES (1209406252110852097, '2019-12-24 17:31:35', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 123, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:31:35', 0, '2019-12-24 17:31:35', b'0');
INSERT INTO `t_account_logs` VALUES (1209406439315222529, '2019-12-24 17:32:19', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 124, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:32:19', 0, '2019-12-24 17:32:19', b'0');
INSERT INTO `t_account_logs` VALUES (1209407363668520962, '2019-12-24 17:36:00', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 125, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:36:00', 0, '2019-12-24 17:36:00', b'0');
INSERT INTO `t_account_logs` VALUES (1209407432933257217, '2019-12-24 17:36:16', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 126, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:36:16', 0, '2019-12-24 17:36:16', b'0');
INSERT INTO `t_account_logs` VALUES (1209407537358843905, '2019-12-24 17:36:41', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 127, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:36:41', 0, '2019-12-24 17:36:41', b'0');
INSERT INTO `t_account_logs` VALUES (1209408060556324866, '2019-12-24 17:38:46', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 128, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:38:46', 0, '2019-12-24 17:38:46', b'0');
INSERT INTO `t_account_logs` VALUES (1209408079506190338, '2019-12-24 17:38:51', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 129, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:38:51', 0, '2019-12-24 17:38:51', b'0');
INSERT INTO `t_account_logs` VALUES (1209408381412192257, '2019-12-24 17:40:03', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 130, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:40:03', 0, '2019-12-24 17:40:03', b'0');
INSERT INTO `t_account_logs` VALUES (1209408395635077122, '2019-12-24 17:40:06', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 131, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:40:06', 0, '2019-12-24 17:40:06', b'0');
INSERT INTO `t_account_logs` VALUES (1209408935047737345, '2019-12-24 17:42:15', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 132, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:42:15', 0, '2019-12-24 17:42:15', b'0');
INSERT INTO `t_account_logs` VALUES (1209408986776088577, '2019-12-24 17:42:27', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 133, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:42:27', 0, '2019-12-24 17:42:27', b'0');
INSERT INTO `t_account_logs` VALUES (1209409503333986305, '2019-12-24 17:44:30', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 134, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:44:30', 0, '2019-12-24 17:44:30', b'0');
INSERT INTO `t_account_logs` VALUES (1209410157888679938, '2019-12-24 17:47:06', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 135, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:47:06', 0, '2019-12-24 17:47:06', b'0');
INSERT INTO `t_account_logs` VALUES (1209410846538870785, '2019-12-24 17:49:50', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 136, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:49:50', 0, '2019-12-24 17:49:50', b'0');
INSERT INTO `t_account_logs` VALUES (1209411012280987650, '2019-12-24 17:50:30', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 137, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:50:30', 0, '2019-12-24 17:50:30', b'0');
INSERT INTO `t_account_logs` VALUES (1209411229264916481, '2019-12-24 17:51:21', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 138, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:51:21', 0, '2019-12-24 17:51:21', b'0');
INSERT INTO `t_account_logs` VALUES (1209411243550715906, '2019-12-24 17:51:25', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 139, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:51:25', 0, '2019-12-24 17:51:25', b'0');
INSERT INTO `t_account_logs` VALUES (1209411283610513409, '2019-12-24 17:51:34', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 140, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:51:34', 0, '2019-12-24 17:51:34', b'0');
INSERT INTO `t_account_logs` VALUES (1209411481082540033, '2019-12-24 17:52:22', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 141, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:52:22', 0, '2019-12-24 17:52:22', b'0');
INSERT INTO `t_account_logs` VALUES (1209411749023068161, '2019-12-24 17:53:25', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 142, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 17:53:25', 0, '2019-12-24 17:53:25', b'0');
INSERT INTO `t_account_logs` VALUES (1209415826888994817, '2019-12-24 18:09:38', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 143, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-24 18:09:38', 0, '2019-12-24 18:09:38', b'0');
INSERT INTO `t_account_logs` VALUES (1209635512113270785, '2019-12-25 08:42:35', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 144, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-25 08:42:35', 0, '2019-12-25 08:42:35', b'0');
INSERT INTO `t_account_logs` VALUES (1209752646911614978, '2019-12-25 16:28:02', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 145, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-25 16:28:02', 0, '2019-12-25 16:28:02', b'0');
INSERT INTO `t_account_logs` VALUES (1210144073096368129, '2019-12-26 18:23:25', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 146, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-26 18:23:25', 0, '2019-12-26 18:23:25', b'0');
INSERT INTO `t_account_logs` VALUES (1210144852855230465, '2019-12-26 18:26:30', '192.168.0.118', 'Apache-HttpClient/4.5.10 (Java/1.8.0_161)', 147, 1, 'admin', 'username', 1, '@admin.com', 0, '2019-12-26 18:26:30', 0, '2019-12-26 18:26:30', b'0');
INSERT INTO `t_account_logs` VALUES (1268188360329224194, '2020-06-03 22:30:41', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0', 148, 1, 'admin', 'username', 1, '@admin.com', 0, '2020-06-03 22:30:42', 0, '2020-06-03 22:30:42', b'0');
INSERT INTO `t_account_logs` VALUES (1268378566302961665, '2020-06-04 11:06:30', '127.0.0.1', 'PostmanRuntime/7.25.0', 149, 1, 'admin', 'username', 1, '@admin.com', 0, '2020-06-04 11:06:30', 0, '2020-06-04 11:06:30', b'0');
INSERT INTO `t_account_logs` VALUES (1268378732456120322, '2020-06-04 11:07:10', '127.0.0.1', 'PostmanRuntime/7.25.0', 150, 1, 'admin', 'username', 1, '@admin.com', 0, '2020-06-04 11:07:10', 0, '2020-06-04 11:07:10', b'0');
INSERT INTO `t_account_logs` VALUES (1268381483131015169, '2020-06-04 11:18:06', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 151, 1, 'admin', 'username', 1, '@admin.com', 0, '2020-06-04 11:18:06', 0, '2020-06-04 11:18:06', b'0');
INSERT INTO `t_account_logs` VALUES (1268384080239857666, '2020-06-04 11:28:25', '127.0.0.1', 'PostmanRuntime/7.25.0', 152, 1, 'admin', 'username', 1, '@admin.com', 0, '2020-06-04 11:28:25', 0, '2020-06-04 11:28:25', b'0');
INSERT INTO `t_account_logs` VALUES (1268384660630876162, '2020-06-04 11:30:43', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 153, 1, 'admin', 'username', 1, '@admin.com', 0, '2020-06-04 11:30:43', 0, '2020-06-04 11:30:43', b'0');
INSERT INTO `t_account_logs` VALUES (1268384864323055617, '2020-06-04 11:31:32', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 154, 1, 'admin', 'username', 1, '@admin.com', 0, '2020-06-04 11:31:32', 0, '2020-06-04 11:31:32', b'0');
INSERT INTO `t_account_logs` VALUES (1268467973076848642, '2020-06-04 17:01:46', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 155, 1, 'admin', 'username', 1, '@admin.com', 0, '2020-06-04 17:01:46', 0, '2020-06-04 17:01:46', b'0');
INSERT INTO `t_account_logs` VALUES (1268468295069372417, '2020-06-04 17:03:03', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 156, 1, 'admin', 'username', 1, '@admin.com', 0, '2020-06-04 17:03:03', 0, '2020-06-04 17:03:03', b'0');
INSERT INTO `t_account_logs` VALUES (1268498184170438658, '2020-06-04 19:01:49', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 157, 1, 'admin', 'username', 1, '@admin.com', 0, '2020-06-04 19:01:49', 0, '2020-06-04 19:01:49', b'0');
INSERT INTO `t_account_logs` VALUES (1268498300600123393, '2020-06-04 19:02:17', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 158, 1, 'admin', 'username', 1, '@admin.com', 0, '2020-06-04 19:02:17', 0, '2020-06-04 19:02:17', b'0');
INSERT INTO `t_account_logs` VALUES (1268500241937895426, '2020-06-04 19:10:00', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 159, 1, 'admin', 'username', 1, '@admin.com', 0, '2020-06-04 19:10:00', 0, '2020-06-04 19:10:00', b'0');
INSERT INTO `t_account_logs` VALUES (1268501552083275777, '2020-06-04 19:15:12', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1, 2, 'test', 'username', 2, '@admin.com', 0, '2020-06-04 19:15:12', 0, '2020-06-04 19:15:12', b'0');
INSERT INTO `t_account_logs` VALUES (1268501635768029186, '2020-06-04 19:15:32', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 160, 1, 'admin', 'username', 1, '@admin.com', 0, '2020-06-04 19:15:32', 0, '2020-06-04 19:15:32', b'0');
INSERT INTO `t_account_logs` VALUES (1268503372771921922, '2020-06-04 19:22:26', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1, 1268503318577319937, 'test1', 'username', 1268503319009333249, '@admin.com', 0, '2020-06-04 19:22:26', 0, '2020-06-04 19:22:26', b'0');
INSERT INTO `t_account_logs` VALUES (1268503446956576770, '2020-06-04 19:22:44', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 2, 1268503318577319937, 'test1', 'username', 1268503319009333249, '@admin.com', 0, '2020-06-04 19:22:44', 0, '2020-06-04 19:22:44', b'0');
INSERT INTO `t_account_logs` VALUES (1268503506972872705, '2020-06-04 19:22:58', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 3, 1268503318577319937, 'test1', 'username', 1268503319009333249, '@admin.com', 0, '2020-06-04 19:22:58', 0, '2020-06-04 19:22:58', b'0');
INSERT INTO `t_account_logs` VALUES (1268503586266189825, '2020-06-04 19:23:17', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 161, 1, 'admin', 'username', 1, '@admin.com', 0, '2020-06-04 19:23:17', 0, '2020-06-04 19:23:17', b'0');
INSERT INTO `t_account_logs` VALUES (1268505622755958785, '2020-06-04 19:31:23', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 4, 1268503318577319937, 'test1', 'username', 1268503319009333249, '@admin.com', 0, '2020-06-04 19:31:23', 0, '2020-06-04 19:31:23', b'0');
INSERT INTO `t_account_logs` VALUES (1268505742226513921, '2020-06-04 19:31:51', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 162, 1, 'admin', 'username', 1, '@admin.com', 0, '2020-06-04 19:31:51', 0, '2020-06-04 19:31:51', b'0');
INSERT INTO `t_account_logs` VALUES (1268512768721735682, '2020-06-04 19:59:47', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 5, 1268503318577319937, 'test1', 'username', 1268503319009333249, '@admin.com', 0, '2020-06-04 19:59:47', 0, '2020-06-04 19:59:47', b'0');
INSERT INTO `t_account_logs` VALUES (1268512805124100098, '2020-06-04 19:59:55', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 163, 1, 'admin', 'username', 1, '@admin.com', 0, '2020-06-04 19:59:55', 0, '2020-06-04 19:59:55', b'0');
INSERT INTO `t_account_logs` VALUES (1268512921046274050, '2020-06-04 20:00:23', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 6, 1268503318577319937, 'test1', 'username', 1268503319009333249, '@admin.com', 0, '2020-06-04 20:00:23', 0, '2020-06-04 20:00:23', b'0');
INSERT INTO `t_account_logs` VALUES (1268512932974874626, '2020-06-04 20:00:26', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 7, 1268503318577319937, 'test1', 'username', 1268503319009333249, '@admin.com', 0, '2020-06-04 20:00:26', 0, '2020-06-04 20:00:26', b'0');
INSERT INTO `t_account_logs` VALUES (1268512995662942209, '2020-06-04 20:00:41', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 164, 1, 'admin', 'username', 1, '@admin.com', 0, '2020-06-04 20:00:41', 0, '2020-06-04 20:00:41', b'0');
INSERT INTO `t_account_logs` VALUES (1268513126923685889, '2020-06-04 20:01:12', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 8, 1268503318577319937, 'test1', 'username', 1268503319009333249, '@admin.com', 0, '2020-06-04 20:01:12', 0, '2020-06-04 20:01:12', b'0');
INSERT INTO `t_account_logs` VALUES (1268513186042400770, '2020-06-04 20:01:26', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 9, 1268503318577319937, 'test1', 'username', 1268503319009333249, '@admin.com', 0, '2020-06-04 20:01:26', 0, '2020-06-04 20:01:26', b'0');

SET FOREIGN_KEY_CHECKS = 1;
